
name := """ssoakkacorelib"""

organization := "com.verizon"

//This project uses semantic versioning. Learn: https://semver.org/
version := "4.9.0"

scalaVersion := "2.12.4"

val akkaVersion = "2.5.16"
val circeVersion = "0.9.3"
val scalapbVersion = "0.8.0"
val grpcVersion = "1.16.1"
val elasticVersion = "6.3.3"
val json4sVersion = "3.6.4"

fork in run := true

parallelExecution in Test := false

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser",
  "io.circe" %% "circe-generic-extras"
).map(_ % circeVersion)


libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
  "com.typesafe.akka" %% "akka-cluster-sharding" % akkaVersion,
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.json4s" %% "json4s-jackson" % "3.6.0-M2",
  "com.lightbend.akka" %% "akka-stream-alpakka-csv" % "0.8",
  "com.typesafe.akka" %% "akka-stream-kafka" % "0.18",
  "org.apache.kafka" % "kafka-clients" % "1.0.0",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.0.11",
  "com.google.protobuf" % "protobuf-java" % "3.1.0",
  "com.thesamet.scalapb" %% "scalapb-runtime" % scalapbVersion,
  "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalapbVersion,
  "io.grpc" % "grpc-netty" % grpcVersion,
  "io.grpc" % "grpc-netty-shaded" % grpcVersion,
  "org.feijoas" % "mango_2.12" % "0.14",
  "org.feijoas" %% "mango" % "0.14",
  "commons-io" % "commons-io" % "2.6",
  "io.monix" %% "monix" % "3.0.0-RC1",
  "org.scalacheck" %% "scalacheck" % "1.13.4" % "test",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.8" % Test,
  "com.github.sebruck" %% "scalatest-embedded-redis" % "0.3.0" % Test,
  "com.lightbend.akka" %% "akka-stream-alpakka-elasticsearch" % "1.0-M3",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.8",
  "org.scala-graph" %% "graph-core" % "1.13.0",
  "org.scala-graph" % "graph-json_2.12" % "1.12.1",
  "com.sksamuel.elastic4s" %% "elastic4s-http" % elasticVersion,
  "com.sksamuel.elastic4s" %% "elastic4s-testkit" % elasticVersion % "test",
  "com.sksamuel.elastic4s" %% "elastic4s-embedded" % elasticVersion % "test",
  "org.apache.logging.log4j" % "log4j-core" % "2.11.2"
)

libraryDependencies ++= Seq(
  "org.camunda.bpm.dmn" % "camunda-engine-dmn-bom" % "7.9.0" pomOnly(),
  "org.camunda.bpm.dmn" % "camunda-engine-dmn" % "7.9.0",
  "org.camunda.bpm.extension.feel.scala" % "feel-engine" % "1.5.0",
  "org.camunda.bpm.extension.feel.scala" % "feel-engine-factory" % "1.5.0",
  "org.camunda.bpm.extension.feel.scala" % "feel-engine-plugin" % "1.5.0",
  "org.camunda.bpm.dmn" % "camunda-engine-feel-api" % "7.9.0",
  "org.camunda.spin" % "camunda-spin-core" % "1.5.3",
  "org.camunda.bpm" % "camunda-engine" % "7.9.0" % "provided"
)

enablePlugins(AkkaGrpcPlugin)

//ENABLE TO GENERATE CLASSES FROM PROTO FILES
//PB.targets in Compile := Seq(
//  scalapb.gen() -> (sourceManaged in Compile).value
//)

// When running tests, we use this configuration
// javaOptions in Test += s"-Dconfig.file=${sourceDirectory.value}/test/resources/application.test.conf"
// We need to fork a JVM process when testing so the Java options above are applied
// fork in Test := true
