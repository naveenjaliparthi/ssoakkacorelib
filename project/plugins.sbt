addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.2")
addSbtPlugin("com.lightbend.akka.grpc" % "sbt-akka-grpc" % "0.4.1")
//ENABLE TO GENERATE CLASSES FROM PROTO FILES
//addSbtPlugin("com.thesamet" % "sbt-protoc" % "0.99.18")
//libraryDependencies += "com.thesamet.scalapb" %% "compilerplugin" % "0.7.4"
