package com.vz.security.sso.cache.kafka

import java.util
import java.util.{Properties, UUID}
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.kafka.GenerickKafkaPublisher
import com.vz.security.sso.logging.VZLogger
import com.vz.security.sso.models.CacheManagerOp
import com.vz.security.sso.models.CacheOp
import com.vz.security.sso.models.KakfaPublisherPayload
import scala.collection.JavaConverters._
import scala.concurrent.Future
import spray.json._

object CacheKafkaPublisher {
  import com.vz.security.sso.models.SprayFormatter._

  val conf: Config = ConfigFactory.load()
  val cacheKafkaConf: Config = conf.getConfig("sso.cache.kafka")
  val topicsList:List[String] = cacheKafkaConf.getStringList("topic").asScala.toList
  val topicWithKey = cacheKafkaConf.getBoolean("topicWithKey")
  val kafkaList:List[String] = cacheKafkaConf.getStringList("instances").asScala.toList

  def publishConfigToKafka(cacheConfOp: Option[CacheManagerOp]) : Future[Boolean] = {
    if(cacheConfOp.isEmpty){
      return Future.failed(new NullPointerException("Config is missing"))
    }
    val payload = cacheConfOp.get.config.id + ":" + cacheConfOp.get.toJson.compactPrint
    val payloadList: List[String] = List(payload)
    val kafkaRequest = Option(KakfaPublisherPayload(topicsList, kafkaList, topicWithKey, payloadList))
    val future = GenerickKafkaPublisher.publishPayload(kafkaRequest)
    future
  }

  def publishCacheData(dataOpList: Option[List[CacheOp]], topicNames: Set[String]): Future[Boolean] = {
    if(dataOpList.isEmpty) {
      return Future.failed(new NullPointerException("No data entries to add"))
    }
    val payloadList: List[String] = dataOpList.get.map ( dataOp => {
      val jsonString = dataOp.toJson.compactPrint
      jsonString
    })

    val topicsList: List[String] = topicNames.toList
    val kafkaRequest = Option(KakfaPublisherPayload(topicsList, kafkaList, topicWithKey, payloadList))
    val future = GenerickKafkaPublisher.publishPayload(kafkaRequest)
    future
  }

}