package com.vz.security.sso.cache.manager

import java.util.UUID

import akka.{Done, NotUsed}
import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage
import akka.kafka.ConsumerMessage.CommittableMessage
import akka.stream.alpakka.elasticsearch.WriteMessage
import akka.stream.scaladsl.{Flow, GraphDSL, Keep, RunnableGraph, Sink}
import akka.stream.{ActorAttributes, ActorMaterializer, ActorMaterializerSettings, ClosedShape, KillSwitches, UniqueKillSwitch}
import com.typesafe.config.ConfigFactory
import com.vz.security.sso.cache.manager.GenericCacheManager.{CMConfig, logger}
import com.vz.security.sso.cache.manager.cache.{MangoPlainCache, MangoRangeCache}
import com.vz.security.sso.cache.manager.config.ConfigLoader
import com.vz.security.sso.cache.manager.model.CacheExceptions.{GCacheManagerException, MangoPlainException}
import com.vz.security.sso.cache.manager.model.DynamicCache
import com.vz.security.sso.cache.manager.util.CacheUtil._
import com.vz.security.sso.logging.{VZLogger, VZMapLogger, VZQuickLogger}
import com.vz.security.sso.models.{CacheConfig, CacheManagerOp, CacheOp, EventSourceDAO}
import com.vz.security.sso.utils.ObjectMapperUtil
import com.vz.security.sso.utils.streams.{FlowUtil, SinkUtil, SourceUtil}
import com.vz.security.sso.cache.manager.config.Constants._
import com.vz.security.sso.cache.manager.config.ConfigLoader._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}
import com.vz.security.sso.utils.streams.SupervisionStrategyUtil.{flowDecider, sinkDecider, systemDecider}
import org.apache.kafka.clients.consumer.ConsumerRecord


object GenericCacheManager {

  val logger = new VZLogger(this)

  def apply()(implicit system: ActorSystem): GenericCacheManager = {
    new GenericCacheManager()
  }

  case class CMConfig(cacheConfig: CacheConfig, dynamicCache: DynamicCache)

  val elasticConfig = ConfigFactory.load().getConfig("elastic")
  val cacheConfigIndex = elasticConfig.getString("cacheConfigIndex")
  val cacheConfigType = elasticConfig.getString("cacheConfigType")
  val mangoIndex = elasticConfig.getString("mangoIndex")
  val mangoType = elasticConfig.getString("mangoType")
  val rangeIndex = elasticConfig.getString("rangeIndex")
  val rangeType = elasticConfig.getString("rangeType")

}

import GenericCacheManager._
class GenericCacheManager(implicit system: ActorSystem) {

  implicit val ec = system.dispatchers.lookup("akka.actor.kafka-dispatcher")
  implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(systemDecider))

  val logger = new VZQuickLogger(this)

  def generateId = UUID.randomUUID().toString

  /**
   * #######################
   * CACHE CONFIG OPERATIONS
   * #######################
   */
  val cacheConfigs = mutable.HashMap[String, CMConfig]()

  val lookupIndexMap = mutable.HashMap[String, List[DynamicCache]]()

  private var allCategories = Set[String]()

  private def addLookup(lookup_attributes: Set[String], dyn: DynamicCache) = {
    try {
      lookup_attributes.map(_.toLowerCase).foreach(att => {
        val dynList = lookupIndexMap.get(att) match {
          case Some(currentDynList) => dyn :: currentDynList
          case None => List(dyn)
        }
        lookupIndexMap.put(att, dynList)
      })
      true
    }
    catch {
      case ex: Exception =>
        logger.error("GenericCacheManagerAddLookup", null, ex,
          "error_data", s"cacheid:${dyn.metadata().id} lookup_attributes: $lookup_attributes",
          "error_message", ex.getMessage)
        false
    }
  }

  private def removeLookup(lookup_attributes: Set[String], dyn: DynamicCache) = {
    try {
      lookup_attributes.map(_.toLowerCase).foreach(att => {
        lookupIndexMap.get(att).foreach(currentDynList => {
          val dynList = currentDynList diff List(dyn)
          lookupIndexMap.put(att, dynList)
        })
      })
      true
    }
    catch {
      case ex: Exception =>
        logger.error("GenericCacheManagerRemoveLookup", null, ex,
          "error_data", s"cacheid:${dyn.metadata().id} lookup_attributes: $lookup_attributes",
          "error_message", ex.getMessage)
        false
    }
  }

  private def refreshCategories = {
    allCategories = cacheConfigs.map(_._2.cacheConfig.category).toSet
  }

  /**
   * Checks if config already exist
   */
  def isConfigExist(cacheConfig: CacheConfig): Boolean = {
    cacheConfigs.get(cacheConfig.id).isDefined
  }


  /**
   * Creates new cache instance based on cachetype
   * Start cache streams if not called for rehydrate purpose. i.e. only for new request
   */
  def addCache(cacheConfig: CacheConfig, rehydrate: Boolean = false)(implicit system: ActorSystem) = {
    try {
      if (isConfigExist(cacheConfig)) {
        logger.info("AddCacheConfig", null, "status", s"cache with id ${cacheConfig.id} already exist. Rehydrate: $rehydrate.")
        false
      }
      else {
        logger.info("AddCacheConfig", null, "status", s"Adding cache: ${ObjectMapperUtil.toJson(cacheConfig)} and rehydrate $rehydrate")
        val cacheInstance = cacheConfig.cache_type match {
          case MANGO => new MangoPlainCache(cacheConfig)
          case RANGE => new MangoRangeCache(cacheConfig)
          case _ => throw MangoPlainException(s"Cacheid ${cacheConfig.id} Unsupported cache type ${cacheConfig.cache_type}")
        }
        if (!rehydrate) cacheInstance.startCacheStreams()
        val cmConfig = CMConfig(cacheConfig, cacheInstance)
        addLookup(cacheConfig.lookup_attributes, cacheInstance)
        cacheConfigs.put(cmConfig.cacheConfig.id, cmConfig)
        refreshCategories
        true
      }
    }
    catch {
      case ex: Exception =>
        logger.error("AddCacheConfig", null, ex,
          "error_data", s"cacheConfig: ${cacheConfig} rehydrate: $rehydrate",
          "error_message", ex.getMessage
        )
        false
    }
  }

  def removeCache(cacheConfig: CacheConfig) = {
    try {
      cacheConfigs.get(cacheConfig.id).foreach(_.dynamicCache.stopCacheStreams())
      cacheConfigs.get(cacheConfig.id).foreach(dyn => removeLookup(cacheConfig.lookup_attributes, dyn.dynamicCache))
      cacheConfigs.remove(cacheConfig.id)
      refreshCategories
      true
    }
    catch {
      case ex: Exception =>
        logger.error("RemoveCacheConfig", null, ex,
          "error_data", s"cacheConfig: $cacheConfig", "error_message", ex.getMessage)
        false
    }
  }

  /**
   * Graph helps to dynamically operate/Process cache
   */
  def cacheConfigStream = {
    logger.info("cache_config_streams", null, "streamStatus", s"Starting config in-memory stream")
    val processFlow =
      Flow[CacheManagerOp]
        .map(message => {
          val response = message.op match {
            case ADD => addCache(message.config)
            case REMOVE => removeCache(message.config)
            case _ => throw GCacheManagerException(s"cacheid:${message.config.id} action ${message.op} cannot be Performed")
          }
          logCount()
          logger.info("InMemoryOperation", null, "cacheConfiginfo",
            s"cacheId: ${message.config.id} operation: ${message.op} payload: ${ObjectMapperUtil.toJson(message.config)}")
        })
        .toMat(Sink.ignore)(Keep.right)
        .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))

    RunnableGraph.fromGraph(g = GraphDSL.create() {
      implicit b =>

        import GraphDSL.Implicits._

        //Source
        val kafkaSource = b.add(SourceUtil.kafkaConsumerPlainSource(generateId,
          ConfigLoader.kafkaTopicConfigManager,
          ConfigLoader.bootStrapServers,
          ConfigLoader.kafkaProperties)).out

        //Flows
        val MessageUnMarshallFlow = b.add(FlowUtil.unMarshallFlowCopy[CacheManagerOp]())
        val ValidationFlow = b.add(validationFlow)
        val ProcessSink = b.add(processFlow)

        kafkaSource ~> MessageUnMarshallFlow ~> ValidationFlow ~> ProcessSink
        ClosedShape
    })
  }

  /**
   * Graph that writes to elastic and helps to dynamically operate caches
   * Uses unique kafka groupId across cluster to avoid duplication of writes in elastic
   */
  def cacheConfigElasticStream = {
    logger.info("cache_config_streams", null, "streamStatus", s"Starting config elastic stream")
    lazy val elasticProcessFlow: Flow[(CacheManagerOp, ConsumerMessage.CommittableOffset), WriteMessage[CacheConfig, ConsumerMessage.CommittableOffset], NotUsed] =
      Flow[(CacheManagerOp, ConsumerMessage.CommittableOffset)]
        .map(msg => {
          val elasticMessage: WriteMessage[CacheConfig, ConsumerMessage.CommittableOffset] =
            msg._1.op match {
              case ADD =>
                WriteMessage.createUpsertMessage(msg._1.config.id, msg._1.config)
                  .withPassThrough(msg._2)
              case REMOVE =>
                WriteMessage.createDeleteMessage(msg._1.config.id).withPassThrough(msg._2)
              case _ => throw new Exception(s"cacheid:${msg._1.config.id} action ${msg._1.op} cannot be Performed")
            }
          logger.info("ElasticOperation", null, "CacheConfigInfo", s"cacheId: ${msg._1.config.id} operation: ${msg._1.op} payload: ${ObjectMapperUtil.toJson(msg._1.config)}")
          elasticMessage
        }).withAttributes(ActorAttributes.supervisionStrategy(flowDecider))

    import com.vz.security.sso.models.SprayFormatter._
    RunnableGraph.fromGraph(g = GraphDSL.create() {
      implicit b =>
        import GraphDSL.Implicits._

        //Source
        val kafkaSource = b.add(SourceUtil.kafkaConsumerCommittableSource(ConfigLoader.cacheConfigGroupId,
          ConfigLoader.kafkaTopicConfigManager, ConfigLoader.bootStrapServers, ConfigLoader.kafkaProperties)).out

        //Flows
        val MessageUnMarshallFlow = b.add(FlowUtil.unMarshallFlowWithCommittableMessage[CacheManagerOp]())
        val ValidationFlow = b.add(validationFlowWithOffset)
        val ElasticProcessFlow = b.add(elasticProcessFlow)

        //Sink
        val ElasticSink = b.add(SinkUtil.elasticSinkWithCommittableOffset[CacheConfig](cacheConfigIndex, cacheConfigType))

        kafkaSource ~> MessageUnMarshallFlow ~> ValidationFlow ~> ElasticProcessFlow ~> ElasticSink
        ClosedShape
    })
  }

  /**
   * Fetch all cache config data from Elastic. Create cache's and add to cacheConfigs map
   */
  def rehydrateCacheConfig: Future[Done] = {
    logger.info("cache_config_streams", null, "streamStatus", s"rehydrateCacheConfig started")
    import com.vz.security.sso.models.SprayFormatter.cacheConfigFormat
    lazy val getAllQuery = """{"match_all":{}}""".stripMargin
    SourceUtil.elasticSource[CacheConfig](cacheConfigIndex, cacheConfigType, getAllQuery)
      .map(cc => CacheManagerOp(ADD, cc._2))
      .via(validationFlow)
      .map(r => {
        addCache(r.config, rehydrate = true)
      })
      .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
      .runWith(Sink.ignore)
  }


  /**
   * Executed on application start/restart in sequence
   * 1. Loads whole cache config from elastic
   * 2. For each config
   *   a. Create cache's and add to cacheConfigs map
   *   b. Loads data/snapshot from elastic to cache
   *   c. Start cache data streams
   * 3. cache config stream
   * 4. Start cache config elastic stream
   */
  def initialize() = {
    logger.info("cache_config_streams", null, "StreamStatus", s"Initialization started")
    val rehydrate = for {
      // Elastic data
      r1 <- rehydrateCacheConfig.recoverWith {
        case t: Throwable => {
          logger.error("Initialize in generic_cache_manager", null, t, "error_message", t.getMessage)
          System.exit(1)
          Future.failed(new Exception("rehydrateCacheConfig", t))
        }
      }
      r2 <- rehydrateCache.recoverWith {
        case t: Throwable => {
          logger.error("Initialize in generic_cache_manager", null, t, "error_message", t.getMessage)
          System.exit(1)
          Future.failed(new Exception("rehydrateCache", t))
        }
      }
      // Cache data streams for existing cache's
      r3 <- startCacheDataStreams.recoverWith {
        case t: Throwable => {
          logger.error("Initialize in generic_cache_manager", null, t, "error_message", t.getMessage)
          System.exit(1)
          Future.failed(new Exception("startCacheDataStreams", t))
        }
      }
    } yield (r1, r2, r3)
    rehydrate.foreach(r => {
      logger.info("cache_config_streams", null, "streamstatus", s"Completed rehydration process")
      cacheConfigStream.run()
      cacheDataStream.run()
      if (ConfigLoader.elasticPersistence) {
        cacheConfigElasticStream.run()
        cacheDataElasticStream.run()
      }
      else {
        logger.info("GenericCacheManager", null, "Flag", "Application level elastic persistence is disabled")
      }
    })
  }

  val cacheTypes = Set(MANGO, RANGE)
  val evictionTypes = Set(sizeEviction, timeEviction)

  /**
   * Validates cache config operation payload
   */
  def validateConfig(cmop: CacheManagerOp): (Boolean, List[String]) = {
    var errorMessages = new ListBuffer[String]()
    val cacheConfig = cmop.config
    val cacheTypeCheck = cacheTypes.contains(cacheConfig.cache_type)
    if (!cacheTypeCheck) errorMessages += "cacheTypeCheck; "

    val evictionTypeCheck = if (cacheConfig.eviction_policy.isDefined)
      evictionTypes.contains(cacheConfig.eviction_policy.get.eviction_type)
    else true //eviction type is optional so allow
    if (!evictionTypeCheck) errorMessages += "evictionTypeCheck; "

    val keysCountCheck = if (cacheConfig.cache_type.equalsIgnoreCase(MANGO)) {
      cacheConfig.key.length == 1
    }
    else {
      cacheConfig.key.length == 2
    }
    if (!keysCountCheck) errorMessages += "keysCountCheck; "

    (cacheTypeCheck && evictionTypeCheck && keysCountCheck, errorMessages.toList)
  }

  /**
   * Flow that validates incoming cache config operation request
   */
  lazy val validationFlowWithOffset: Flow[(CacheManagerOp, ConsumerMessage.CommittableOffset), (CacheManagerOp, ConsumerMessage.CommittableOffset), NotUsed] =
    Flow[(CacheManagerOp, ConsumerMessage.CommittableOffset)].filter(cmop => {
      val validationResult = validateConfig(cmop._1)
      if (!validationResult._1) {
        logger.info("validationFlowWithOffset", null, "ValidationStatus", s"Validation failed for config operation in elastic stream. CacheConfigOp: ${ObjectMapperUtil.toJson(cmop)}. Failures: ${validationResult._2}")
      }
      validationResult._1
    }).withAttributes(ActorAttributes.supervisionStrategy(flowDecider))

  lazy val validationFlow: Flow[CacheManagerOp, CacheManagerOp, NotUsed] =
    Flow[CacheManagerOp].filter(cmop => {
      val validationResult = validateConfig(cmop)
      if (!validationResult._1) {
        logger.info("validationFlow", null, "ValidationStatus", s"Validation failed for config operation in in-memory stream. CacheConfigOp: ${ObjectMapperUtil.toJson(cmop)}. Failures: ${validationResult._2}")
      }
      validationResult._1
    }).withAttributes(ActorAttributes.supervisionStrategy(flowDecider))


  /**
   * ##################################
   * OPERATIONS PERFORMED ON CACHE DATA
   * ##################################
   */
  /**
   * Gives count of all cache data
   * Returns: List of Map(cacheId, cacheName, cacheCategory, cacheCount)
   */
  def getCount: List[Map[String, Any]] = {
    try {
      cacheConfigs.map(inv => {
        Map("cacheId" -> inv._2.cacheConfig.id, "name" -> inv._2.cacheConfig.name, "category" -> inv._2.cacheConfig.category, "count" -> inv._2.dynamicCache.getCount())
      }).toList
    }
    catch {
      case ex: Exception =>
        logger.error("getCount", null, ex, "error_message", ex.getMessage)
        throw GCacheManagerException(s"Unable to lookup on cache")
    }
  }

  /**
   * Logs count of each cache and cache name
   */
  def logCount() = {
    val vzLogger = new VZLogger(this, "cache_data_count")
    vzLogger.addKeyValue("count", getCount)
    vzLogger.info
  }

  /**
   * Create snapshot of cache. Saves snapshot in elastic
   * This is applicable to only Range Cache
   */
  def dehydrateCache: Future[Done] = {
    Future {
      cacheConfigs.foreach(inv => {
        inv._2.dynamicCache.dehydrate()
      })
      Done
    }.recover { case ex: Exception =>
      logger.error("dehydrateCache", null, ex, "error_message", ex.getMessage)
      Done
    }
  }

  /**
   * Fetch latest snapshot/data from Elastic
   * Load snapshot/data in cache
   */
  def rehydrateCache: Future[Done] =
    Future {
      cacheConfigs.foreach(inv => {
        inv._2.dynamicCache.rehydrate()
      })
      Done
    }.recover { case ex: Exception =>
      logger.error("rehydrateCache", null, ex, "error_message", ex.getMessage)
      Done
    }


  def startCacheDataStreams: Future[Done] =
    Future {
      cacheConfigs.foreach(config => config._2.dynamicCache.startCacheStreams())
      Done
    }.recover { case ex: Exception =>
      logger.error("startCacheDataStreams", null, ex, "error_message", ex.getMessage)
      Done
    }


  /**
   * Get categories for Ip or User
   */
  def processInputAttributes(inputMap: Map[String, Any]): mutable.Map[String, AnyRef] = {
    val vzLogger = new VZLogger(this, "process_input_attributes_log")
    try {
      /**
       * STEP 1: Lookup on cache data and build CATEGORY -> SET(CACHENAMES)
       * a. Iterate inputMap and compute List[CategoryLookup] for whole inputMap
       * b. Extract additional cache attributes to add
       * c. Group by category -> Map[category, List[CategoryLookup]
       * d. Flatten value part in #b and remove duplicates -> Map[category, list[cacheNames]
       */
      var cacheAttrs = Map[String, AnyRef]()
      val categoryLookupMap =
        inputMap.foldLeft(List[CategoryLookup]())((catLookupList, keyValue) => {
          catLookupList ::: lookupOnCache(keyValue._1, keyValue._2)
        }) //#a
          .map(x => {
            cacheAttrs ++= x.cacheAttr
            x
          }) //#b
          .groupBy(_.cacheCategory) //#c
          .map(kv => (kv._1, kv._2.map(_.cacheName).distinct)) //#d

      //STEP 2: For missing categories add empty list
      val allCategoryLookup = allCategories.foldLeft(categoryLookupMap)((catLookupMap, cat) => {
        if (catLookupMap.get(cat).isEmpty) catLookupMap + (cat -> List())
        else catLookupMap
      })

      //STEP 3: Merge categoryResult with inputmap
      var lookupResult = scala.collection.mutable.Map[String, AnyRef]()
      inputMap.foreach(keyvalue => lookupResult += (keyvalue._1 -> keyvalue._2.toString))
      allCategoryLookup.foreach(keyvalue => lookupResult += (keyvalue._1 -> keyvalue._2))


      //STEP 4: Merge Added Params
      lookupResult ++= cacheAttrs

      // LOGGING
      vzLogger.addKeyValue("generic_lookup_result", "Lookup result")
      lookupResult.foreach(keyvalue => vzLogger.addKeyValue(keyvalue._1, keyvalue._2))
      vzLogger.info

      //RETURN RESULT
      lookupResult
    }
    catch {
      case ex: Exception =>
        vzLogger.addKeyValue("error_source", "processInputAttributes method in generic_cache_manager")
        vzLogger.addKeyValue("error_data", s"inputMap: $inputMap")
        vzLogger.addKeyValue("error_message", ex.getMessage)
        vzLogger.error(ex)
        throw GCacheManagerException(s"Unable to lookup on inputMap: $inputMap")
    }
  }

  case class CategoryLookup(cacheCategory: String, cacheName: String, cacheAttr: Map[String, AnyRef])

  /**
   * @param lookupOn    lookup parameter on which this operation is performed
   * @param keyToSearch Key to search cache data
   *                    1. For Mango plain send key in String format
   *                    2. For Mango Range send key in Long/String format but not tuple
   * @return List of CategoryLookup
   */
  private def lookupOnCache(lookupOn: String, keyToSearch: Any): List[CategoryLookup] = {
    try {
      lookupIndexMap.getOrElse(lookupOn.toLowerCase, List()).foldLeft(List[CategoryLookup]())((catList, cache) => {
        if (cache.read(keyToSearch).isDefined) {
          val config = cache.metadata()
          val cacheAttr = cache.read(keyToSearch).get.filterKeys(config.output_attributes.toSet).mapValues(_.toString)
          catList :+ CategoryLookup(config.category, config.name, cacheAttr)
        }
        else catList
      })
    }
    catch {
      case ex: Exception =>
        logger.error("lookupOnCache", null, ex, "error_data", s"lookupOn: $lookupOn keyToSearch: $keyToSearch",
          "error_message", ex.getMessage
        )
        List()
    }
  }


  /**
   * Cache key in elastic is stored as hash with combination of cacheid and key
   * This helps to fetch or delete document from elastic data on particular index and type
   */
  private def elasticKeyForCache(cacheOp: CacheOp): String = {
    try {
      val cacheConfig = cacheConfigs(cacheOp.cache_id).cacheConfig //Message will reach this step only if cacheId exists
      md5HashString(cacheOp.cache_id + cacheKey(cacheOp.payload, cacheConfig))
    }
    catch {
      case ex: Exception =>
        logger.error("elasticKeyForCache", null, ex,
          "error_data", s"cacheid: ${cacheOp.cache_id} payload: ${cacheOp.payload}",
          "error_message", ex.getMessage)
        throw MangoPlainException(s"cacheid: ${cacheOp.cache_id} not able to compute elastic key for payload ${cacheOp.payload}")
    }
  }


  /**
   * Graph that writes to elastic
   * Uses unique kafka groupId across cluster to avoid duplication of writes in elastic
   */
  def cacheDataElasticStream = {

    //Check if respective cache config is alive and persist is true
    lazy val IsCacheExistFlowWithCommittableOffset: Flow[CommittableMessage[String, String], CommittableMessage[String, String], NotUsed] = {
      logger.info("IsCacheExistFlowWithCommittableOffset", null, "status", s"Started")
      Flow[CommittableMessage[String, String]]
        .filter(c => {
          if (Option(c.record.key()).isDefined) {
            val cc = cacheConfigs.get(c.record.key())
            cc.isDefined && cc.get.cacheConfig.cache_type.equalsIgnoreCase(MANGO) && cc.get.cacheConfig.persist
          }
          else {
            false
          }
        })
        .withAttributes(ActorAttributes.supervisionStrategy(flowDecider))
    }
    lazy val elasticOperationsFlow: Flow[(CacheOp, ConsumerMessage.CommittableOffset), WriteMessage[EventSourceDAO, ConsumerMessage.CommittableOffset], NotUsed] =
      Flow[(CacheOp, ConsumerMessage.CommittableOffset)]
        .map(message => {
          val elasticMessage: WriteMessage[EventSourceDAO, ConsumerMessage.CommittableOffset] = message._1.op match {
            case ADD =>
              WriteMessage.createUpsertMessage(elasticKeyForCache(message._1),
                EventSourceDAO(message._1.cache_id, message._1.payload, getElasticDate)).withPassThrough(message._2)
            case REMOVE =>
              WriteMessage.createDeleteMessage(elasticKeyForCache(message._1)).withPassThrough(message._2)
            case _ =>
              throw MangoPlainException(s"cacheid: ${message._1.cache_id} action ${message._1.op} cannot be Performed")
          }
          elasticMessage
        }).withAttributes(ActorAttributes.supervisionStrategy(flowDecider))

    import com.vz.security.sso.models.SprayFormatter._

    val switch = KillSwitches.single[WriteMessage[EventSourceDAO, ConsumerMessage.CommittableOffset]]

    RunnableGraph.fromGraph(g = GraphDSL.create(switch) { implicit b: GraphDSL.Builder[UniqueKillSwitch] =>
      sw =>
        import GraphDSL.Implicits._
        //Source
        val kafkaSource = b.add(SourceUtil
          .kafkaConsumerCommittableSource(ConfigLoader.elastickafkagroupid,
            ConfigLoader.kafkaTopicCacheData,
            bootStrapServers,
            kafkaProperties)).out

        //Flows
        val FilterIfCacheExistFlowCommittableOffset = b.add(IsCacheExistFlowWithCommittableOffset)
        val MessageUnMarshallFlow = b.add(FlowUtil.unMarshallFlowWithCommittableMessage[CacheOp])
        val ElasticFlow = b.add(elasticOperationsFlow)

        //Elastic Sink
        val ElasticSink = b.add(SinkUtil.elasticSinkWithCommittableOffset[EventSourceDAO](mangoIndex, mangoType))

        kafkaSource ~> FilterIfCacheExistFlowCommittableOffset ~> MessageUnMarshallFlow ~> ElasticFlow ~> sw ~> ElasticSink
        ClosedShape
    })
  }

  /**
   * Filters and Forwards message only if cache exists and queueRef is not empty
   */
  lazy val FilterFlow: Flow[ConsumerRecord[String, String], ConsumerRecord[String, String], NotUsed] = {
    logger.info("FilterFlow", null, "status", s"Started ")
    Flow[ConsumerRecord[String, String]]
      .filter(c => Option(c.key()).isDefined)
      .filter(c => cacheConfigs.get(c.key()).isDefined && cacheConfigs(c.key()).dynamicCache.getqueueRef().isDefined)
      .withAttributes(ActorAttributes.supervisionStrategy(flowDecider))
  }

  lazy val queueSink = {
    logger.info("queueSink", null, "status", "started")
    Flow[ConsumerRecord[String, String]]
      .map(c => {
        cacheConfigs(c.key()).dynamicCache.getqueueRef().get offer c
      })
      .toMat(Sink.ignore)(Keep.right)
      .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
  }

  def cacheDataStream = {
    logger.info("cache_data_streams", null, "streamStatus", s"Starting cache data stream")

    RunnableGraph.fromGraph(g = GraphDSL.create() {
      implicit b =>

        import GraphDSL.Implicits._

        //Source
        val kafkaSource = b.add(SourceUtil.kafkaConsumerPlainSource(generateId,
          ConfigLoader.kafkaTopicCacheData,
          ConfigLoader.bootStrapServers,
          ConfigLoader.kafkaProperties)).out

        //Flows
        val filterFlow = b.add(FilterFlow)

        //Sink
        val qsink = b.add(queueSink)

        kafkaSource ~> filterFlow ~> qsink

        ClosedShape
    })
  }

}