package com.vz.security.sso.cache.manager.cache



import java.time.{LocalDateTime, ZoneOffset}
import java.util.concurrent.TimeUnit

import akka.Done
import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl.{Flow, Keep, Sink, SourceQueueWithComplete}
import com.vz.security.sso.common.constants.SharedConstants._
import com.vz.security.sso.cache.manager.GenericCacheManager
import com.vz.security.sso.cache.manager.model.Cache
import com.vz.security.sso.cache.manager.model.CacheExceptions.MangoPlainException
import com.vz.security.sso.cache.manager.util.CacheUtil
import com.vz.security.sso.cache.manager.util.CacheUtil._
import com.vz.security.sso.logging.VZQuickLogger
import com.vz.security.sso.models.{CacheConfig, CacheOp, EventSourceDAO}
import com.vz.security.sso.utils.streams.SupervisionStrategyUtil.{sinkDecider, systemDecider}
import com.vz.security.sso.utils.streams.{FlowUtil, SourceUtil, SupervisionStrategyUtil}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.feijoas.mango.common.cache.CacheBuilder

import scala.concurrent.Future

class MangoPlainCache(cacheConfig: CacheConfig)
                     (implicit system: ActorSystem) extends Cache[String, Map[String, Any]] {

  implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(systemDecider))
  implicit val ec = system.dispatchers.lookup("akka.actor.kafka-dispatcher")

  val logger = new VZQuickLogger(this)

  val mangoCache: org.feijoas.mango.common.cache.Cache[String, Map[String, Any]] = CacheBuilder.newBuilder()
    .maximumSize(getEvictionSize(cacheConfig))
    .expireAfterWrite(getEvictionTime(cacheConfig), TimeUnit.MINUTES)
    .build()

  override def add(key: String, value: Map[String, Any]) = {
    try {
      val k = if (!cacheConfig.case_sensitive) key.toLowerCase
      else key
      mangoCache.put(k.trim, value)
      true
    }
    catch {
      case e: Exception =>
        logger.error("MangoPlainAdd", null, e,
          "error_data", s"cacheid: ${cacheConfig.id} key: $key value: $value",
          "error_message", e.getMessage)
        false
    }
  }

  override def read(key: Any): Option[Map[String, Any]] = {
    try {
      val k = if (!cacheConfig.case_sensitive) key.toString.toLowerCase
      else key.toString
      mangoCache.getIfPresent(k.trim)
    }
    catch {
      case e: Exception =>
        logger.error("MangoPlainRead", null, e,
          "error_data", s"cacheid: ${cacheConfig.id} key: $key",
          "error_message", e.getMessage)
        None
    }
  }

  override def remove(key: String) = {
    try {
      val k = if (!cacheConfig.case_sensitive) key.toLowerCase
      else key
      mangoCache.invalidate(k.trim)
      true
    } catch {
      case e: Exception =>
        logger.error("MangoPlainRemove", null, e,
          "error_data", s"cacheid: ${cacheConfig.id} key: $key",
          "error_message", e.getMessage)
        false
    }
  }

  override def search(key: Any): Vector[String] = {
    try {
      val k = if (!cacheConfig.case_sensitive) key.toString.toLowerCase
      else key.toString
      mangoCache.asMap().keys.filter(_.contains(k)).toVector
    }
    catch {
      case ex: Exception =>
        logger.error("MangoPlainSearch", null, ex,
          "error_data", s"cacheid: ${cacheConfig.id} key: $key",
          "error_message", ex.getMessage)
        Vector[String]()
    }
  }

  override def readAll(): Vector[Map[String, Any]] = {
    try {
      mangoCache.asMap().values.toVector
    } catch {
      case e: Exception =>
        logger.error("MangoPlainReadAll", null, e,
          "error_data", s"cacheid: ${cacheConfig.id}",
          "error_message", e.getMessage)
        Vector()
    }
  }

  /**
   * Mango cache follows event sourcing mechanism of storing data to Elastic
   * dehydration is not required to implement
   */
  override def dehydrate(): Future[Done] = Future {
    Done
  }

  /**
   * Fetch all data from Elastic
   * Load it to cache
   */
  override def rehydrate(): Future[Done] = loadDataFromElastic

  override def metadata(): CacheConfig = cacheConfig

  override def status(): Option[String] = None

  override def getCount(): Long = mangoCache.size()

  var cacheDataQueueRef: Option[SourceQueueWithComplete[ConsumerRecord[String, String]]] = None

  override def getqueueRef(): Option[SourceQueueWithComplete[ConsumerRecord[String, String]]] = cacheDataQueueRef


  override def startCacheStreams(): Unit = {
    cacheDataQueueRef = Some(cacheDataGraph)
    logger.info("mPlain_cache_data_streams", null,
      "StreamStatus", s"Starting data stream for cacheid ${cacheConfig.id}")
  }

  override def stopCacheStreams(): Unit = {
    logger.info("mPlain_cache_data_streams", null,
      "streamStatus", s"Stopping data stream for cacheid ${cacheConfig.id}")
    //    cacheDataGraphSwitch.foreach(_.shutdown())
  }

  private def loadDataFromElastic: Future[Done] = {
    import com.vz.security.sso.models.SprayFormatter.eventSourceDAOFormat
    logger.info("mangoloadDataFromElastic", null, "cacheid", cacheConfig.id)
    //Elastic source is giving in reverse order so sorting desc in query
    val searchParams = Map(
      "query" ->
        s""" {
           |    "bool": {
           |      "filter": [
           |        { "term":  { "cache_id": "${cacheConfig.id}" }},
           |        { "range": { "date": { "gte": "$getRehydrateTime"}}}
           |      ]
           |    }
           |  }""".stripMargin,
      "sort" -> """{"date": "asc"}""".stripMargin
    )
    SourceUtil.elasticSourceWithSearchParams[EventSourceDAO](mangoIndex, mangoType, searchParams)
      .map(r => {
        try {
          add(CacheUtil.cacheKey(r._2.cache_payload, cacheConfig), r._2.cache_payload)
        }
        catch {
          case ex: Exception =>
            logger.error("loadDataFromElastic", null, ex,
              "error_data", s"cacheid: ${cacheConfig.id} payload: ${r._2.cache_payload}",
              "error_message", ex.getMessage)
            false
        }
      })
      .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
      .runWith(Sink.ignore)
  }

  def getRehydrateTime: String = {
    val evicTime = getEvictionTime(cacheConfig)
    val date = LocalDateTime.now(ZoneOffset.UTC).minusMinutes(evicTime)
    date.format(elasticFormat)
  }

  /**
   * Graph that operates on cache
   * It generates random kafka groupId to read all messages from topic
   */
  def cacheDataGraph: SourceQueueWithComplete[ConsumerRecord[String, String]] = {
    lazy val processFlow =
      Flow[CacheOp]
        .map(message => {
          message.op match {
            case ADD => add(CacheUtil.cacheKey(message.payload, cacheConfig), message.payload)
            case REMOVE => remove(CacheUtil.cacheKey(message.payload, cacheConfig))
            case _ =>
              throw MangoPlainException(s"cacheid: ${cacheConfig.id} action ${message.op} cannot be Performed")
          }
        })
        .toMat(Sink.ignore)(Keep.right)
        .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))

    SourceUtil.queueSource[ConsumerRecord[String, String]](1000)
      .via(FlowUtil.FilterKeyFlow(cacheConfig.id))
      .via(FlowUtil.unMarshallFlowCopy[CacheOp](Some(cacheConfig.id)))
      .to(processFlow)
      .withAttributes(ActorAttributes.supervisionStrategy(SupervisionStrategyUtil.systemDecider))
      .run()
  }
}

