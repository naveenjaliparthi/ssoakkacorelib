package com.vz.security.sso.cache.manager.cache



import java.util.UUID

import akka.Done
import akka.actor.ActorSystem
import akka.serialization.SerializationExtension
import akka.stream._
import akka.stream.alpakka.elasticsearch.WriteMessage
import akka.stream.scaladsl.{Flow, Keep, Sink, Source, SourceQueueWithComplete}
import com.vz.security.sso.common.constants.SharedConstants._
import com.vz.security.sso.cache.manager.config.ConfigLoader
import com.vz.security.sso.cache.manager.model.Cache
import com.vz.security.sso.cache.manager.model.CacheExceptions.MangoRangeException
import com.vz.security.sso.cache.manager.util.CacheUtil._
import com.vz.security.sso.logging.VZQuickLogger
import com.vz.security.sso.models.{CacheConfig, CacheOp, SnaphotDAO}
import com.vz.security.sso.utils.streams.SupervisionStrategyUtil.{flowDecider, systemDecider}
import com.vz.security.sso.utils.streams.{FlowUtil, SinkUtil, SourceUtil, SupervisionStrategyUtil}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.feijoas.mango.common.collect._

import scala.concurrent.Future
import scala.math.Ordering.Long

class MangoRangeCache(cacheConfig: CacheConfig)
                     (implicit system: ActorSystem) extends Cache[(Long, Long), Map[String, Any]] {

  val RangeCache: mutable.RangeMap[Long, Map[String, Any], Ordering.Long.type] = mutable.RangeMap.empty

  implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(systemDecider))
  implicit val ec = system.dispatchers.lookup("akka.actor.kafka-dispatcher")

  val logger = new VZQuickLogger(this)

  override def add(key: (Long, Long), value: Map[String, Any]): Boolean = {
    try {
      if(key._1 <= key._2){
        RangeCache += Range.closed(key._1, key._2) -> value
        true
      }
      else {
        logger.info("MangoRangeAdd", null, "status", "NotSaved",
          "reason", "key1 greater than key2", "key1", key._1, "key2", key._2)
        false
      }
    } catch {
      case e: Exception =>
        logger.error("MangoRangeAdd", null, e,
          "error_data", s"cacheid: ${cacheConfig.id} key: $key value: $value",
          "error_message", e.getMessage)
        false
    }
  }


  override def read(key: Any): Option[Map[String, Any]] = {
    try {
      RangeCache.get(convertToRangeKey(key))
    }
    catch {
      case ex: Exception =>
        logger.error("MangoRangeRead", null, ex,
          "error_data", s"cacheid: ${cacheConfig.id} key: $key",
          "error_message", ex.getMessage
        )
        None
    }
  }

  override def remove(key: (Long, Long)): Boolean = {
    try {
      RangeCache -= Range.closed(key._1, key._2)
      true
    }
    catch {
      case e: Exception =>
        logger.error("MangoRangeRemove", null, e,
          "error_data", s"cacheid: ${cacheConfig.id} key: $key",
          "error_message", e.getMessage
        )
        false
    }
  }


  //todo: verify this logic
  override def search(key: Any): Vector[Map[String, Any]] = {
    try {
      val rKey = convertToRangeKey(key)
      val rangeMap = RangeCache.asMapOfRanges().filter(x => {
        val keys = cacheKey(x._2)
        (keys._1 == rKey) || (keys._2 == rKey)
      })
      if (rangeMap.isEmpty) {
        val exitsIP: Option[Map[String, Any]] = read(rKey)
        exitsIP.toVector
      } else {
        rangeMap.map { case (k: Range[Long, Ordering.Long.type], value: Map[String, Any]) =>
          value
        }.toVector
      }
    }
    catch {
      case ex: Exception =>
        logger.error("MangoRangeSearch", null, ex,
          "error_data", s"cacheid: ${cacheConfig.id} key: $key",
          "error_message", ex.getMessage
        )
        Vector()
    }
  }


  override def readAll(): Vector[Map[String, Any]] = {
    try {
      val rangeMap = RangeCache.asMapOfRanges()
      rangeMap.map {
        case (key: Range[Long, Ordering.Long.type], value: Map[String, Any]) => value
      }.toVector
    } catch {
      case e: Exception =>
        logger.error("MangoRangeReadAll", null, e,
          "error_data", s"cacheid: ${cacheConfig.id}",
          "error_message", e.getMessage
        )
        Vector()
    }
  }

  /**
   * Create snapshot of cache
   * Saves snapshot in elastic
   */
  override def dehydrate(): Future[Done] = {
    val dh = if (ConfigLoader.elasticPersistence && cacheConfig.snapshot && getCount > 0) {
      logger.info("RangeSnapshotDehydrate", null, "cacheId", cacheConfig.id,
        "status", "Saved", "count", getCount, "elasticPersistence", ConfigLoader.elasticPersistence, "snapshotFlag", cacheConfig.snapshot)
      val snapshot = encodeCacheSnapshot
      snapshot.flatMap(s =>
        saveSnapshotToElastic(SnaphotDAO(cache_id = cacheConfig.id,
          cache_payload = s,
          date = Some(getElasticDate))))
    }
    else Future {
      logger.info("RangeSnapshotDehydrate", null, "cacheId", cacheConfig.id,
        "status", "NotSaved", "count", getCount, "elasticPersistence", ConfigLoader.elasticPersistence, "snapshotFlag", cacheConfig.snapshot)
      Done
    }
    dh recover { case ex: Exception =>
      logger.error("RangeSnapshotDehydrate", null, ex,
        "error_data", s"cacheid: ${cacheConfig.id}",
        "error_message", ex.getMessage
      )
      Done
    }
  }

  /**
   * Fetch latest snapshot from Elastic
   * Load snapshot in cache
   */
  override def rehydrate(): Future[Done] = loadSnapshotFromElastic

  override def status(): Option[String] = None

  override def metadata(): CacheConfig = cacheConfig

  override def getCount(): Long = RangeCache.asMapOfRanges().size

  var cacheDataQueueRef: Option[SourceQueueWithComplete[ConsumerRecord[String, String]]]= None

  override def getqueueRef(): Option[SourceQueueWithComplete[ConsumerRecord[String, String]]] = cacheDataQueueRef


  override def startCacheStreams(): Unit = {
    cacheDataQueueRef = Some(cacheDataGraph)
    logger.info("mRange_cache_data_streams", null, "StreamStatus", s"Starting data stream for cacheid ${cacheConfig.id}")
  }

  override def stopCacheStreams(): Unit = {
    logger.info("mRange_cache_data_streams", null, "StreamStatus", s"Stopping data stream for cacheid ${cacheConfig.id}")
  }

  /**
   * Fetch cache key from given payload
   */
  private def cacheKey(payload: Map[String, Any]): (Long, Long) = {
    try {
      val key1 = payload.getOrElse(cacheConfig.key(0), throw new Exception("Key1 not found in payload"))
      val key2 = payload.getOrElse(cacheConfig.key(1), throw new Exception("Key2 not found in payload"))
      (convertToRangeKey(key1), convertToRangeKey(key2))
    }
    catch {
      case ex: Exception =>
        logger.error("MangoRangeCacheKey", null, ex,
          "error_data", s"cacheid: ${cacheConfig.id} cannot compute cache key for payload: $payload",
          "error_message", ex.getMessage)
        throw MangoRangeException(s"cacheid: ${cacheConfig.id} cannot compute cache key for payload: $payload", ex.getMessage, ex)
    }
  }

  /**
   * This converts user provided keys to Long
   */
  private def convertToRangeKey(key: Any): Long = {
    try {
      key.toString.toLong
    }
    catch {
      case ex: Exception =>
        throw MangoRangeException(s"cacheid: ${cacheConfig.id}. cannot convert key $key to rangeKey")
    }
  }

  private def encodeCacheSnapshot: Future[String] = {
    Future {
      try {
        val serialization = SerializationExtension(system)
        val serializer = serialization.findSerializerFor(RangeCache.asMapOfRanges())
        val bytes = serializer.toBinary(RangeCache.asMapOfRanges())
        base64Encoder(bytes)
      }
      catch {
        case ex: Exception =>
          logger.error("RangeSnapshotDehydrate", null, ex,
            "error_data", s"cacheid: ${cacheConfig.id}",
            "error_data", s"cacheid: ${cacheConfig.id}")
          throw MangoRangeException(s"Unable to encode range snapshot for cacheid ${cacheConfig.id}", ex.getMessage, ex)
      }
    }
  }

  /**
   * Always override snapshot
   * Maintains one snapshot for one range cache instance
   */
  private def saveSnapshotToElastic(snapshotDAO: SnaphotDAO): Future[Done] = {
    import com.vz.security.sso.models.SprayFormatter.snaphotDAOFormat
    lazy val elasticFlow = Flow[SnaphotDAO].map(r =>
      WriteMessage.createUpsertMessage(id = UUID.randomUUID().toString, source = r))
      .withAttributes(ActorAttributes.supervisionStrategy(flowDecider))

    Source.single(snapshotDAO)
      .via(elasticFlow)
      .runWith(SinkUtil.elasticSink[SnaphotDAO](rangeIndex, rangeType))
  }

  //Used asc to take latest snapshot because alpakka elastic source sending in reverse order
  private def loadSnapshotFromElastic: Future[Done] = {
    import com.vz.security.sso.models.SprayFormatter.snaphotDAOFormat
    val searchParams = Map(
      "query" ->
        s""" {
           |    "term" : { "cache_id" : "${cacheConfig.id}"}
           |  }""".stripMargin,
      "sort" -> """{"date": "desc"}""".stripMargin,
      "size" -> ConfigLoader.scrollSize
    )
    SourceUtil.elasticSourceWithSearchParams[SnaphotDAO](rangeIndex, rangeType, searchParams)
      .take(1) //Take only latest snapshot
      .map(r => {
        logger.info("RangeSnapshotRehydrate", null, "cacheid", cacheConfig.id, "snapshotDate", r._2.date.getOrElse("NoDateFound"))
        loadSnapshotInCache(r._2)
      })
      .runWith(Sink.ignore)
      .recover { case ex: Exception =>
        logger.error("RangeSnapshotRehydrate", null,
          ex, "error_data", s"cacheid: ${cacheConfig.id}", "error_message", ex.getMessage)
        Done
      }
  }

  private def loadSnapshotInCache(snapshotDAO: SnaphotDAO) = {
    try {
      val sn = decodeCacheSnapshot(snapshotDAO)
      logger.info("RangeSnapshotRehydrate", null, "cacheId", cacheConfig.id, "count", sn.size)
      sn.foreach(element => {
        RangeCache.put(element._1, element._2)
      })
    }
    catch
      {
        case ex: Exception =>
          logger.error("RangeSnapshotRehydrate", null, ex,
            "error_data", s"cacheid: ${cacheConfig.id}",
            "error_message", ex.getMessage)
          throw MangoRangeException(s"Unable to load snapshot for cacheid ${cacheConfig.id}", ex.getMessage, ex)
      }
  }

  private def decodeCacheSnapshot(snapshotDAO: SnaphotDAO): Map[Range[Long, Ordering.Long.type], Map[String, Any]] = {
    try {
      val decoded = base64Decoder(snapshotDAO.cache_payload)
      val serialization = SerializationExtension(system)
      val serializer = serialization.findSerializerFor(RangeCache.asMapOfRanges())
      val deserializedData = serializer.fromBinary(decoded, manifest = None)
      deserializedData.asInstanceOf[Map[Range[Long, Ordering.Long.type], Map[String, Any]]]
    }
    catch
      {
        case ex: Exception =>
          logger.error("RangeSnapshotRehydrate", null, ex,
            "error_data", s"cacheid: ${cacheConfig.id}",
            "error_message", ex.getMessage)
          throw MangoRangeException(s"Unable to decode range snapshot for cacheid ${cacheConfig.id}", ex.getMessage, ex)
      }
  }


  /**
   * Graph that operates on cache
   * It generates random kafka groupId to read all messages from topic
   */
  def cacheDataGraph: SourceQueueWithComplete[ConsumerRecord[String, String]] = {
    lazy val processFlow =
      Flow[CacheOp]
        .map(message => {
          message.op match {
            case ADD => add(cacheKey(message.payload), message.payload)
            case REMOVE => remove(cacheKey(message.payload))
            case _ =>
              throw MangoRangeException(s"Range cacheid: ${cacheConfig.id} action ${message.op} cannot be Performed")
          }
        })
        .toMat(Sink.ignore)(Keep.right)
        .withAttributes(ActorAttributes.supervisionStrategy(flowDecider))

    SourceUtil.queueSource[ConsumerRecord[String, String]](1000)
      .via(FlowUtil.FilterKeyFlow(cacheConfig.id))
      .via(FlowUtil.unMarshallFlowCopy[CacheOp](Some(cacheConfig.id)))
      .to(processFlow)
      .withAttributes(ActorAttributes.supervisionStrategy(SupervisionStrategyUtil.systemDecider))
      .run()
  }
}


