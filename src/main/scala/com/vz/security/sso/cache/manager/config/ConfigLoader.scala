package com.vz.security.sso.cache.manager.config

import com.typesafe.config.ConfigFactory
import org.apache.kafka.clients.consumer.ConsumerConfig

import scala.collection.JavaConverters._


object ConfigLoader {

  lazy val config = ConfigFactory.load()
  lazy val kafkaType = config.getString("kafka-type")

  //Kafka properties for old cache manager
  lazy val kafkaAuthProps: Map[String, String] = {
    if (kafkaType.equalsIgnoreCase("JAAS")) {
      System.setProperty("java.security.auth.login.config", config.getString("jaas-config"))
      val secProps = Map(
        "security.protocol" -> "SASL_PLAINTEXT",
        "sasl.mechanism" -> "PLAIN"
      )
      secProps
    }
    else if (kafkaType.equalsIgnoreCase("SSL")) {
      val sslProps = Map("security.protocol" -> "SSL",
        "ssl.truststore.location" -> config.getString("kafka-security-trust-store"),
        "ssl.truststore.password" -> config.getString("kafka-security-trust-store-password"),
        "ssl.keystore.location" -> config.getString("kafka-security-key-store"),
        "ssl.keystore.password" -> config.getString("kafka-security-key-store-password"),
        "ssl.key.password" -> config.getString("kafka-security-key-password"),
        "ssl.truststore.type" -> "JKS",
        "ssl.keystore.type" -> "JKS",
        "ssl.enabled.protocols" -> "TLSv1.2")
      sslProps
    }
    else
      Map()
  }

  //Kafka properties for generic cache manager
  lazy val kafkaProperties: Map[String, String] = {
    val kafkConfig = config.getConfig("kafka")
    val plainKafkaProps = Map(
      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> kafkConfig.getString("AUTO_OFFSET_RESET_CONFIG"),
      ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG -> kafkConfig.getString("ENABLE_AUTO_COMMIT_CONFIG"),
      ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG -> kafkConfig.getString("AUTO_COMMIT_INTERVAL_MS_CONFIG"),
      ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG -> kafkConfig.getString("MAX_POLL_INTERVAL_MS_CONFIG"),
      ConsumerConfig.MAX_POLL_RECORDS_CONFIG -> kafkConfig.getString("MAX_POLL_RECORDS_CONFIG"),
      ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG -> kafkConfig.getString("SESSION_TIMEOUT_MS_CONFIG"),
      ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG -> kafkConfig.getString("HEARTBEAT_INTERVAL_MS_CONFIG"),
      ConsumerConfig.METADATA_MAX_AGE_CONFIG -> kafkConfig.getString("METADATA_MAX_AGE_CONFIG"),
      ConsumerConfig.RECONNECT_BACKOFF_MS_CONFIG -> kafkConfig.getString("RECONNECT_BACKOFF_MS_CONFIG"),
      ConsumerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG -> kafkConfig.getString("CONNECTIONS_MAX_IDLE_MS_CONFIG")
    )
    kafkaAuthProps ++ plainKafkaProps
  }


  lazy val compromisedIpKafkaTopics: Set[String] = config.getStringList("compromised-ip-kafka-topics")
    .asScala.foldLeft(Set[String]()) { (set, topic) => set + topic }
  lazy val bootStrapServers =
    if (kafkaType.equalsIgnoreCase("JAAS")) config.getString("secure-bootstrap-server-host-port")
    else if (kafkaType.equalsIgnoreCase("SSL")) config.getString("secure-ssl-bootstrap-server-host-port")
    else config.getString("bootstrap-server-host-port")

  lazy val statsDHost = config.getString("statsDHost")
  lazy val statsDPort = config.getInt("statsDport")
  lazy val kafkaTopicBlackSet = config.getString("kafka_topic_blackList").split(",").map(_.trim).toSet
  lazy val kafkaTopicConfigManager = config.getString("kafka_topic_config_manager").split(",").map(_.trim).toSet
  lazy val kafkaTopicCacheData = config.getString("kafka_topic_cacheData").split(",").map(_.trim).toSet



  lazy val elasticHost = config.getString("elasticHost")
  lazy val elasticPort = config.getString("elasticPort")
  lazy val elastickafkagroupid = config.getString("elastic-kafka-groupid")

  lazy val defaultEvictionSize = config.getLong("default-eviction-size")
  lazy val defaultEvictionTime = config.getLong("default-eviction-time")
  lazy val cacheLogCountTime = config.getInt("cache_log_count_duration")

  lazy val elasticPersistence = config.getBoolean("elastic-persistence")

  lazy val scrollSize = config.getString("scroll-size")

  lazy val cacheConfigGroupId = config.getString("cache-Config-GroupId")

  /**
   * #####################
   * ### RT SHARD INFO ###
   * #####################
   */
  lazy val shardConfig = config.getConfig("rtsharding")
  lazy val sName = shardConfig.getString("shard-name")
  lazy val sRole = shardConfig.getString("shard-role")
  lazy val numberOfShards = shardConfig.getInt("number-of-shards")

}