package com.vz.security.sso.cache.manager.config


object Constants {

  //Cache Types
  lazy val MANGO = "MANGO"
  lazy val RANGE = "RANGE"

  //Persistence
  lazy val YES = "YES"
  lazy val NO = "NO"

  //EVICTION POLICY
  val sizeEviction = "size"
  val timeEviction = "time"

  //Cache operations
  val ADD = "add"
  val REMOVE = "remove"

}