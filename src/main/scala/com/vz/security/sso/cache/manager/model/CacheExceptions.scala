package com.vz.security.sso.cache.manager.model



import com.vz.security.sso.common.exceptions.CustomExceptions._

object CacheExceptions {

  case class MangoPlainException(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends GenericException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }

  case class MangoRangeException(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends GenericException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }

  case class GCacheManagerException(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends GenericException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }

}
