package com.vz.security.sso.cache.manager.model



import akka.Done
import akka.stream.scaladsl.SourceQueueWithComplete
import com.vz.security.sso.models.CacheConfig
import org.apache.kafka.clients.consumer.ConsumerRecord

import scala.concurrent.Future


/**
 * Parent cache which holds functions that has no parameters
 * This functions are operated from Cache Manager
 */
trait DynamicCache {

  def getCount(): Long

  def readAll(): Vector[Map[String, Any]]

  def status(): Option[String]

  def metadata(): CacheConfig

  def dehydrate(): Future[Done]

  def rehydrate(): Future[Done]

  def read(key: Any): Option[Map[String, Any]]

  def search(key: Any): Vector[Any] //todo: Range returns vector[map] where as plain returns vector[string]

  def startCacheStreams(): Unit

  def stopCacheStreams(): Unit

  def getqueueRef(): Option[SourceQueueWithComplete[ConsumerRecord[String, String]]]
}

/**
 * Parent cache which holds functions that has parameters
 */
trait Cache[T, Q] extends DynamicCache {
  def add(key: T, value: Q): Boolean

  def remove(key: T): Boolean
}

