package com.vz.security.sso.cache.manager.util


import com.vz.security.sso.cache.manager.config.{ConfigLoader, Constants}
import com.vz.security.sso.models.CacheConfig
import java.math.BigInteger
import java.util.Base64
import java.security.MessageDigest
import java.time.{LocalDateTime, ZoneOffset}
import java.time.format.DateTimeFormatter

import com.vz.security.sso.cache.manager.model.CacheExceptions.MangoPlainException

object CacheUtil {

  /**
   * using cacheId as groupId for elastic writes
   */
  def getGroupid(cacheConfig: CacheConfig): String = {
    cacheConfig.id
  }

  /**
   * Returns eviction size defined by user
   * If not defined then returns default size
   */
  def getEvictionSize(cacheConfig: CacheConfig): Long = {
    cacheConfig.eviction_policy match {
      case Some(ep) =>
        if (ep.eviction_type.equalsIgnoreCase(Constants.sizeEviction)) ep.size.getOrElse(ConfigLoader.defaultEvictionSize)
        else ConfigLoader.defaultEvictionSize
      case None => ConfigLoader.defaultEvictionSize
    }
  }

  /**
   * Returns eviction time defined by user
   * If not defined then returns default time
   */
  def getEvictionTime(cacheConfig: CacheConfig): Long = {
    cacheConfig.eviction_policy match {
      case Some(ep) =>
        if (ep.eviction_type.equalsIgnoreCase(Constants.timeEviction)) ep.time.getOrElse(ConfigLoader.defaultEvictionTime)
        else ConfigLoader.defaultEvictionTime
      case None => ConfigLoader.defaultEvictionTime
    }
  }


  lazy val md = MessageDigest.getInstance("MD5")

  /**
   * MD5 hash function producing a 128-bit hash value
   */
  def md5HashString(s: String): String = {
    val digest = md.digest(s.getBytes)
    val bigInt = new BigInteger(1, digest)
    val hashedString = bigInt.toString(16)
    hashedString
  }


  lazy val encoder = Base64.getEncoder

  /**
   * Encodes all bytes from the specified byte array using the Base64 encoding scheme
   */
  def base64Encoder(data: Array[Byte]): String = {
    encoder.encodeToString(data)
  }

  lazy val decoder = Base64.getDecoder

  /**
   * Decodes Base64 encoded string using the Base64 decoding scheme
   */
  def base64Decoder(encodedData: String): Array[Byte] = {
    decoder.decode(encodedData)
  }

  def currentMillis: Long = System.currentTimeMillis()

  val elasticFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")

  def getElasticDate: String = {
    val date = LocalDateTime.now(ZoneOffset.UTC)
    date.format(elasticFormat)
  }

  def cacheKey(payload: Map[String, Any], cacheConfig: CacheConfig): String = {
    cacheConfig.key.headOption match {
      case Some(k) => payload.getOrElse(k, throw MangoPlainException(s"cacheid: ${cacheConfig.id} Key: ${cacheConfig.key} not found in payload: $payload")).toString
      case None => throw MangoPlainException(s"cacheid: ${cacheConfig.id} Key: ${cacheConfig.key} not found in payload: $payload")
    }
  }


}
