package com.vz.security.sso.camunda

/**
  * Created by khana8s on 8/20/2018.
  */

import akka.actor.Actor
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.logging.VZLogger
import org.camunda.bpm.dmn.engine.{DmnDecision, DmnDecisionTableResult}
import org.camunda.bpm.engine.variable.VariableMap

import scala.collection.JavaConverters._

case class ExecuteDMNRules(module: String, variableMap: VariableMap)

case class CamundaResult(result: Map[String, AnyRef])

class CamundaActor extends Actor {
  val defaultCallUserRules: Boolean = false
  private val conf: Config = ConfigFactory.load()
  val RiskScoreName = conf.getString("risk-scores.dmnDefaultRiskScore")
  val ReasonName = conf.getString("risk-scores.dmnDefaultReasonName")

  // This method evaluates the decision and returns the Result.
  def ruleEngineResult(decision: DmnDecision, variableMap: VariableMap): CamundaResult = {
    try {
      val result: DmnDecisionTableResult = RuleEngine.getDmnEngine.evaluateDecisionTable(decision, variableMap)
      CamundaResult(result.get(0).getEntryMap.asScala.toMap.asInstanceOf[Map[String, AnyRef]])
    } catch {
      case ex: Exception =>
        val logger = new VZLogger(this, "camunda_actor_log")
        logger.addKeyValue("error_source", "Camunda Actor, ruleEngineResult method")
        logger.addKeyValue("error_message", ex.getMessage)
        logger.addKeyValue("stack_trace", ex.getStackTrace)
        variableMap.entrySet().forEach(x => logger.addKeyValue(x.getKey, x.getValue))
        logger.error(ex)
        CamundaResult(Map.empty[String, String])
    }
  }

  override def receive: Receive = {
    case ExecuteDMNRules(module, variableMap) =>
      try {
        val decision = RuleEngine.getDecision(module.toLowerCase())
        if(decision.nonEmpty) {
          RuleEngine.getDecisionVarMap(module.toLowerCase()).getOrElse(Map()).foreach(inputvar=>{
            if(!variableMap.containsKey(inputvar._1))
              {
                variableMap.putValue(inputvar._1,null)
              }
          })
          val res = ruleEngineResult(decision.get, variableMap)
          sender() ! res
          val logger = new VZLogger(this, "camunda_actor_log")
          logger.addKeyValue("operation", s"Executing DMN Rules for module :$module")
          res.result.foreach(x => logger.addKeyValue(s"${x._1}", x._2))
          variableMap.entrySet().forEach(x => logger.addKeyValue(s"${x.getKey}", x.getValue))
          logger.info
        } else {
          val defaultCamundaResponse: Map[String, AnyRef] = Map("RiskScoreName" -> RiskScoreName, "ReasonName" -> ReasonName)
          val res = CamundaResult(defaultCamundaResponse)
          sender() ! res
          val logger = new VZLogger(this, "camunda_actor_log")
          logger.addKeyValue("operation", s"Failed to retrieve DMN Decision for module :$module")
          variableMap.entrySet().forEach(x => logger.addKeyValue(s"${x.getKey}", x.getValue))
          logger.info
        }

      } catch {
        case ex: Exception =>
          val logger = new VZLogger(this, "camunda_actor_log")
          logger.addKeyValue("error_source", "Camunda Actor, ExecuteDMNRules case")
          logger.addKeyValue("error_message", ex.getMessage)
          logger.addKeyValue("stack_trace", ex.getStackTrace)
          variableMap.entrySet().forEach(x => logger.addKeyValue(x.getKey, x.getValue))
          logger.error(ex)
          val defaultCamundaResponse: Map[String, AnyRef] = Map("RiskScoreName" -> RiskScoreName, "ReasonName" -> ReasonName)
          CamundaResult(defaultCamundaResponse)
      }

    case _ => // Do Nothing
  }
}