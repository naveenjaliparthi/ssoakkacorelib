package com.vz.security.sso.camunda

import java.io.ByteArrayInputStream
import java.util.UUID

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.routing.FromConfig
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.camunda.elastic.CamundaElasticClient
import com.vz.security.sso.logging.VZLogger
import com.vz.security.sso.utils.ObjectMapperUtil
import org.camunda.bpm.dmn.engine.impl.{DefaultDmnEngineConfiguration, DmnDecisionTableImpl}
import org.camunda.bpm.dmn.engine.{DmnDecision, DmnEngine, DmnEngineConfiguration}
import org.camunda.feel.integration.CamundaFeelEngineFactory

import scala.collection.mutable
import scala.util.{Failure, Success, Try}

/**
 * Created by khana8s on 8/7/2018.
 */

object RuleEngine {

  private val dmnDecisionsMap = new mutable.HashMap[String, DmnDecision]()
  private val dmnAttributeMap = new mutable.HashMap[String, mutable.HashMap[String, Any]]
  private val processedRBTracker = new mutable.HashMap[UUID, Long]()
  private val dmnEngineConfig: DefaultDmnEngineConfiguration = DmnEngineConfiguration.createDefaultDmnEngineConfiguration().asInstanceOf[DefaultDmnEngineConfiguration]
  dmnEngineConfig.setDefaultInputEntryExpressionLanguage("feel")
  private val dmnEngine: DmnEngine = dmnEngineConfig.feelEngineFactory(new CamundaFeelEngineFactory).buildEngine()

  case class RBMetadata(metadata: Map[String, Any])

  private val conf: Config = ConfigFactory.load()
  //This helps to load only cluster specific rulebooks
  val confTargetCluster: Option[String] = if (conf.hasPath("sso.camunda.metadata.targetCluster")) Some(conf.getString("sso.camunda.metadata.targetCluster").toLowerCase) else None
  val metadataLogger = new VZLogger(this, "rule_engine_metadata_log")
  metadataLogger.addKeyValue("confTargetCluster", confTargetCluster)
  metadataLogger.info

  private var system: Option[ActorSystem] = _
  implicit var executionContext = scala.concurrent.ExecutionContext.Implicits.global
  // import scala.concurrent.ExecutionContext.Implicits._
  // system.get.dispatchers.lookup("akka.actor.camunda-dispatcher")

  //dummy method to initialize object
  def run(actorSystem: ActorSystem): Unit = {
    system = Option(actorSystem)
    executionContext = system.get.dispatchers.lookup("akka.actor.camunda-dispatcher")
  }

  def startKafkaListnerActor(): ActorRef = {
    system.get.actorOf(FromConfig.props(Props(new RuleEngineKakfaListener(processedRBTracker))), "reKafkaListnerActor")
  }

  /**
   *
   * @param rbInputConf Rule book's input config
   * @return Decision to process in the deployed cluster
   */
  def processRuleBook(rbInputConf: String): Boolean = {
    val rbTargetCluster: Option[String] = Try {
      val rbMetadata = ObjectMapperUtil.fromJson[RBMetadata](rbInputConf)
      rbMetadata.metadata.getOrElse("targetCluster", "").toString.toLowerCase
    }.toOption
    if (rbTargetCluster.isDefined && confTargetCluster.isDefined)
    //Same rulebook can be used in multiple clusters so using contains API
    rbTargetCluster.get.contains(confTargetCluster.get)
    else true //default setting is allowed
  }

  CamundaElasticClient.getAllRulebooksWithDMN onComplete {
    case Success(rulebookList) =>
      val logger = new VZLogger(this, "rule_engine_object_log")
      rulebookList.foreach(ruleBook => {
        val processRB: Boolean = processRuleBook(ruleBook.inputConfig.get)
        // "sso" is the module name for SSO
        logger.addKeyValue("dmn_retrieved_from_elastic", "yes")
        logger.addKeyValue("dmn_uuid", ruleBook.dmnUUID)
        logger.addKeyValue("dmn_module", ruleBook.module)
        logger.addKeyValue("dmn_name", ruleBook.name)
        logger.addKeyValue("confTargetCluster", confTargetCluster)
        logger.addKeyValue("processRuleBook", processRB)
        logger.info
        if (processRB) {
          processedRBTracker.put(ruleBook.uuid, ruleBook.timestamp)
          val dmnstr = ruleBook.dmnXML.substring(1, ruleBook.dmnXML.length - 1)
          updateDecision(ruleBook.module, dmnstr)
        }
      })
      startKafkaListnerActor()
    case Failure(exception) => {
      new VZLogger(this).error(exception)
      startKafkaListnerActor()
    }
  }

  // this methods reads the DMN file from Kafka and updates the DMN Decision
  def updateDecision(key: String, dmnAsString: String): Boolean = {
    val logger = new VZLogger(this, "rule_engine_object_log")
    try {
      if (Option(dmnAsString).getOrElse("").nonEmpty) {
        val dmnstr = dmnAsString.replaceAll("\\\\", "")

        val decision = dmnEngine.parseDecision("decision", new ByteArrayInputStream(dmnstr.getBytes()))
        dmnDecisionsMap.put(key.toLowerCase, decision)
        val inputMap = new mutable.HashMap[String, Any]
        if (decision.isDecisionTable) {
          try {
            val decisionInputs = decision.getDecisionLogic.asInstanceOf[DmnDecisionTableImpl].getInputs
            decisionInputs.forEach(input => {
              inputMap.put(input.getInputVariable, null)
            })
          }
          catch {
            case e: Exception => {
              logger.addKeyValue("InputVarParseException", e.getMessage)
              logger.info
            }
          }

        }
        dmnAttributeMap.put(key.toLowerCase, inputMap)
        logger.addKeyValue("dmnOperation", s"Successfully updated DMN Decision in Map for : $key")
        logger.info
        true
      } else {
        logger.addKeyValue("dmnOperation", s"Missing DMN file in the message : $key")
        logger.info
        false
      }
    } catch {
      case ex: Exception =>
        val logger = new VZLogger(this)
        logger.addKeyValue("error_message", s"error occured in the UpdateDecision method, for : $key::")
        logger.addKeyValue("error_stackTrace", ex.getStackTrace)
        logger.error(ex)
        false
    }
  }

  def getDecisionVarMap(key: String): Option[mutable.HashMap[String, Any]] = dmnAttributeMap.get(key)

  def getDecision(key: String): Option[DmnDecision] = dmnDecisionsMap.get(key)

  def getDmnEngine: DmnEngine = dmnEngine

  // Removes the DMN decision from HashMap by using a Key.
  def removeDecision(key: String): Boolean = {
    dmnDecisionsMap.remove(key) match {
      case Some(x) =>
        val logger = new VZLogger(this)
        logger.addKeyValue("dmnOperation", s"Successfully deleted DMN Decision from Map for : $key")
        logger.info
        true
      case None => false
    }
    dmnAttributeMap.remove(key) match {
      case Some(x) =>
        val logger = new VZLogger(this)
        logger.addKeyValue("dmnVarMapOperation", s"Successfully deleted DMNVarMap for : $key")
        logger.info
        true
      case None => false
    }

  }

}
