package com.vz.security.sso.camunda

import java.util.UUID

import akka.actor.Actor
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.camunda.kafka.{RulesKafkaConfig, RulesKafkaStreamActor}
import com.vz.security.sso.logging.VZLogger
import com.vz.security.sso.objects.CamundaRule

import scala.collection.mutable
import scala.concurrent.duration._

class RuleEngineKakfaListener (val processedRBTracker: mutable.HashMap[UUID, Long]) extends RulesKafkaStreamActor {

  private val conf: Config = ConfigFactory.load()
  implicit val timeout: Timeout = conf.getInt("ask-timeout").seconds
  override lazy val _config = RulesKafkaConfig(conf.getConfig("sso.camunda.kafka.ruleengine-listner"))

  val logger = new VZLogger(this, "re_kakfa_listener")

  override def process(ruleToProcess: Option[CamundaRule]): Boolean = {
    val ruleBook = ruleToProcess.get
    //process only latest version not retrieved from redis
    if(processedRBTracker.exists(_._1 == ruleBook.uuid)){
      if(ruleBook.timestamp <= processedRBTracker(ruleBook.uuid) ) {
        logger.addKeyValue("operation", s"skippig DMN for module :${ruleBook.module}")
        logger.addKeyValue("rule_uuid", ruleBook.uuid)
        logger.addKeyValue("rule_timestamp", ruleBook.timestamp)
        logger.info
        return false
      }
    }
    val module = ruleBook.module.toLowerCase
    if (ruleToProcess.nonEmpty) {
      try {
        if(ruleToProcess.get.operation.equalsIgnoreCase("delete")) {
          RuleEngine.removeDecision(module)
          logger.addKeyValue("operation", s"removing DMN for module :$module")
          logger.addKeyValue("rule_uuid", ruleBook.uuid)
          logger.addKeyValue("rule_timestamp", ruleBook.timestamp)
          logger.info
        } else {
          RuleEngine.updateDecision(module, ruleBook.dmnXML)
          logger.addKeyValue("operation", s"update DMN for module :$module")
          logger.addKeyValue("rule_uuid", ruleBook.uuid)
          logger.addKeyValue("rule_timestamp", ruleBook.timestamp)
          logger.info
        }
        true
      } catch {
        case ex: Exception =>
          logger.addKeyValue("stack_trace", ex.printStackTrace())
          logger.addKeyValue("error_message", ex.getMessage)
          logger.addKeyValue("error_source", "process method in Camunda Actor")
          logger.error(ex)
          false
      }
    } else false
  }

  override def receive: Receive = Actor.emptyBehavior
}
