package com.vz.security.sso.camunda.kafka

import java.util.UUID

import akka.actor.{Actor, Props}
import akka.pattern.{Backoff, BackoffSupervisor}
import akka.stream.Supervision
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.camunda.elastic.CamundaElasticClient
import com.vz.security.sso.camunda.redis.CamundaRedisClient
import com.vz.security.sso.logging.VZLogger
import com.vz.security.sso.objects.CamundaRule

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}


class RedisRulesKafkaStreamActor(val config: Config) extends RulesKafkaStreamActor {

  override lazy val _config = RulesKafkaConfig(config.getConfig("sso.camunda.kafka.redis-listner"))

  override def process(ruleToProcess: Option[CamundaRule]): Boolean = {
    val logger = new VZLogger(this, "save_to_redis")
    if(ruleToProcess.isEmpty) {
      logger.addKeyValue("rule_uuid", "missing")
      logger.info
      return false
    }

    logger.addKeyValue("rule_timestamp", ruleToProcess.get.timestamp)
    logger.addKeyValue("byUserId", ruleToProcess.get.byUserId)
    logger.addKeyValue("operation", ruleToProcess.get.operation)
    logger.info
    val futureRes = ruleToProcess.get.operation match {
      case "add" =>
        //CamundaRedisClient.addRulebook(ruleToProcess)
        CamundaElasticClient.addRulebook(ruleToProcess)
      case "update" =>
        //CamundaRedisClient.addRulebookVersion(ruleToProcess)
        CamundaElasticClient.addRulebookVersion(ruleToProcess)
      case "remove" =>
        //CamundaRedisClient.deleteRulebook(ruleToProcess)
        CamundaElasticClient.deleteRulebook(ruleToProcess)
      case _ => Future.failed(new UnsupportedOperationException("This operation is not supported"))
    }

    Await.result(futureRes, 3000 millis)
  }

  override def receive = Actor.emptyBehavior
}
