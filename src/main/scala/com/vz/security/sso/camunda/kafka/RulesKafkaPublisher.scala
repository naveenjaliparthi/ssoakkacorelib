package com.vz.security.sso.camunda.kafka
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.kafka.GenerickKafkaPublisher
import com.vz.security.sso.logging.VZLogger
import com.vz.security.sso.models.KakfaPublisherPayload
import com.vz.security.sso.objects.CamundaRule
import io.circe.parser.decode
import io.circe.generic.auto._
import io.circe.syntax._

import scala.concurrent.Future
import scala.collection.JavaConverters._

object RulesKafkaPublisher {

  val conf: Config = ConfigFactory.load()
  val rulesKafkaConf: Config = conf.getConfig("sso.camunda.kafka")
  val topicsList = rulesKafkaConf.getStringList("topic").asScala.toList
  val topicWithKey = rulesKafkaConf.getBoolean("topicWithKey")
  val kafkaList = rulesKafkaConf.getStringList("instances").asScala.toList

  def publishToKafka(rule: Option[CamundaRule]): Future[Boolean] = {
    if(rule.isEmpty){
      return Future.failed(new NullPointerException("Rule is missing"))
    }

    val vzlogger = new VZLogger(this, "rules_kafka_publish")
    vzlogger.addKeyValue("ruleVersion", rule.get.timestamp)
    vzlogger.addKeyValue("ruleUUID", rule.get.uuid)
    vzlogger.addKeyValue("dmnUUID", rule.get.dmnUUID)
    vzlogger.info

    val payload = rule.get.uuid + ":" + rule.get.asJson.noSpaces
    val payloadList: List[String] = List(payload)
    val kafkaRequest = Option(KakfaPublisherPayload(topicsList, kafkaList, topicWithKey, payloadList))
    val future = GenerickKafkaPublisher.publishPayload(kafkaRequest)
    future

  }


}
