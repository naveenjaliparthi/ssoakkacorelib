package com.vz.security.sso.common.actors

import akka.actor.{Actor, Props}
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._
import scala.concurrent.duration._

class VZSupervisorStrategy extends Actor {

  override val supervisorStrategy =
    OneForOneStrategy(maxNrOfRetries = -1, withinTimeRange = Duration.Inf) {
      case _: ArithmeticException      => Resume
      case _: NullPointerException     => Restart
      case _: Exception                => Resume
    }

  def receive: Receive = {
    case p: Props => sender() ! context.actorOf(p)
  }
}
