package com.vz.security.sso.common.config

import com.typesafe.config.ConfigFactory

object ConfigLoaderCore {

  lazy val config = ConfigFactory.load()

  //Reads from client config file
  lazy val grpcEnabled = if (config.hasPath("grpc.enabled")) config.getBoolean("grpc.enabled") else false

  lazy val elasticSinkBufferSize = config.getInt("elastic-stream.sinkBufferSize")
  lazy val elasticSinkMaxRetries = config.getInt("elastic-stream.sinkMaxRetries")
  lazy val elasticSinkRetryIntervalInSec = config.getInt("elastic-stream.sinkRetryIntervalInSec")

  lazy val minBackoff = config.getInt("akka-stream.minBackoff")
  lazy val maxBackoff = config.getInt("akka-stream.maxBackoff")
  lazy val randomFactor = config.getDouble("akka-stream.randomFactor")
  lazy val wakeupTimeout = config.getInt("akka-stream.wakeupTimeout")
  lazy val maxWakeups = config.getInt("akka-stream.maxWakeups")
  lazy val producerParallelism = config.getInt("akka-stream.producerParallelism")
  lazy val parallelism = config.getInt("akka-stream.parallelism")
  lazy val producerCloseTimeoutInSec = config.getInt("akka-stream.producerCloseTimeoutInSec")

  lazy val callFlowConfig = config.getConfig("call-flow")
  lazy val elasticConfig = config.getConfig("elastic")
  lazy val askTimeout = callFlowConfig.getLong("askTimeout")

  lazy val VCVS = callFlowConfig.getString("vcvs")
  lazy val ETNI = callFlowConfig.getString("etni")
  lazy val MODELHOST = callFlowConfig.getString("modelhost")
  lazy val KAFKA = callFlowConfig.getString("kafka")
  lazy val GATEWAY = callFlowConfig.getString("gateway")
  lazy val INITIALRISKSCORE = callFlowConfig.getString("initialRiskScore")
  lazy val GENERICRISKSCORE = callFlowConfig.getString("genericRiskScore")
  lazy val PERSISTENCESHARD = callFlowConfig.getString("persistenceShard")
  lazy val AAPERSISTENCESHARD = callFlowConfig.getString("aaPersistenceShard")
  lazy val RTSTREAMSHARD = callFlowConfig.getString("rtStreamShard")
  lazy val AARTSTREAMSHARD = callFlowConfig.getString("aaRTStreamShard")

  lazy val route = callFlowConfig.getString("route")
  lazy val etniActorPath = callFlowConfig.getString("etni-actor-path")
  lazy val vcvsActorPath = callFlowConfig.getString("vcvs-actor-path")
  lazy val mlActorPath = callFlowConfig.getString("ml-actor-path")
  lazy val rulesAppPath = callFlowConfig.getString("rules-app-path")
  lazy val genericRulesActorPath  = callFlowConfig.getString("generic-rules-actor-path")
  lazy val servicesRole = callFlowConfig.getString("role")
  lazy val mlRole = callFlowConfig.getString("mlRole")
  lazy val staticRole = callFlowConfig.getString("staticRole")
  lazy val gatewayKey = callFlowConfig.getString("gatewayKey")
  lazy val gatewayValueList = callFlowConfig.getString("gatewayValue").split(",").map(_.trim).toList
  lazy val etniKey = callFlowConfig.getString("etniKey")
  lazy val etniValueList = callFlowConfig.getString("etniValue").split(",").map(_.trim).toList
  lazy val etniExpression = callFlowConfig.getString("etniExpression")
  lazy val vcvsKey = callFlowConfig.getString("vcvsKey")
  lazy val vcvsvalueList = callFlowConfig.getString("vcvsvalue").split(",").map(_.trim).toList

  lazy val totalNoOfInstancesEtni = callFlowConfig.getInt("total-instances-etni")
  lazy val localroutesetni = callFlowConfig.getBoolean("allow-Local-Routees-etni")

  lazy val totalNoOfInstancesvcvs = callFlowConfig.getInt("total-instances-vcvs")
  lazy val localroutesvcvs = callFlowConfig.getBoolean("allow-Local-Routees-vcvs")

  lazy val totalNoOfInstancesMl = callFlowConfig.getInt("total-instances-ml")
  lazy val localroutesMl = callFlowConfig.getBoolean("allow-Local-Routees-ml")

  lazy val totalNoOfInstancesStatic = callFlowConfig.getInt("total-instances-static")
  lazy val localroutesStatic = callFlowConfig.getBoolean("allow-Local-Routees-static")

  lazy val wfConfig = config.getConfig("work-flow")

  lazy val workFlowTopic = wfConfig.getString("intopic").split(",").map(_.trim).toSet
  lazy val kafkaTopicsKey = wfConfig.getString("kafkaTopicsKey")
  lazy val rtQIdKey = wfConfig.getString("rtQIdKey")
  lazy val rtShardKey = wfConfig.getString("rtShardKey")
  lazy val rtParKey = wfConfig.getString("rtParKey")
  lazy val wfLogTimeInMin = wfConfig.getInt("wf-log-timer")
  lazy val wfAskTimeout = wfConfig.getLong("askTimeout")


  //Persistence Actor Proxy
  lazy val genericShardName: String = wfConfig.getString("generic-shard-name")
  lazy val numberOfGenericShards: Int = wfConfig.getInt("number-of-generic-shards")
  lazy val genericShardRole = Some(wfConfig.getString("generic-shard-role"))

  //RT stream Actor Proxy
  lazy val rtShardName: String = wfConfig.getString("rt-shard-name")
  lazy val numberOfRTShards: Int = wfConfig.getInt("number-of-rt-shards")
  lazy val rtShardRole = Some(wfConfig.getString("rt-shard-role"))


  lazy val elastichttpsSupport = elasticConfig.getBoolean("enable-https")
  lazy val elasticHost = elasticConfig.getString("host")
  lazy val elasticPort = elasticConfig.getString("port")
  lazy val wfgIndex = elasticConfig.getString("wfgindex")
  lazy val wfgType = elasticConfig.getString("wfgtype")


  lazy val opRegex = config.getString("toolbox.opRegex").stripMargin
  lazy val exprRegex = config.getString("toolbox.exprRegex").stripMargin

}
