package com.vz.security.sso.common.connectors

import com.typesafe.config.ConfigFactory
import com.vz.security.sso.common.config.ConfigLoaderCore
import javax.net.ssl.SSLContext
import org.apache.http.HttpHost
import org.elasticsearch.client.RestClient

object ElasticConnector {
  //These will be passed from client config file
  lazy val config = ConfigFactory.load()
  lazy val elasticHost = config.getString("elastic.host")
  lazy val elasticPort = config.getInt("elastic.port")
  implicit lazy val client: RestClient =
    if(ConfigLoaderCore.elastichttpsSupport){
      RestClient.builder(new HttpHost(elasticHost, elasticPort, "https"))
        .setHttpClientConfigCallback(httpClientBuilder => {
          val httpAsyncClientBuilder = httpClientBuilder.setSSLContext(SSLContext.getDefault)
          httpAsyncClientBuilder
        })
        .build()
    }
    else {
      RestClient.builder(new HttpHost(elasticHost, elasticPort)).build()
    }

}
