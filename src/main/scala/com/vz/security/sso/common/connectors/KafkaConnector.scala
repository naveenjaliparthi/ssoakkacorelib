package com.vz.security.sso.common.connectors

import akka.actor.ActorSystem
import akka.kafka.{ConsumerSettings, ProducerSettings}
import com.vz.security.sso.common.config.ConfigLoaderCore
import org.apache.kafka.common.serialization.{Deserializer, Serializer}

import scala.concurrent.duration._

object KafkaConnector {

  def configKafkaProducerSettings[K, V](
                                         _bootStrapServers: String,
                                         _keySerializer: Serializer[K],
                                         _valueSerializer: Serializer[V],
                                         _props: Option[Map[String, String]])(
                                         implicit _system: ActorSystem
                                       ): ProducerSettings[K, V] = {
    val prodSettings = ProducerSettings(system = _system, _keySerializer, _valueSerializer)
      .withBootstrapServers(_bootStrapServers)
      .withParallelism(ConfigLoaderCore.producerParallelism)
      .withCloseTimeout(ConfigLoaderCore.producerCloseTimeoutInSec.seconds)
    if (_props.isDefined && _props.get.nonEmpty) prodSettings.withProperties(_props.get)
    else prodSettings
  }

  /**
    * Only consumer properties are passed through this functions
    * Rest all consumer config are passed through akka configuration
    */
  def configkafkaConsumerSettings[K, V](
                                         _bootStrapServers: String,
                                         _groupId: String,
                                         _keySerializer: Deserializer[K],
                                         _valueSerializer: Deserializer[V],
                                         _props: Map[String, String])(implicit _system: ActorSystem): ConsumerSettings[K, V] =
    ConsumerSettings(system = _system, _keySerializer, _valueSerializer)
      .withBootstrapServers(_bootStrapServers)
      .withGroupId(_groupId)
      .withProperties(_props)
      .withWakeupTimeout(ConfigLoaderCore.wakeupTimeout.seconds)
      .withMaxWakeups(ConfigLoaderCore.maxWakeups)
}
