package com.vz.security.sso.common.exceptions

object CustomExceptions {

  lazy val concatStr = ". ExceptionMessage: "

  trait ObjectMapperException extends Exception {
    val message: String
    val cause: Throwable
  }

  case class UnmarshallException(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends ObjectMapperException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }

  case class MarshallException(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends ObjectMapperException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }

  trait GenericException extends Exception {
    val message: String
    val cause: Throwable
  }

  case class MangoException(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends GenericException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }

  case class SlidingWindowing(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends GenericException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }


  trait GrpcException extends Exception {
    val message: String
    val cause: Throwable
  }

  case class GrpcClientException(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends GrpcException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }

  trait HarmlessException extends Exception {
    val message: String
    val cause: Throwable
  }

  case class IDGenerationException(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends HarmlessException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }

  /**
    * Stack trace as a string
    * This can be embedded into logs
    */
  def stacktraceAsString(e: Exception) = {
    import java.io.StringWriter
    import java.io.PrintWriter
    val sw = new StringWriter()
    val pw = new PrintWriter(sw)
    e.printStackTrace(pw)
    sw.toString
  }

}