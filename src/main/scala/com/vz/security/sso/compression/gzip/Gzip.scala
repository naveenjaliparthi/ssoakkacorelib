package com.vz.security.sso.compression.gzip

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}
import java.util.zip.{GZIPInputStream, GZIPOutputStream}

import akka.util.{ByteString, ByteStringBuilder}
import org.apache.commons.io.IOUtils
import com.google.protobuf

import scala.util.{Failure, Success, Try}

object Gzip {

  def compressProto(bytes: protobuf.ByteString): protobuf.ByteString = {
    val builder = protobuf.ByteString.newOutput()
    val zipOutputStream = new GZIPOutputStream(builder)
    bytes.writeTo(zipOutputStream)
    zipOutputStream.close()
    builder.toByteString
  }

  def decompressProto(bytes: protobuf.ByteString): protobuf.ByteString = {
    val zipInputStream = new GZIPInputStream(bytes.newInput())
    val result = protobuf.ByteString.readFrom(zipInputStream)
    result
  }

  def compressBytes(input: Array[Byte]): Array[Byte] = {
    val bos = new ByteArrayOutputStream(input.length)
    val gzip = new GZIPOutputStream(bos)
    gzip.write(input)
    gzip.close()
    val compressed = bos.toByteArray
    bos.close()
    compressed
  }

  def decompressBytes(compressed: Array[Byte]): Array[Byte] = {
    val outStream = new ByteArrayOutputStream()
    Try {
      IOUtils.copy(new GZIPInputStream(new ByteArrayInputStream(compressed)), outStream)
    } match {
      case Success(_) => outStream.toByteArray
      case Failure(error) => throw new RuntimeException(error)
    }
  }

  def compressAkkaByteString(bytes: ByteString): ByteString = {
    val builder = new ByteStringBuilder
    val zipOutputStream = new GZIPOutputStream(builder.asOutputStream)
    IOUtils.copy(bytes.iterator.asInputStream, zipOutputStream)
    zipOutputStream.close()
    builder.result
  }

  def decompressAkkaByteString(bytes: ByteString): ByteString = {
    val builder = new ByteStringBuilder
    val zipInputStream = new GZIPInputStream(bytes.iterator.asInputStream)
    IOUtils.copy(zipInputStream, builder.asOutputStream)
    builder.asOutputStream.close()
    builder.result
  }
}
