package com.vz.security.sso.config

import akka.kafka.ConsumerSettings
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.camunda.kafka.RulesKafkaConfig
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer

import scala.collection.JavaConverters._


object KafkaConfigLoader {

  val config = ConfigFactory.load()
  lazy val kafkaType = config.getString("kafka-consumer-type")

  lazy val kafkaAuthProps: Map[String, String] = kafkaType match {
    case "JAAS" => {
      val secProps = Map(
        "security.protocol" -> "SASL_PLAINTEXT",
        "sasl.mechanism" -> "PLAIN",
        "sasl.jaas.config" -> config.getString("jaas-config-fe")
      )
      secProps
    }
    case "SSL" => {
      val sslProps = Map("security.protocol" -> "SSL",
        "ssl.truststore.location" -> config.getString("kafka-security-trust-store"),
        "ssl.truststore.password" -> config.getString("kafka-security-trust-store-password"),
        "ssl.keystore.location" -> config.getString("kafka-security-key-store"),
        "ssl.keystore.password" -> config.getString("kafka-security-key-store-password"),
        "ssl.key.password" -> config.getString("kafka-security-key-password"),
        "ssl.truststore.type" -> "JKS",
        "ssl.keystore.type" -> "JKS",
        "ssl.enabled.protocols" -> "TLSv1.2")
      sslProps
    }
    case "PLAIN" => Map()
  }


  lazy val kafkaProps: Map[String, String] = {
    val kafkConfig = config.getConfig("kafka")
    val plainKafkaProps = Map(
      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> kafkConfig.getString("AUTO_OFFSET_RESET_CONFIG"),
      ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG -> kafkConfig.getString("ENABLE_AUTO_COMMIT_CONFIG"),
      ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG -> kafkConfig.getString("AUTO_COMMIT_INTERVAL_MS_CONFIG"),
      ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG -> kafkConfig.getString("MAX_POLL_INTERVAL_MS_CONFIG"),
      ConsumerConfig.MAX_POLL_RECORDS_CONFIG -> kafkConfig.getString("MAX_POLL_RECORDS_CONFIG"),
      ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG -> kafkConfig.getString("SESSION_TIMEOUT_MS_CONFIG"),
      ConsumerConfig.HEARTBEAT_INTERVAL_MS_CONFIG -> kafkConfig.getString("HEARTBEAT_INTERVAL_MS_CONFIG"),
      ConsumerConfig.METADATA_MAX_AGE_CONFIG -> kafkConfig.getString("METADATA_MAX_AGE_CONFIG"),
      ConsumerConfig.RECONNECT_BACKOFF_MS_CONFIG -> kafkConfig.getString("RECONNECT_BACKOFF_MS_CONFIG"),
      ConsumerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG -> kafkConfig.getString("CONNECTIONS_MAX_IDLE_MS_CONFIG")
    )
    kafkaAuthProps ++ plainKafkaProps
  }

  lazy val bootStrapServers = kafkaType match {
    case "JAAS" => config.getString("secure-bootstrap-server-host-port")
    case "SSL" => config.getString("secure-ssl-bootstrap-server-host-port")
    case "PLAIN" => config.getString("bootstrap-server-host-port")
  }

  def getConsumerSettings(kafkaConfig: RulesKafkaConfig) : ConsumerSettings[String, String] = {
    val consumerConfig: Config = config.getConfig("akka.kafka.consumer")
    kafkaType match {
      case "JAAS" => getJaasSettings(consumerConfig, kafkaConfig)
      case "SSL" => getSSLSettings(consumerConfig, kafkaConfig)
      case "PLAIN" => getPlainSettings(consumerConfig, kafkaConfig)
    }
  }
  def getPlainSettings(consumerConfig: Config, kafkaConfig: RulesKafkaConfig) : ConsumerSettings[String, String] = {
    val consumerSettings: ConsumerSettings[String, String] =
      ConsumerSettings(consumerConfig, new StringDeserializer, new StringDeserializer)
        .withBootstrapServers(bootStrapServers)
        .withGroupId(kafkaConfig.groupId) //as groupId
        .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, kafkaConfig.offset)
        .withProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
        .withProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000")
        .withProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
        .withProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
    consumerSettings
  }

  def getJaasSettings(consumerConfig: Config, kafkaConfig: RulesKafkaConfig) : ConsumerSettings[String, String] = {
    val consumerSettings: ConsumerSettings[String, String] =
      ConsumerSettings(consumerConfig, new StringDeserializer, new StringDeserializer)
        .withBootstrapServers(bootStrapServers)
        .withGroupId(kafkaConfig.groupId) //as groupId
        .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, kafkaConfig.offset)
        .withProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
        .withProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000")
        .withProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
        .withProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
        .withProperty("security.protocol", kafkaProps("security.protocol"))
        .withProperty("sasl.mechanism", kafkaProps("sasl.mechanism"))
        .withProperty("sasl.jaas.config", kafkaProps("sasl.jaas.config"))
    consumerSettings
  }

  def getSSLSettings(consumerConfig: Config, kafkaConfig: RulesKafkaConfig) : ConsumerSettings[String, String] = {
    val consumerSettings: ConsumerSettings[String, String] =
      ConsumerSettings(consumerConfig, new StringDeserializer, new StringDeserializer)
        .withBootstrapServers(bootStrapServers)
        .withGroupId(kafkaConfig.groupId) //as groupId
        .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, kafkaConfig.offset)
        .withProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
        .withProperty(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, "1000")
        .withProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
        .withProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer")
        .withProperty("security.protocol" , kafkaProps("security.protocol"))
        .withProperty("ssl.truststore.location" , kafkaProps("ssl.truststore.location"))
        .withProperty("ssl.truststore.password" , kafkaProps("ssl.truststore.password"))
        .withProperty("ssl.keystore.location" , kafkaProps("ssl.keystore.location"))
        .withProperty("ssl.keystore.password" , kafkaProps("ssl.keystore.password"))
        .withProperty("ssl.key.password" , kafkaProps("ssl.key.password"))
        .withProperty("ssl.truststore.type" , kafkaProps("ssl.truststore.type"))
        .withProperty("ssl.keystore.type" , kafkaProps("ssl.keystore.type"))
        .withProperty("ssl.enabled.protocols" , kafkaProps("ssl.enabled.protocols"))
    consumerSettings
  }

}
