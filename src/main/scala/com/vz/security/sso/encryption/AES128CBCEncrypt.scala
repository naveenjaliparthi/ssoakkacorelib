package com.vz.security.sso.encryption

import java.security.SecureRandom
import java.security.spec.InvalidKeySpecException

import javax.crypto.spec.{IvParameterSpec, PBEKeySpec, SecretKeySpec}
import javax.crypto.{BadPaddingException, Cipher, SecretKeyFactory}
import org.apache.commons.codec.binary.Base64
import org.apache.commons.logging.LogFactory

class AES128CBCEncrypt(encryptionKey: String, iterationCount: Int = 65536) extends Encryption {

  val IV_ALGORITHM = "SHA1PRNG"
  val UNIQUE_FACTORY_ALGORITHM = "PBKDF2WithHmacSHA1"
  val UNIQUE_ALGORITHM = "AES"
  val ALGORITHM = "HmacSHA1"
  val IV_SIZE = 16
  val SALT_SIZE = 32
  val KEY_SIZE = 128
  val SALT_STRING = "S@1tS@1t"
  val PROVIDER = "AES/CBC/PKCS5Padding"

  private val LOGGER = LogFactory.getLog(classOf[AES128CBCEncrypt])
  private val secretKey = new SecretKeySpec(Base64.decodeBase64(encryptionKey), ALGORITHM)
  private val secretKeyFactory = SecretKeyFactory.getInstance(UNIQUE_FACTORY_ALGORITHM)
  private val secureRandom = SecureRandom.getInstance(IV_ALGORITHM)
  private val cipher = Cipher.getInstance(PROVIDER)


  override def encrypt(clearText: String): Option[String] = try {
    if (LOGGER.isDebugEnabled) LOGGER.debug("clearText: " + clearText)
    val salt = generateSalt
    val sk = generateUniqueKey(salt)
    val iv = generateIV
    val ivs = new IvParameterSpec(iv)
    val textEncrypted = this.synchronized {
      cipher.init(Cipher.ENCRYPT_MODE, sk, ivs)
      cipher.doFinal(clearText.getBytes)
    }

    if (LOGGER.isDebugEnabled) LOGGER.debug("encrypted (inner): " + Base64.encodeBase64String(textEncrypted))
    // but 3 things together:  salt + hash + encrypted data
    val cipherText = new Array[Byte](SALT_SIZE + IV_SIZE + textEncrypted.length)
    System.arraycopy(salt, 0, cipherText, 0, SALT_SIZE)
    System.arraycopy(iv, 0, cipherText, SALT_SIZE, IV_SIZE)
    System.arraycopy(textEncrypted, 0, cipherText, SALT_SIZE + IV_SIZE, textEncrypted.length)
    if (LOGGER.isDebugEnabled) LOGGER.debug("salt size: " + salt.length + ", iv size: " + iv.length + ", encrypted: " + textEncrypted.length)
    val finalEncrypted = Base64.encodeBase64String(cipherText)
    if (LOGGER.isDebugEnabled) LOGGER.debug("final encrypted: " + finalEncrypted)
    Some(finalEncrypted)
  } catch {
    case t: Throwable =>
      LOGGER.fatal(t.getMessage, t)
      None
  }


  private def generateSalt = {
    val random = new SecureRandom
    val salt = new Array[Byte](SALT_SIZE)
    random.nextBytes(salt)
    salt
  }

  @throws[Exception]
  private def generateIV = {
    var byteIV = new Array[Byte](IV_SIZE)
    byteIV = secureRandom.generateSeed(IV_SIZE)
    byteIV
  }

  def decrypt(base64encrypted: String): Option[String] = {
    try {
      val decodedBytes = Base64.decodeBase64(base64encrypted.getBytes)
      if (decodedBytes.length <= SALT_SIZE + IV_SIZE) { // something is wrong if the size of the encrypted string is equal or smaller than the IV and SALT by itself...
        //if (LOGGER.isDebugEnabled) LOGGER.debug("invalid encrypt string: " + base64encrypted)
        None
      }
      val salt = decodedBytes.slice(0, SALT_SIZE)
      val iv = decodedBytes.slice(SALT_SIZE, SALT_SIZE + IV_SIZE)
      val cipherText = decodedBytes.slice(SALT_SIZE + IV_SIZE, decodedBytes.length)
      if ((cipherText.length % 16) != 0) { // with padded cipher length has to be multiple of 16
        if (LOGGER.isDebugEnabled) LOGGER.debug("invalid encrypt string: " + base64encrypted)
        None
      }
      if (LOGGER.isDebugEnabled) LOGGER.debug("salt size: " + salt.length + ", iv size: " + iv.length + ", encrypted: " + cipherText.length)
      val sk = generateUniqueKey(salt)
      val ivspec = new IvParameterSpec(iv)
      val textDecrypted = this.synchronized {
        cipher.init(Cipher.DECRYPT_MODE, sk, ivspec)
        cipher.doFinal(cipherText)
      }
      Some(new String(textDecrypted))
    } catch {
      case ignore: BadPaddingException =>
        if (LOGGER.isDebugEnabled) LOGGER.debug("invalid encrypted string: " + base64encrypted)
        None
      case t: Throwable =>
        LOGGER.fatal(t.getMessage, t)
        None
    }
  }

  @throws[Exception]
  private def generateUniqueKey(salt: Array[Byte]) = {
    try {
      val key = Base64.encodeBase64String(this.secretKey.getEncoded)
      val spec = new PBEKeySpec(key.toCharArray, salt, this.iterationCount, KEY_SIZE)
      val tmp = this.synchronized {
        secretKeyFactory.generateSecret(spec)
      }

      val encoded = tmp.getEncoded
      new SecretKeySpec(encoded, UNIQUE_ALGORITHM)
    } catch {
      case e: InvalidKeySpecException =>
        // should never happen...
        LOGGER.fatal(e.getMessage, e)
        throw new Exception(e.getMessage)
      //throw new OutOfServiceException(e.getMessage)
    }
  }

}
