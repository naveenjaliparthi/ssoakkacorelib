package com.vz.security.sso.encryption

import java.security.spec.InvalidKeySpecException
import java.security.{MessageDigest, SecureRandom}
import java.util

import com.vz.security.sso.logging.VZMapLogger
import javax.crypto._
import javax.crypto.spec.{IvParameterSpec, PBEKeySpec, SecretKeySpec}
import org.apache.commons.codec.binary.Base64

import scala.collection.mutable

object AES256Encrypt {
  private val LOGGER = new VZMapLogger(this)
  private val PROVIDER = "AES/CBC/PKCS5Padding"
  private val IV_ALGORITHM = "SHA1PRNG"
  private val UNIQUE_FACTORY_ALGORITHM = "PBKDF2WithHmacSHA1"
  private val UNIQUE_ALGORITHM = "AES"
  private val ALGORITHM = "HmacSHA1"
  private val IV_SIZE = 16
  private val SALT_SIZE = 32
  private val HMAC_SIZE = 20
  private val KEY_SIZE = 256
  private val ITERATION_COUNT = 65536

  def apply(_encryptionKey: String, _macEncryptionKey: String, _iterationCount: Int): AES256Encrypt = new AES256Encrypt(_encryptionKey, _macEncryptionKey, _iterationCount)

}

import com.vz.security.sso.encryption.AES256Encrypt._

class AES256Encrypt(_encryptionKey: String, _macEncryptionKey: String, _iterationCount: Int) extends Encrypt {
  // Used for encrypting the clear text
  private val secretKey = new SecretKeySpec(Base64.decodeBase64(_encryptionKey), ALGORITHM)
  // Used for signing the final encrypted string encrypting salt/iv/encrypteddata/hmac
  private val signingKey = new SecretKeySpec(Base64.decodeBase64(_macEncryptionKey), ALGORITHM)
  private val mac = Mac.getInstance(ALGORITHM)
  mac.init(this.signingKey)
  private val secretKeyFactory = SecretKeyFactory.getInstance(UNIQUE_FACTORY_ALGORITHM)
  private val secureRandom = SecureRandom.getInstance(IV_ALGORITHM)
  private val cipher = Cipher.getInstance(PROVIDER)
  private val iterationCount = _iterationCount

  override def encrypt(clearText: String): String =
    try {
      val logMap = mutable.Map[String, Any]()
      logMap += ("clearText" -> clearText)
      val salt = generateSalt
      val sk = generateUniqueKey(salt)
      val iv = generateIV
      val ivs = new IvParameterSpec(iv)
      // encrypted the clear text
      var textEncrypted: Array[Byte] = null
      cipher.synchronized {
        cipher.init(Cipher.ENCRYPT_MODE, sk, ivs)
        textEncrypted = cipher.doFinal(clearText.getBytes)
      }
      logMap += ("encrypted_inner)" -> Base64.encodeBase64String(textEncrypted))
      // but 3 things together:  salt + hash + encrypted data
      val cipherText = new Array[Byte](SALT_SIZE + IV_SIZE + textEncrypted.length)
      System.arraycopy(salt, 0, cipherText, 0, SALT_SIZE)
      System.arraycopy(iv, 0, cipherText, SALT_SIZE, IV_SIZE)
      System.arraycopy(textEncrypted, 0, cipherText, SALT_SIZE + IV_SIZE, textEncrypted.length)
      // create a signature based upon cipherText
      var rawHmac: Array[Byte] = null
      mac.synchronized(rawHmac = mac.doFinal(cipherText))
      logMap += ("signature" -> clearText)
      logMap += ("saltSize" -> salt.length)
      logMap += ("ivSize" -> iv.length)
      logMap += ("encrypted" -> textEncrypted.length)
      logMap += ("signatureSize" -> rawHmac.length)
      // return cipherText + signature
      val finalCipherText = new Array[Byte](cipherText.length + HMAC_SIZE)
      System.arraycopy(cipherText, 0, finalCipherText, 0, cipherText.length)
      System.arraycopy(rawHmac, 0, finalCipherText, cipherText.length, rawHmac.length)
      val finalEncrypted = Base64.encodeBase64String(finalCipherText)
      logMap += ("encrypted_final)" -> finalEncrypted)
      LOGGER.debug("encrypt", null, logMap)
      finalEncrypted
    } catch {
      case t: Throwable =>
        val logMap = mutable.Map[String, Any]()
        logMap += ("clearText" -> clearText)
        logMap += ("errorName" -> t.getMessage)
        logMap += ("stackTrace" -> t.getStackTrace.mkString("\r\n\t"))
        LOGGER.debug("encryptError", null, logMap)
        null
    }

  override def decrypt(base64encrypted: String): String = decrypt(base64encrypted, false)

  def decrypt(base64encrypted: String, suppressError: Boolean): String = {
    val logMap = mutable.Map[String, Any]()
    try {
      logMap += ("suppressError" -> suppressError)
      logMap += ("base64encrypted" -> base64encrypted)
      val decodedBytes = Base64.decodeBase64(base64encrypted.getBytes)
      if (decodedBytes.length <= SALT_SIZE + IV_SIZE + HMAC_SIZE) { // something is wrong if the size of the encrypted string is equal or smaller than the IV and SALT and HMAC by itself...
        if (!suppressError) {
          logMap += ("invalid encrypt string, invalid decoded byte length:" -> base64encrypted)
          LOGGER.debug("decrypt", null, logMap)
        }
        else {
          logMap += ("invalid encrypt string, invalid decoded byte length:" -> base64encrypted)
          LOGGER.debug("decrypt", null, logMap)
        }
        ""
      }
      else {
        val salt = util.Arrays.copyOfRange(decodedBytes, 0, SALT_SIZE)
        val iv = util.Arrays.copyOfRange(decodedBytes, SALT_SIZE, SALT_SIZE + IV_SIZE)
        val cipherText = util.Arrays.copyOfRange(decodedBytes, SALT_SIZE + IV_SIZE, decodedBytes.length - HMAC_SIZE)
        // digest is computed during encryption on salt+iv+ciphertext = fulltext
        val fullText = util.Arrays.copyOfRange(decodedBytes, 0, decodedBytes.length - HMAC_SIZE)
        val hmacDigest = util.Arrays.copyOfRange(decodedBytes, decodedBytes.length - HMAC_SIZE, decodedBytes.length)
        logMap += ("saltSize" -> salt.length)
        logMap += ("ivSize" -> iv.length)
        logMap += ("encrypted" -> cipherText.length)
        logMap += ("signatureSize" -> hmacDigest.length)
        if ((cipherText.length % 16) != 0) { // with padded cipher length has to be multiple of 16
          if (!suppressError) {
            logMap += ("invalid encrypt string, invalid decoded byte length:" -> base64encrypted)
            LOGGER.debug("decrypt", null, logMap)
          }
          else {
            logMap += ("invalid encrypt string, invalid decoded byte length:" -> base64encrypted)
            LOGGER.debug("decrypt", null, logMap)
          }
          ""
        }
        else {
          // Generate the signature again based upon cipher text
          var newHmac: Array[Byte] = null
          mac.synchronized(newHmac = mac.doFinal(fullText))

          // if it doesn't match we have an issue
          if (!MessageDigest.isEqual(newHmac, hmacDigest)) {
            if (!suppressError) {
              logMap += ("reason" -> "signature doesn't match")
              LOGGER.debug("decrypt", null, logMap)
            }
            else {
              logMap += ("reason" -> "signature doesn't match")
              LOGGER.debug("decrypt", null, logMap)
            }
            ""
          }
          else {
            // decrypt
            val sk = generateUniqueKey(salt)
            val ivspec = new IvParameterSpec(iv)

            var textDecrypted: Array[Byte] = null
            cipher.synchronized {
              cipher.init(Cipher.DECRYPT_MODE, sk, ivspec)
              textDecrypted = cipher.doFinal(cipherText)
            }
            val finalDecrypted: String = new String(textDecrypted)
            logMap += ("decrypted" -> finalDecrypted)
            finalDecrypted
          }
        }
      }
    } catch {
      case ignore: BadPaddingException =>
        // we know the clear text can't be decrypted
        if (!suppressError) {
          logMap += ("invalid encrypted string, bad padding" -> base64encrypted)
          logMap += ("errorName" -> ignore.getMessage)
          logMap += ("stackTrace" -> ignore.getStackTrace.mkString("\r\n\t"))
          LOGGER.debug("decryptError", null, logMap)
          ""
        }
        else {
          logMap += ("invalid encrypted string, bad padding" -> base64encrypted)
          logMap += ("errorName" -> ignore.getMessage)
          logMap += ("stackTrace" -> ignore.getStackTrace.mkString("\r\n\t"))
          LOGGER.debug("decryptError", null, logMap)
          ""
        }
      case t: Throwable =>
        if (!suppressError) {
          logMap += ("base64encrypted" -> base64encrypted)
          logMap += ("errorName" -> t.getMessage)
          logMap += ("stackTrace" -> t.getStackTrace.mkString("\r\n\t"))
          LOGGER.debug("decryptError", null, logMap)
          ""
        }
        else {
          logMap += ("base64encrypted" -> base64encrypted)
          logMap += ("errorName" -> t.getMessage)
          logMap += ("stackTrace" -> t.getStackTrace.mkString("\r\n\t"))
          LOGGER.debug("decryptError", null, logMap)
          ""
        }
    }
  }

  private def generateSalt = {
    val random = new SecureRandom
    val salt = new Array[Byte](SALT_SIZE)
    random.nextBytes(salt)
    salt
  }

  @throws[Exception]
  private def generateIV = {
    var byteIV = new Array[Byte](IV_SIZE)
    byteIV = secureRandom.generateSeed(IV_SIZE)
    byteIV
  }

  @throws[Exception]
  private def generateUniqueKey(salt: Array[Byte]) = try {
    val key = Base64.encodeBase64String(this.secretKey.getEncoded)
    val spec = new PBEKeySpec(key.toCharArray, salt, this.iterationCount, KEY_SIZE)
    var tmp: SecretKey = null
    secretKeyFactory.synchronized(tmp = secretKeyFactory.generateSecret(spec))
    val encoded = tmp.getEncoded
    new SecretKeySpec(encoded, UNIQUE_ALGORITHM)
  } catch {
    case e: InvalidKeySpecException =>
      // should never happen...
      val logMap = mutable.Map[String, Any]()
      logMap += ("errorName" -> e.getMessage)
      logMap += ("stackTrace" -> e.getStackTrace.mkString("\r\n\t"))
      LOGGER.debug("generateUniqueKey", null, logMap)
      throw new Exception(e.getMessage)
  }
}



