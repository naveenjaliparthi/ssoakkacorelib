package com.vz.security.sso.encryption

import javax.crypto.spec.SecretKeySpec
import javax.crypto.{BadPaddingException, Cipher}
import org.apache.commons.codec.binary.Base64
import org.apache.commons.logging.LogFactory

class AESEncrypt(encryptionKey: String) extends Encryption {

  private val ALGORITHM = "AES"
  private val PROVIDER = "SunJCE"
  private val LOGGER = LogFactory.getLog(classOf[AESEncrypt])

  val secretKeySpec =try{
    Some(new SecretKeySpec(Base64.decodeBase64(encryptionKey), ALGORITHM))
  }
  catch {
    case ex:Exception =>
      LOGGER.fatal(s"Secret Key initialization failed:${ex.getMessage}",ex)
      None
  }

  override def encrypt(clearText: String): Option[String] = this.synchronized {
    try {
      val c = Cipher.getInstance(ALGORITHM, PROVIDER)
      c.init(Cipher.ENCRYPT_MODE, secretKeySpec.get)
      val encrypted = c.doFinal(clearText.getBytes)
      Some(new String(Base64.encodeBase64(encrypted)))
    } catch {
      case e: Throwable =>
        LOGGER.error(e.getMessage, e)
        None
    }
    // if any errors just return null
  }

  def decrypt(base64encrypted: String): Option[String] = this.synchronized {
    try {
      val encrypted = Base64.decodeBase64(base64encrypted.getBytes)
      if ((encrypted.length % 16) != 0) { // with padded cipher length has to be multiple of 16
        if (LOGGER.isDebugEnabled) LOGGER.debug("invalid encrypt string: " + base64encrypted)
        None
      }
      val c = Cipher.getInstance(ALGORITHM, PROVIDER)
      c.init(Cipher.DECRYPT_MODE, secretKeySpec.get)
      val clearText = c.doFinal(encrypted)
      Some(new String(clearText))
    } catch {
      case ignore: BadPaddingException =>
        // we know the clear text can't be decrypted
        if (LOGGER.isDebugEnabled) LOGGER.debug("invalid encrypted string: " + base64encrypted)
        None
      case e: Throwable =>
        LOGGER.error(e.getMessage, e)
        None
    }

  }

}
