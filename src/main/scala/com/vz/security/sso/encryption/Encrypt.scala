package com.vz.security.sso.encryption

trait Encrypt {
  def encrypt(clearText: String): String

  def decrypt(encryptedText: String): String
}
