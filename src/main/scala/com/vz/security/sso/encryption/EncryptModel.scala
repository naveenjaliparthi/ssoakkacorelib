package com.vz.security.sso.encryption
import com.vz.security.sso.encryption.EncryptionConfig._
sealed case class EncryptType(value: String)

object EncryptTypeObj {

  object AES256 extends EncryptType("AES256")

  val etList = Seq(AES256)

}

sealed case class EncryptInfo(encryptionKey: String, macEncryptionKey: String, eType: EncryptType, iterationCount: Int)

object EncryptInfoObj {

  import EncryptTypeObj._

  object PROFILE_TOKEN extends EncryptInfo(profTokenEKey, profTokenSKey, AES256, profTokenICount)

  val eList = Seq(PROFILE_TOKEN)

}
