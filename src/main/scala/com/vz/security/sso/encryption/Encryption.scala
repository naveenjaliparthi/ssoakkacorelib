package com.vz.security.sso.encryption

trait Encryption {
  def encrypt(clearText: String): Option[String]

  def decrypt(encryptedText: String): Option[String]
}
