package com.vz.security.sso.encryption

import com.typesafe.config.ConfigFactory

object EncryptionConfig {

  val config = ConfigFactory.load()

  /**
   * ********************
   * **** ENCRYPTION ****
   * ********************
   */
  val encConfig = config.getConfig("vcm.encryption")
  val profTokenEKey = encConfig.getString("profTokenEKey")
  val profTokenSKey = encConfig.getString("profTokenSKey")
  val profTokenICount = encConfig.getInt("profTokenICount")
  val PAIRDELIMITER = encConfig.getString("pairDelimiter")
  val KVDELIMITER = encConfig.getString("kvDelimiter")

}
