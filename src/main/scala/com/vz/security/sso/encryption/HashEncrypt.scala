package com.vz.security.sso.encryption

trait HashEncrypt {

  def encrypt(clearText: String): String

  def isMatch(clearText: String, encrypted: String): Boolean

  def isMatch(clearText: String, encrypted: String, caseSensitive: Boolean): Boolean

}
