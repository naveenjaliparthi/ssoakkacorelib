package com.vz.security.sso.grpc.client

import com.vz.security.sso.grpc.services.ReplicationRequest
import org.json4s.DefaultFormats

import scala.concurrent.Future

trait ReplicationGrpcClient {
  implicit val mapJsonFormat: DefaultFormats.type = DefaultFormats

  def replicateMessage(request: ReplicationRequest): Future[Int]
}


