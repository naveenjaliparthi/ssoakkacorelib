package com.vz.security.sso.grpc.client

import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.pattern.{AskTimeoutException, CircuitBreaker, ask, pipe}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.config.Config
import com.vz.security.sso.common.constants.SharedConstants.{halfOpenMessage, halfOpenMessageType}
import com.vz.security.sso.common.exceptions.CustomExceptions.GrpcClientException
import com.vz.security.sso.grpc.services.ReplicationRequest
import com.vz.security.sso.logging.VZQuickLogger

import scala.collection.JavaConverters._
import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Failure

case class ReplicationGrpcWithCircuitBreakerConfig(grpcConfig: ReplicationGrpcConfig, maxFailures: Int,
                                                   callTimeout: FiniteDuration, resetTimeout: FiniteDuration)

object ReplicationGrpcWithCircuitBreakerConfig {
  def apply(config: Config): ReplicationGrpcWithCircuitBreakerConfig = {
    ReplicationGrpcWithCircuitBreakerConfig(ReplicationGrpcConfig(config getConfig "grpc"),
      config getInt "max-failures",
      FiniteDuration(config.getDuration("call-timeout").toMillis, TimeUnit.MICROSECONDS),
      FiniteDuration(config.getDuration("reset-timeout").toMillis, TimeUnit.MICROSECONDS))
  }
}

private object ReplicationGrpcActor {
  def props(index: Int, config: ReplicationGrpcWithCircuitBreakerConfig)(implicit system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer): Props = {
    Props(new ReplicationGrpcActor(index, config))
  }

  case class HalfOpened()

}

private class ReplicationGrpcActor(val index: Int, config: ReplicationGrpcWithCircuitBreakerConfig)
                                  (implicit system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer) extends Actor {

  import ReplicationGrpcActor._
  import ReplicationGrpcSupervisor._
  val logger = new VZQuickLogger(this)
  val client = new ReplicationGrpcPlainClient(config.grpcConfig)
  private val breaker =
    new CircuitBreaker(
      context.system.scheduler,
      maxFailures = config.maxFailures,
      callTimeout = config.callTimeout,
      resetTimeout = config.resetTimeout)
      .onOpen(opened())
      .onHalfOpen(halfOpened())
      .onClose(closed())

  override def receive: Receive = {
    case req: ReplicationRequest =>
      try {
        breaker.withCircuitBreaker(client.replicateMessage(req)) pipeTo sender()
      } catch {
        case ex: Exception => sender() ! Failure(ex)
      }
    case _@HalfOpened =>
      //dummy request to test connection
      logger.warn("grpc_half_opened_client", null, "host", config.grpcConfig.host, "port", config.grpcConfig.port)
      breaker.withCircuitBreaker(client.replicateMessage(ReplicationRequest(rMap = halfOpenMessage, rType = halfOpenMessageType)))
    case m@_ =>
      throw GrpcClientException(s"Received unsupported case in ReplicationGrpcActor, message: $m")
  }

  def opened(): Unit = {
    //println(s" circuit breaker opened for $index")
    context.parent ! Opened(self)
  }

  def halfOpened(): Unit = {
    //println(s" circuit breaker half-opened for $index")
    self ! HalfOpened
  }

  def closed(): Unit = {
    //println(s" circuit breaker closed for $index")
    context.parent ! Closed(self)
  }

  override def postStop(): Unit = {
    //println(s" stopping actor $index")
  }
}

private object ReplicationGrpcSupervisor {

  def props(config: Config)(implicit system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer): Props = Props(new ReplicationGrpcSupervisor(config))

  case class Opened(a: ActorRef)

  case class Closed(a: ActorRef)

  case class Body(body: String)

  case class Fields(fields: Map[String, String])

}

private class ReplicationGrpcSupervisor(config: Config)
                                       (implicit system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer) extends Actor {

  import ReplicationGrpcSupervisor._

  implicit val timeout: Timeout = FiniteDuration(config.getDuration("timeout").toNanos, TimeUnit.NANOSECONDS)

  private val configs = config getConfigList "grpcs"
  private val grpcConfigs = configs.asScala.map { c => ReplicationGrpcWithCircuitBreakerConfig(c) }

  private var router = {
    val routees = grpcConfigs.zipWithIndex.map {
      case (c, i) =>
        val r = context.actorOf(ReplicationGrpcActor.props(i, c), s"actor$i")
        context watch r
        ActorRefRoutee(r)
    }.toVector
    Router(RoundRobinRoutingLogic(), routees)
  }

  override def receive: Receive = {
    case req: ReplicationRequest =>
      router.route(req, sender())

    case Opened(a: ActorRef) =>
      router = router.removeRoutee(ActorRefRoutee(a))

    case Closed(a) =>
      router = router.addRoutee(ActorRefRoutee(a))

    case m@_ =>
      throw GrpcClientException(s"Received unsupported case in ReplicationGrpcSupervisor, message: $m")
  }
}

private[grpc] class ReplicationGrpcLbClient(config: Config, sname: String)
                                           (implicit system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer) extends ReplicationGrpcClient {

  implicit val timeout: Timeout = FiniteDuration(config.getDuration("timeout").toMillis, TimeUnit.MILLISECONDS)

  private val supervisor = system.actorOf(ReplicationGrpcSupervisor.props(config), sname)

  override def replicateMessage(request: ReplicationRequest): Future[Int] = {
    try {
      supervisor.ask(request).mapTo[Int]
    } catch {
      case ex: AskTimeoutException =>
        Future.failed(GrpcClientException("Exception occured while routing to ReplicationGrpcSupervisor", ex.getMessage, ex.getCause()))
    }
  }
}
