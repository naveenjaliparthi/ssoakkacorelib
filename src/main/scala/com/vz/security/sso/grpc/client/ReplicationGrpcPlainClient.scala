package com.vz.security.sso.grpc.client

import java.io.File
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.config.Config
import com.vz.security.sso.common.exceptions.CustomExceptions.GrpcClientException
import com.vz.security.sso.grpc.interceptors.ClientPerformanceInterceptor
import com.vz.security.sso.grpc.services.{ReplicationRequest, ReplicationResponse, ReplicationServiceGrpc}
import com.vz.security.sso.logging.VZQuickLogger
import io.grpc.ClientInterceptors
import io.grpc.internal.DnsNameResolverProvider
import io.grpc.netty.{GrpcSslContexts, NegotiationType, NettyChannelBuilder}

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}

case class ReplicationGrpcConfig(host: String, port: Int, deadline: Timeout, enabletls: Boolean,
                                 trustCertCollectionFilePath: String, clientCertChainFilePath: String, clientPrivateKeyFilePath: String)

object ReplicationGrpcConfig {
  def apply(config: Config): ReplicationGrpcConfig = {
    ReplicationGrpcConfig(
      config getString "host",
      config getInt "port",
      FiniteDuration(config.getDuration("deadline").toMillis, TimeUnit.MILLISECONDS),
      if (config.hasPath("enabletls")) config getBoolean "enabletls" else false,
      if (config.hasPath("trust-cert-file")) config getString "trust-cert-file" else null,
      if (config.hasPath("cert-chain-file")) config getString "cert-chain-file" else null,
      if (config.hasPath("private-key-file")) config getString "private-key-file" else null
    )
  }
}

private[grpc] class ReplicationGrpcPlainClient(config: ReplicationGrpcConfig)(implicit system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer) extends ReplicationGrpcClient {
  val logger = new VZQuickLogger(this)
  logger.info("grpc_client", null,  "host", config.host, "port", config.port)

  private val monitoringInterceptor = new ClientPerformanceInterceptor()

  private val channelBuilder = NettyChannelBuilder.forAddress(config.host, config.port) //.intercept(monitoringInterceptor)
  private val channel = if (config.enabletls) {
    val sslBuilder = GrpcSslContexts.forClient()
    if (Option(config.trustCertCollectionFilePath).isDefined) {
      sslBuilder.trustManager(new File(config.trustCertCollectionFilePath))
    }
    if (Option(config.clientCertChainFilePath).isDefined && Option(config.clientPrivateKeyFilePath).isDefined)
      sslBuilder.keyManager(new File(config.clientCertChainFilePath), new File(config.clientPrivateKeyFilePath))

    channelBuilder.negotiationType(NegotiationType.TLS)
      .sslContext(sslBuilder.build())
      .nameResolverFactory(new DnsNameResolverProvider())
      .build
  } else {
    channelBuilder.nameResolverFactory(new DnsNameResolverProvider()).usePlaintext.build
  }
  private val interCeptedChannel = ClientInterceptors.intercept(channel, monitoringInterceptor)

  private val client = ReplicationServiceGrpc.stub(interCeptedChannel)

  override def replicateMessage(request: ReplicationRequest): Future[Int] = {
    val response: Future[ReplicationResponse] = client.withDeadlineAfter(config.deadline.duration.toMillis, TimeUnit.MILLISECONDS).replicateMessage(request)
    fetchResponse(response)
  }

  private def fetchResponse(response: Future[ReplicationResponse]): Future[Int] = {
    response.map { x =>
      x.rStatus
    }
      .recover {
        case ex: io.grpc.StatusRuntimeException =>
          throw GrpcClientException("Unable to retrieve status from ReplicationResponse", ex.getMessage, ex.getCause)
      }
  }
}
