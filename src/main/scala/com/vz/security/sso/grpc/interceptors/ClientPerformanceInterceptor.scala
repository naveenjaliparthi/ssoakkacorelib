package com.vz.security.sso.grpc.interceptors

import io.grpc._

class ClientPerformanceInterceptor extends ClientInterceptor {
  override def interceptCall[ReqT, RespT](
                                           method: MethodDescriptor[ReqT, RespT],
                                           callOptions: CallOptions,
                                           next: Channel): ClientCall[ReqT, RespT] = {
    new PerformanceClientCall(next.newCall(method, callOptions))
  }
}
