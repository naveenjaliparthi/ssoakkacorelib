package com.vz.security.sso.grpc.interceptors

import com.vz.security.sso.logging.VZLogger
import io.grpc.MethodDescriptor.MethodType
import io.grpc.{ForwardingServerCallListener, MethodDescriptor, ServerCall, ServerCallHandler, ServerInterceptor, _}

object GrpcServerInterceptor {
  def create() = new GrpcServerInterceptor()
}

class GrpcServerInterceptor private() extends ServerInterceptor {

  override def interceptCall[R, S](call: ServerCall[R, S], requestHeaders: Metadata, next: ServerCallHandler[R, S]): ServerCall.Listener[R] = {
    val method = call.getMethodDescriptor
    val grpcMethod = GrpcMethod.of(method)
    val monitoringCall = new MonitoringServerCall[R, S](call, grpcMethod)
    new MonitoringServerCallListener[R](next.startCall(monitoringCall, requestHeaders), GrpcMethod.of(method), monitoringCall)
  }

}

object MonitoringServerCall {
  private val MICROS_PER_MILLI = 1000L.toDouble
}

class MonitoringServerCall[R, S](val delegate1: ServerCall[R, S], val grpcMethod: GrpcMethod) extends ForwardingServerCall.SimpleForwardingServerCall[R, S](delegate1) {

  import MonitoringServerCall._
  val logger = new VZLogger(this,"gateway_audit_log")
  private val startTime = System.nanoTime

  override def close(status: Status, responseHeaders: Metadata): Unit = {
    /*
    logger.info("gateway_audit_log", null,
        "method", grpcMethod.methodName,
                "protocol", grpcMethod.methodType,
                "uri", grpcMethod.serviceName,
                "status", status,
                "resp_time", ((System.nanoTime - startTime) / MICROS_PER_MILLI).toLong
    )
    */
    logger.addKeyValue("resp_time", ((System.nanoTime - startTime) / MICROS_PER_MILLI).toLong)
    logger.addKeyValue("method",grpcMethod.methodName)
    logger.addKeyValue("protocol", s"GRPC_${grpcMethod.methodType.toString}")
    logger.addKeyValue("uri", grpcMethod.serviceName)
    logger.addKeyValue("status", status.getCode.name)
    //logger.addKeyValue("statustoString", status.toString)
    //logger.addKeyValue("statusgetDescription", status.getDescription)

    logger.debug
    super.close(status, responseHeaders)
  }

  override def sendMessage(respMessage: S): Unit = {
    logger.addKeyValue("response", respMessage)
    super.sendMessage(respMessage)
  }

  def logRequestObj(reqMessage: R): Unit = {
    logger.addKeyValue("request", reqMessage)
  }
}

class MonitoringServerCallListener[R](val delegate1: ServerCall.Listener[R], val grpcMethod: GrpcMethod, val monitorCall: MonitoringServerCall[R, _]) extends ForwardingServerCallListener[R] {

  override protected def delegate: ServerCall.Listener[R] = delegate1

  override def onMessage(message: R): Unit = {
    monitorCall.logRequestObj(message)
    super.onMessage(message)
  }
}


object GrpcMethod {
  def of(method: MethodDescriptor[_, _]): GrpcMethod = {
    val serviceName = MethodDescriptor.extractFullServiceName(method.getFullMethodName)
    val methodName = method.getFullMethodName.substring(serviceName.length + 1)
    new GrpcMethod(serviceName, methodName, method.getType)
  }
}

class GrpcMethod private(val serviceName: String, val methodName: String, val methodType: MethodDescriptor.MethodType) {

  def streamsRequests: Boolean = (methodType eq MethodType.CLIENT_STREAMING) || (methodType eq MethodType.BIDI_STREAMING)

  def streamsResponses: Boolean = (methodType eq MethodType.SERVER_STREAMING) || (methodType eq MethodType.BIDI_STREAMING)
}