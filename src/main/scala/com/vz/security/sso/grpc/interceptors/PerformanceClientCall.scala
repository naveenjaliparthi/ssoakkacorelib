package com.vz.security.sso.grpc.interceptors

import io.grpc.{ClientCall, ForwardingClientCall, Metadata}


class PerformanceClientCall[R, S](delegate: ClientCall[R, S]) extends ForwardingClientCall.SimpleForwardingClientCall[R, S](delegate){

  override def start(responseListener: ClientCall.Listener[S], headers: Metadata): Unit = {
    super.start(new ClientPerformanceCallListner[S](responseListener, System.nanoTime), headers)
  }

}
