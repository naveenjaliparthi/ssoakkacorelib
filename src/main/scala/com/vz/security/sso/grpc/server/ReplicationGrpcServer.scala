package com.vz.security.sso.grpc.server

import java.io.File
import java.net.InetSocketAddress
import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorSystem}
import akka.util.Timeout
import com.typesafe.config.Config
import com.vz.security.sso.common.constants.SharedConstants.{grpcTimestamp, halfOpenMessageType}
import com.vz.security.sso.grpc.interceptors.GrpcServerInterceptor
import com.vz.security.sso.grpc.services.{ReplicationRequest, ReplicationResponse, ReplicationServiceGrpc}
import com.vz.security.sso.logging.VZQuickLogger
import io.grpc.netty.{GrpcSslContexts, NettyServerBuilder}
import io.grpc.{Server, ServerInterceptors, Status}
import io.netty.handler.ssl.{ClientAuth, SslContextBuilder, SslProvider}

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

case class ReplicationGrpcServerConfig(host: String, port: Int, certChainFile: String, privateKeyFile: String, trustCertFile: String, timeout: FiniteDuration)

object ReplicationGrpcServerConfig {
  def apply(config: Config): ReplicationGrpcServerConfig = {
    new ReplicationGrpcServerConfig(
      config getString "host",
      config getInt "port",
      if (config.hasPath("cert-chain-file")) config getString "cert-chain-file" else null,
      if (config.hasPath("private-key-file")) config getString "private-key-file" else null,
      if (config.hasPath("trust-cert-file")) config getString "trust-cert-file" else null,
      FiniteDuration(config.getDuration("timeout").toMillis, TimeUnit.MILLISECONDS))
  }
}

private[grpc] class ReplicationGrpcServer(config: ReplicationGrpcServerConfig, shardRef: ActorRef)(implicit system: ActorSystem, executionContext: ExecutionContext) {
  val logger = new VZQuickLogger(this)

  implicit def timeout: Timeout = config.timeout

  class ReplicationGrpc extends ReplicationServiceGrpc.ReplicationService {

    override def replicateMessage(request: ReplicationRequest): Future[ReplicationResponse] = {
      if (request.rMap.nonEmpty) {
        request.rType match {
          case `halfOpenMessageType` =>
            //Just to test client server connection so return 200 to represents successful connection
            logger.warn("grpc_half_opened_server", null, "host", config.host, "port", config.port)
            Future {
              ReplicationResponse(rStatus = 200)
            }
          case _ =>
            val timestamp = request.rMap.get(grpcTimestamp)
            if (timestamp.isDefined) {
              logger.debug("grpcLag", null, "grpc_lag_server", System.currentTimeMillis() - timestamp.get.toLong, "host", config.host, "port", config.port)
            }
            shardRef ! request
            Future {
              ReplicationResponse(rStatus = 200)
            }
        }
      } else {
        Future.failed(
          Status
            .INVALID_ARGUMENT
            .augmentDescription("Invalid request")
            .asRuntimeException()
        )
      }
    }
  }

  logger.info("grpc_config", null, "host", config.host, "port", config.port)

  val serverBuilder: NettyServerBuilder = NettyServerBuilder.forAddress(new InetSocketAddress(config.host, config.port))
  if (Option(config.certChainFile).isDefined && Option(config.privateKeyFile).isDefined) {
    val sslContextBuilder = SslContextBuilder.forServer(new File(config.certChainFile), new File(config.privateKeyFile))
    if (Option(config.trustCertFile).isDefined) {
      sslContextBuilder.trustManager(new File(config.trustCertFile)).clientAuth(ClientAuth.REQUIRE)
    }
    logger.info("tsl_config", null, "certChainFile", config.certChainFile, "privateKeyFile", config.privateKeyFile)
    serverBuilder.sslContext(GrpcSslContexts.configure(sslContextBuilder, SslProvider.OPENSSL).build)
  }

  serverBuilder //.useTransportSecurity(new File(config.certChainFile), new File(config.privateKeyFile))
    .addService(ServerInterceptors.intercept(
    ReplicationServiceGrpc.bindService(new ReplicationGrpc, executionContext), GrpcServerInterceptor.create())
  )

  val server: Server = serverBuilder.build()
  server.start()
  logger.info("start", null, "status", "server bound to " + server.getPort)


}
