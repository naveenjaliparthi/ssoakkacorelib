package com.vz.security.sso.grpc.services

object ReplicationServiceGrpc {
  val METHOD_REPLICATE_MESSAGE: _root_.io.grpc.MethodDescriptor[com.vz.security.sso.grpc.services.ReplicationRequest, com.vz.security.sso.grpc.services.ReplicationResponse] =
    _root_.io.grpc.MethodDescriptor.newBuilder()
      .setType(_root_.io.grpc.MethodDescriptor.MethodType.UNARY)
      .setFullMethodName(_root_.io.grpc.MethodDescriptor.generateFullMethodName("com.vz.security.sso.grpc.ReplicationService", "replicateMessage"))
      .setSampledToLocalTracing(true)
      .setRequestMarshaller(_root_.scalapb.grpc.Marshaller.forMessage[com.vz.security.sso.grpc.services.ReplicationRequest])
      .setResponseMarshaller(_root_.scalapb.grpc.Marshaller.forMessage[com.vz.security.sso.grpc.services.ReplicationResponse])
      .build()

  val SERVICE: _root_.io.grpc.ServiceDescriptor =
    _root_.io.grpc.ServiceDescriptor.newBuilder("com.vz.security.sso.grpc.ReplicationService")
      .setSchemaDescriptor(new _root_.scalapb.grpc.ConcreteProtoFileDescriptorSupplier(com.vz.security.sso.grpc.services.ServicesProto.javaDescriptor))
      .addMethod(METHOD_REPLICATE_MESSAGE)
      .build()

  trait ReplicationService extends _root_.scalapb.grpc.AbstractService {
    override def serviceCompanion = ReplicationService
    def replicateMessage(request: com.vz.security.sso.grpc.services.ReplicationRequest): scala.concurrent.Future[com.vz.security.sso.grpc.services.ReplicationResponse]
  }

  object ReplicationService extends _root_.scalapb.grpc.ServiceCompanion[ReplicationService] {
    implicit def serviceCompanion: _root_.scalapb.grpc.ServiceCompanion[ReplicationService] = this
    def javaDescriptor: _root_.com.google.protobuf.Descriptors.ServiceDescriptor = com.vz.security.sso.grpc.services.ServicesProto.javaDescriptor.getServices().get(0)
  }

  trait ReplicationServiceBlockingClient {
    def serviceCompanion = ReplicationService
    def replicateMessage(request: com.vz.security.sso.grpc.services.ReplicationRequest): com.vz.security.sso.grpc.services.ReplicationResponse
  }

  class ReplicationServiceBlockingStub(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions = _root_.io.grpc.CallOptions.DEFAULT) extends _root_.io.grpc.stub.AbstractStub[ReplicationServiceBlockingStub](channel, options) with ReplicationServiceBlockingClient {
    override def replicateMessage(request: com.vz.security.sso.grpc.services.ReplicationRequest): com.vz.security.sso.grpc.services.ReplicationResponse = {
      _root_.scalapb.grpc.ClientCalls.blockingUnaryCall(channel, METHOD_REPLICATE_MESSAGE, options, request)
    }

    override def build(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions): ReplicationServiceBlockingStub = new ReplicationServiceBlockingStub(channel, options)
  }

  class ReplicationServiceStub(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions = _root_.io.grpc.CallOptions.DEFAULT) extends _root_.io.grpc.stub.AbstractStub[ReplicationServiceStub](channel, options) with ReplicationService {
    override def replicateMessage(request: com.vz.security.sso.grpc.services.ReplicationRequest): scala.concurrent.Future[com.vz.security.sso.grpc.services.ReplicationResponse] = {
      _root_.scalapb.grpc.ClientCalls.asyncUnaryCall(channel, METHOD_REPLICATE_MESSAGE, options, request)
    }

    override def build(channel: _root_.io.grpc.Channel, options: _root_.io.grpc.CallOptions): ReplicationServiceStub = new ReplicationServiceStub(channel, options)
  }

  def bindService(serviceImpl: ReplicationService, executionContext: scala.concurrent.ExecutionContext): _root_.io.grpc.ServerServiceDefinition =
    _root_.io.grpc.ServerServiceDefinition.builder(SERVICE)
    .addMethod(
      METHOD_REPLICATE_MESSAGE,
      _root_.io.grpc.stub.ServerCalls.asyncUnaryCall(new _root_.io.grpc.stub.ServerCalls.UnaryMethod[com.vz.security.sso.grpc.services.ReplicationRequest, com.vz.security.sso.grpc.services.ReplicationResponse] {
        override def invoke(request: com.vz.security.sso.grpc.services.ReplicationRequest, observer: _root_.io.grpc.stub.StreamObserver[com.vz.security.sso.grpc.services.ReplicationResponse]): Unit =
          serviceImpl.replicateMessage(request).onComplete(scalapb.grpc.Grpc.completeObserver(observer))(
            executionContext)
      }))
    .build()

  def blockingStub(channel: _root_.io.grpc.Channel): ReplicationServiceBlockingStub = new ReplicationServiceBlockingStub(channel)

  def stub(channel: _root_.io.grpc.Channel): ReplicationServiceStub = new ReplicationServiceStub(channel)

  def javaDescriptor: _root_.com.google.protobuf.Descriptors.ServiceDescriptor = com.vz.security.sso.grpc.services.ServicesProto.javaDescriptor.getServices().get(0)

}
