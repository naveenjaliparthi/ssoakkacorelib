package com.vz.security.sso.grpc.services

object ServicesProto extends _root_.scalapb.GeneratedFileObject {
  lazy val dependencies: Seq[_root_.scalapb.GeneratedFileObject] = Seq(
  )
  lazy val messagesCompanions: Seq[_root_.scalapb.GeneratedMessageCompanion[_]] = Seq(
    com.vz.security.sso.grpc.services.ReplicationRequest,
    com.vz.security.sso.grpc.services.ReplicationResponse
  )
  private lazy val ProtoBytes: Array[Byte] =
      scalapb.Encoding.fromBase64(scala.collection.Seq(
  """Cidjb20vdnovc2VjdXJpdHkvc3NvL2dycGMvc2VydmljZXMucHJvdG8SGGNvbS52ei5zZWN1cml0eS5zc28uZ3JwYyKvAQoSU
  mVwbGljYXRpb25SZXF1ZXN0EkoKBHJNYXAYASADKAsyNi5jb20udnouc2VjdXJpdHkuc3NvLmdycGMuUmVwbGljYXRpb25SZXF1Z
  XN0LlJNYXBFbnRyeVIEck1hcBIUCgVyVHlwZRgCIAEoCVIFclR5cGUaNwoJUk1hcEVudHJ5EhAKA2tleRgBIAEoCVIDa2V5EhQKB
  XZhbHVlGAIgASgJUgV2YWx1ZToCOAEiLwoTUmVwbGljYXRpb25SZXNwb25zZRIYCgdyU3RhdHVzGAEgASgFUgdyU3RhdHVzMocBC
  hJSZXBsaWNhdGlvblNlcnZpY2UScQoQcmVwbGljYXRlTWVzc2FnZRIsLmNvbS52ei5zZWN1cml0eS5zc28uZ3JwYy5SZXBsaWNhd
  GlvblJlcXVlc3QaLS5jb20udnouc2VjdXJpdHkuc3NvLmdycGMuUmVwbGljYXRpb25SZXNwb25zZSIAYgZwcm90bzM="""
      ).mkString)
  lazy val scalaDescriptor: _root_.scalapb.descriptors.FileDescriptor = {
    val scalaProto = com.google.protobuf.descriptor.FileDescriptorProto.parseFrom(ProtoBytes)
    _root_.scalapb.descriptors.FileDescriptor.buildFrom(scalaProto, dependencies.map(_.scalaDescriptor))
  }
  lazy val javaDescriptor: com.google.protobuf.Descriptors.FileDescriptor = {
    val javaProto = com.google.protobuf.DescriptorProtos.FileDescriptorProto.parseFrom(ProtoBytes)
    com.google.protobuf.Descriptors.FileDescriptor.buildFrom(javaProto, Array(
    ))
  }
  @deprecated("Use javaDescriptor instead. In a future version this will refer to scalaDescriptor.", "ScalaPB 0.5.47")
  def descriptor: com.google.protobuf.Descriptors.FileDescriptor = javaDescriptor
}
