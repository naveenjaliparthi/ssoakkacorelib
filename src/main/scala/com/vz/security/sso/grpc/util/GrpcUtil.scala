package com.vz.security.sso.grpc.util

import akka.actor.{ActorRef, ActorSystem}
import akka.stream.ActorMaterializer
import com.typesafe.config.Config
import com.vz.security.sso.common.constants.SharedConstants
import com.vz.security.sso.grpc.client.ReplicationGrpcLbClient
import com.vz.security.sso.grpc.server.{ReplicationGrpcServer, ReplicationGrpcServerConfig}
import com.vz.security.sso.grpc.services.ReplicationRequest

import scala.concurrent.ExecutionContext

/**
  * This util is to start GRPC server and initialize client. To use this util, invoke grpcReplicationInit
  * To achieve singleton all mutable variables are loaded later using grpcReplicationInit function
  * Why singleton: GrpcServer and GrpcLbClient should be initialized only once and will be used later for messages replication in shard region
  */
object GrpcUtil {
  var actorSystem: Option[ActorSystem] = None
  var executionContext: Option[ExecutionContext] = None
  var actorMaterializer: Option[ActorMaterializer] = None
  var shardRef: Option[ActorRef] = None
  var grpcClientConfigList: List[Config] = List()
  var grpcServerConfig: Option[Config] = None
  val sname = "_supervisor"

  //Starts GRPC server for shard message replication
  lazy val startGrpcServer = new ReplicationGrpcServer(ReplicationGrpcServerConfig(grpcServerConfig.get), shardRef.get)(actorSystem.get, executionContext.get)

  //Starts GRPC client for shard message replication
  lazy val grpcClientList = grpcClientConfigList.map(clientConfig => new ReplicationGrpcLbClient(clientConfig, clientConfig.getString("dcname") + sname)(actorSystem.get, executionContext.get, actorMaterializer.get))

  //Sends message to GRPC server through GRPC client
  def replicateMessage(repRequest: ReplicationRequest): Unit = {
    val addedTimestamp = repRequest.copy(rMap = repRequest.rMap + (SharedConstants.grpcTimestamp -> System.currentTimeMillis().toString))
    grpcClientList.foreach(_.replicateMessage(addedTimestamp))
  }

  //Feeds all mutable variables and starts GRPC server
  def grpcReplicationInit(_actorSystem: ActorSystem, _executionContext: ExecutionContext, _actorMaterializer: ActorMaterializer,
                          _shardRef: ActorRef, _grpcClientConfigList: List[Config], _grpcServerConfig: Config) = {
    actorSystem = Some(_actorSystem)
    executionContext = Some(_executionContext)
    actorMaterializer = Some(_actorMaterializer)
    shardRef = Some(_shardRef)
    grpcServerConfig = Some(_grpcServerConfig)
    grpcClientConfigList = _grpcClientConfigList
    startGrpcServer
  }
}
