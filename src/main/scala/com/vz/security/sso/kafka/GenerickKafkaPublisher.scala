package com.vz.security.sso.kafka

import java.util.Properties

import akka.cluster.sharding.ShardRegion.Msg
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.logging.VZLogger
import com.vz.security.sso.models.KakfaPublisherPayload
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}

import scala.collection.JavaConverters._
import scala.concurrent.Future


object GenerickKafkaPublisher {
  val conf: Config = ConfigFactory.load()
  val kakfaConfigList: List[Config] = conf.getConfigList("sso.kafka").asScala.toList

  def getKafkaInstanceNames: List[String] =  kakfaConfigList.map { x => {
       x.getString("name")
  }}

  def getKafkaProperties(kafkaName: String, hasKey: Boolean): Option[Properties] = {
    val kc: Option[Config] = kakfaConfigList collectFirst {
      case c if c.getString("name").equalsIgnoreCase(kafkaName) => c
    }
    if(kc.isEmpty) None

    val kafkaConfig = kc.get
    val props = new Properties()
    props.put("name", kafkaConfig.getString("name"))
    props.put("bootstrap.servers", kafkaConfig.getString("url"))
    props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer ")
    props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
    if (kafkaConfig.getString("security.kafkatype").equalsIgnoreCase("JAAS")) {
      //Todo: check here if its kafka config
      addJaasProps(props, kafkaConfig)
    }
    else if (kafkaConfig.getString("security.kafkatype").equalsIgnoreCase("SSL")) {
      addSSLProperties(props, kafkaConfig)
    } else { // Plain
      Some(props)
    }
  }

  //Adding JAAS properties
  def addJaasProps(props: Properties, conf: Config): Option[Properties] = {
    props.put("security.protocol", "SASL_PLAINTEXT")
    props.put("sasl.mechanism", "PLAIN")
    props.put("sasl.jaas.config", conf.getString("security.jaas.jaasConfig"))
    //ToDo: Check for optional value
    Some(props)
  }

  //Adding SSL properties
  def addSSLProperties(props: Properties, conf: Config): Option[Properties] = {
    props.put("security.protocol" , "SSL")
    props.put("ssl.truststore.location" , conf.getString("security.ssl.truststore.location"))
    props.put("ssl.truststore.password" , conf.getString("security.ssl.truststore.password"))
    props.put("ssl.keystore.location" , conf.getString("security.ssl.keystore.location"))
    props.put("ssl.keystore.password" , conf.getString("security.ssl.keystore.password"))
    props.put("ssl.key.password" , conf.getString("security.ssl.key.password"))
    props.put("ssl.truststore.type" , "JKS")
    props.put("ssl.keystore.type" , "JKS")
    props.put("ssl.enabled.protocols" , "TLSv1.2")
    //ToDo: Check for optional value
    Some(props)
  }

  def publishPayload(kafkaRequest: Option[KakfaPublisherPayload]): Future[Boolean] = {
    val req = kafkaRequest.getOrElse(throw new NullPointerException("kafka request missing"))
    val futureOps = req.kafkaList.map { kafkaName =>
      val props: Option[Properties] = getKafkaProperties(kafkaName, req.topicWithKey)
      publishToKafka(req, props)
    }
    import scala.concurrent.ExecutionContext.Implicits.{ global => ImplicitsGlobal }
    Future.foldLeft(futureOps)(false){ case(finalRes, res) =>
      finalRes || res
    }
  }

  def publishToKafka(kafkaRequest: KakfaPublisherPayload, props: Option[Properties]): Future[Boolean] = {
    val logger = new VZLogger(this, "publishToKafka")
    val producer: KafkaProducer[String, String] = new KafkaProducer[String, String](props.get)
    val futureOpList = kafkaRequest.payload.map { msg =>
      val topicList = kafkaRequest.topicName
      publishMsgToTopicList(msg, topicList, kafkaRequest.topicWithKey, producer)
    }
    import scala.concurrent.ExecutionContext.Implicits.{global => ImplicitsGlobal}
    Future.foldLeft(futureOpList)(false) { case (finalRes, res) =>
      finalRes || res
    }
  }

  def publishMsgToTopicList(msg: String, topics: List[String], hasKey: Boolean, producer: KafkaProducer[String, String]): Future[Boolean] = {
    val logger = new VZLogger(this, "publish msg to topics")
    val futureOpList = topics.map { topic =>
      publishMsgToTopic(msg, topic, hasKey, producer)
    }
    import scala.concurrent.ExecutionContext.Implicits.{global => ImplicitsGlobal}
    Future.foldLeft(futureOpList)(false) { case (finalRes, res) =>
      finalRes || res
    }
  }

  def publishMsgToTopic(msg: String, topic: String, hasKey: Boolean, producer: KafkaProducer[String, String]): Future[Boolean] = {
    try {
      val pr: ProducerRecord[String, String] = if(hasKey){
        val kv = msg.split(":", 2)
        new ProducerRecord(topic, kv.head , kv.last)
      } else {
        new ProducerRecord(topic, null, msg)
      }
      producer.send(pr)
      Future.successful(true)
    } catch {
      case e: Exception =>
        Future.failed(e)
    }
  }
}
