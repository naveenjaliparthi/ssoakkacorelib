package com.vz.security.sso.logging

import akka.actor.ActorSystem
import com.typesafe.scalalogging.StrictLogging
import org.slf4j.{Marker, MarkerFactory}

import scala.collection.immutable.Map
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.FiniteDuration

/**
  * Verizon specific log map generation functionality used for all logging types
  *   -QuickLogger  - Log something right now
  *   -SessionLogger - Log a batch of information over a period and flush it on demand
  *   -DelayedAggregateLogger - For heavy duty logging with overwhelming amounts of logs with the same message key
  */
trait CoreLogger extends StrictLogging {

  type LogMap = scala.collection.Map[String, String]

  protected def loggerMap = Map[String, String]( elems =
    "type_alias" -> typeAlias,
    "className" -> getClass.getName
  )

  def typeAlias: String = ""

  //Add more information to custom log
  protected def addKeyValue(key: String, value: String, logMap: LogMap = loggerMap): LogMap =
    if (key.nonEmpty) logMap + (key.toLowerCase -> value)
    else logMap

  protected def generateLog(logMap: LogMap = loggerMap): String = {
    logMap + ("endTime" -> System.currentTimeMillis())
    import io.circe.syntax._
    val jsonStr = logMap.asJson.noSpaces
    jsonStr.substring(1, jsonStr.length - 1)
  }

  protected def addMessage(message: String, logMap: LogMap = loggerMap): LogMap =
    logMap + ("message" -> message)

  protected def addAggregateLog(log: DelayedAggregateLog, logMap: LogMap = loggerMap): LogMap =
    logMap + ("delayedAggregateLogs" -> log.toString) //TODO Serialize to JSON, remove .toString

  protected def setError(
    exception: Throwable,
    maybeMessage: Option[String] = None,
    messageMap: LogMap = Map.empty,
    logMap: LogMap = loggerMap
  ): LogMap = {
    maybeMessage.map(message => logMap + (("message", message)))
      .getOrElse(logMap) + (("errorName", exception.getMessage)) + (("stackTrace", exception.getStackTrace.mkString("\r\n\t"))) ++ messageMap
  }
}

/**
  * Logs a single message immediately
  */
trait QuickLogger extends CoreLogger {

  def error(exception: Throwable): Unit =
    logger.error(CoreLogging.logThisMarker, generateLog(setError(exception)))

  def error(exception: Throwable, message : String): Unit =
    logger.error(CoreLogging.logThisMarker, generateLog(setError(exception, Some(message))))

  def error(exception:Throwable, messageMap : Map[String, String]): Unit =
    logger.error(CoreLogging.logThisMarker, generateLog(setError(exception, None, messageMap)))

  def warn(key : String, value: String): Unit = logger.warn(CoreLogging.logThisMarker, generateLog(addKeyValue(key, value)))

  def info(key : String, value: String): Unit = logger.info(CoreLogging.logThisMarker, generateLog(addKeyValue(key, value)))

  def debug(key : String, value: String): Unit = logger.debug(CoreLogging.logThisMarker, generateLog(addKeyValue(key, value)))

  def trace(key : String, value: String): Unit = logger.trace(CoreLogging.logThisMarker, generateLog(addKeyValue(key, value)))

}

/**
  * Builds up logs for a specific session and flushes them on demand
  */
class SessionLogger(sessionId: String) extends CoreLogger  {

  protected val mutableLogMap = scala.collection.mutable.Map[String, String]( elems =
    "type_alias" -> typeAlias,
    "className" -> getClass.getName
  )

  def info(key : String, value: String): Unit = mutableLogMap += (key.toLowerCase -> value)

  // This methods takes List of Key, Value tuples and add them logMap
  def addKeyValues(listOfKeyValues: List[(String, String)]): Unit = listOfKeyValues.foreach {
    keyValue => info(keyValue._1, keyValue._2)
  }

  //TODO: An Error should probably just flush the log immediately?
  def addError(throwable: Throwable): Unit = mutableLogMap ++= setError(throwable)

  def startSession: Unit = {
    mutableLogMap += ("session_id" -> sessionId)
    mutableLogMap += ("startTime" -> System.currentTimeMillis.toString)
  }

  def flushLog(level: LogLevel): Unit =
    level match {
      case Error =>
        logger.error(CoreLogging.logThisMarker, generateLog(mutableLogMap))
      case Warn =>
        logger.warn(CoreLogging.logThisMarker, generateLog(mutableLogMap))
      case Info =>
        logger.info(CoreLogging.logThisMarker, generateLog(mutableLogMap))
      case Debug =>
        logger.debug(CoreLogging.logThisMarker, generateLog(mutableLogMap))
      case Trace =>
        logger.trace(CoreLogging.logThisMarker, generateLog(mutableLogMap))
    }
}

/**
  * Builds up logs of the same type over a FiniteDuration and logs a single log with the
  * message and number of aggregates of the same type, rather than flooding logs.
  */
trait DelayedAggregateLogger extends CoreLogger {

  implicit def executionContext: ExecutionContext
  def loggingDelay: FiniteDuration

  private val logMap = scala.collection.mutable.HashMap[String, DelayedAggregateLog]().empty

  def trace(message : String, exception: Option[Throwable] = None): Unit =
    incrementAndPut(message, Trace, exception)

  def debug(message : String, exception: Option[Throwable] = None): Unit =
    incrementAndPut(message, Debug, exception)

  def info(message : String, exception: Option[Throwable] = None): Unit =
    incrementAndPut(message, Info, exception)

  def warn(message : String, exception: Option[Throwable] = None): Unit =
    incrementAndPut(message, Warn, exception)

  def error(message : String, exception: Option[Throwable] = None): Unit =
    incrementAndPut(message, Error, exception)

  private def incrementAndPut(message: String, level: LogLevel, exception: Option[Throwable] = None) = {
    val log = logMap.getOrElse(message, DelayedAggregateLog(level, exception, loggingDelay))
    logMap.put(message, log.copy(numberOf = log.numberOf + 1))
  }

  //The delay must be the same as the interval in this case so the overDuration property is always correct
  CoreLogging.loggingActorSystem.scheduler.schedule(loggingDelay, loggingDelay) {

    val localLogMap = logMap
    logMap.clear()

    localLogMap.foreach { log =>
      val logData = log._2
      logData.level match {
        case Error =>
          logger.error(CoreLogging.logThisMarker, generateLog(addAggregateLog(logData)))

        case Warn =>
          logger.warn(CoreLogging.logThisMarker, generateLog(addAggregateLog(logData)))

        case Info =>
          logger.info(CoreLogging.logThisMarker, generateLog(addAggregateLog(logData)))

        case Debug =>
          logger.debug(CoreLogging.logThisMarker, generateLog(addAggregateLog(logData)))

        case Trace =>
          logger.trace(CoreLogging.logThisMarker, generateLog(addAggregateLog(logData)))
      }
    }
  }
}

object CoreLogging {

  val loggerIdentifierText = "LogThis"
  val logThisMarker: Marker = MarkerFactory.getMarker(loggerIdentifierText)

  //Only initialized for DelayedAggregateLogger
  lazy val loggingActorSystem = ActorSystem("DelayedAggregateLogger")

}

sealed trait LogLevel

case object Error extends LogLevel
case object Warn extends LogLevel
case object Info extends LogLevel
case object Debug extends LogLevel
case object Trace extends LogLevel

case class DelayedAggregateLog(
  level: LogLevel,
  exception: Option[Throwable],
  overDuration: FiniteDuration,
  numberOf: Long = 0
)