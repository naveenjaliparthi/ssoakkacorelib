package com.vz.security.sso.logging

import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization.write
import org.slf4j.{LoggerFactory, MarkerFactory}

import scala.collection.mutable

class VZMapLogger(clazz: Any) {
  private val className = clazz.getClass.getSimpleName
  private val logger = LoggerFactory.getLogger(clazz.getClass)
  private val loggerIdentifierText = "LogThis"
  private val logThisMarker = MarkerFactory.getMarker(loggerIdentifierText)

  implicit val formats = DefaultFormats

  private def convertToMap(typeAlias: String, sessionId: String, userLogs: mutable.Map[String, Any]): Map[String, Any] = {
    val loggerMap = Map[String, Any](
      "type_alias" -> {
        if (typeAlias == null || typeAlias.isEmpty) "logmessage" else typeAlias
      },
      "className" -> className,
      "startTime" -> System.currentTimeMillis(),
      "session_id" -> {
        if (sessionId == null || sessionId.isEmpty) "sessionIdNotProvided" else sessionId
      }
    )
    loggerMap ++ userLogs
  }

  private def convertToMap(typeAlias: String, sessionId: String, key: String, value: String): Map[String, Any] = {
    Map[String, Any](
      "type_alias" -> {
        if (typeAlias == null || typeAlias.isEmpty) "logmessage" else typeAlias
      },
      "className" -> className,
      "startTime" -> System.currentTimeMillis(),
      "session_id" -> {
        if (sessionId == null || sessionId.isEmpty) "sessionIdNotProvided" else sessionId
      },
      key -> value
    )
  }


  def trace(typeAlias: String, sessionId: String, key: String, value: String): Unit =
    logger.trace(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, key, value)))

  def trace(typeAlias: String, sessionId: String, userLogs: mutable.Map[String, Any]): Unit =
    logger.trace(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, userLogs)))

  def debug(typeAlias: String, sessionId: String, key: String, value: String): Unit =
    logger.debug(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, key, value)))

  def debug(typeAlias: String, sessionId: String, userLogs: mutable.Map[String, Any]): Unit =
    logger.debug(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, userLogs)))

  def info(typeAlias: String, sessionId: String, key: String, value: String): Unit =
    logger.info(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, key, value)))

  def info(typeAlias: String, sessionId: String, userLogs: mutable.Map[String, Any]): Unit =
    logger.info(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, userLogs)))

  def warn(typeAlias: String, sessionId: String, key: String, value: String): Unit =
    logger.warn(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, key, value)))

  def warn(typeAlias: String, sessionId: String, userLogs: mutable.Map[String, Any]): Unit =
    logger.warn(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, userLogs)))

  def error(typeAlias: String, sessionId: String, exception: Throwable, key: String, value: String): Unit = {
    var loggerMap = convertToMap(typeAlias, sessionId, key, value)
    loggerMap += (("errorName", exception.getMessage))
    loggerMap += (("stackTrace", exception.getStackTrace.mkString("\r\n\t")))
    logger.error(logThisMarker, generateLog(loggerMap))
  }

  def error(typeAlias: String, sessionId: String, exception: Throwable, userLogs: mutable.Map[String, Any]): Unit = {
    var loggerMap = convertToMap(typeAlias, sessionId, userLogs)
    loggerMap += (("errorName", exception.getMessage))
    loggerMap += (("stackTrace", exception.getStackTrace.mkString("\r\n\t")))
    logger.error(logThisMarker, generateLog(loggerMap))
  }


  //Sometimes we might need to throw error in few scenarios without exceptions. Ex: Validation failure
  def error(typeAlias: String, sessionId: String, key: String, value: String): Unit = {
    val loggerMap = convertToMap(typeAlias, sessionId, key, value)
    logger.error(logThisMarker, generateLog(loggerMap))
  }

  //Sometimes we might need to throw error in few scenarios without exceptions. Ex: Validation failure
  def error(typeAlias: String, sessionId: String, userLogs: mutable.Map[String, Any]): Unit = {
    val loggerMap = convertToMap(typeAlias, sessionId, userLogs)
    logger.error(logThisMarker, generateLog(loggerMap))
  }

  private def generateLog(loggerMap: Map[String, Any]): String = {
    if (loggerMap.nonEmpty) {
      val jsonStr = write(loggerMap)
      return jsonStr.substring(1, jsonStr.length - 1)
    }
    ""
  }

}
