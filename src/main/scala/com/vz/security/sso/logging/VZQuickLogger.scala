package com.vz.security.sso.logging

import java.util.regex.Pattern

import org.json4s._
import org.json4s.jackson.Serialization.write
import org.slf4j.{LoggerFactory, MarkerFactory}


class VZQuickLogger(clazz: Any) {

  private val className = clazz.getClass.getSimpleName
  private val logger = LoggerFactory.getLogger(clazz.getClass)
  private val loggerIdentifierText = "LogThis"
  private val logThisMarker = MarkerFactory.getMarker(loggerIdentifierText)
  val pattern = Pattern.compile("[^a-zA-Z0-9_]+")

  implicit val formats = DefaultFormats

  private def convertToMap(typeAlias: String, sessionId: String, kvPairs: Any*): Map[String, Any] = {
    var loggerMap = Map[String, Any](
      "type_alias" -> typeAlias,
      "className" -> className,
      "startTime" -> System.currentTimeMillis(),
      "session_id" -> sessionId
    )
    //ignores last value in odd numbered args
    val maxLimit = if(kvPairs.length % 2 == 0) kvPairs.length else kvPairs.length-1
    var i = 0
    while(i < maxLimit){
      val key = pattern.matcher(kvPairs(i).asInstanceOf[String]).replaceAll("").toLowerCase
      loggerMap += (key -> kvPairs(i+1))
      i += 2
    }
    loggerMap += ("endTime" -> System.currentTimeMillis())
    return loggerMap
  }

  def info(typeAlias: String, sessionId: String, kvPairs: Any*): Unit =
    logger.info(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, kvPairs: _*)))

  def debug(typeAlias: String, sessionId: String, kvPairs: Any*): Unit =
    logger.debug(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, kvPairs: _*)))

  def warn(typeAlias: String, sessionId: String, kvPairs: Any*): Unit =
    logger.warn(logThisMarker, generateLog(convertToMap(typeAlias, sessionId, kvPairs: _*)))

  def error(typeAlias: String, sessionId: String, exception: Throwable, kvPairs: Any*): Unit = {
    var loggerMap = convertToMap(typeAlias, sessionId, kvPairs: _*)
    loggerMap += (("errorName", exception.getMessage))
    loggerMap += (("stackTrace", exception.getStackTrace.mkString("\r\n\t")))
    logger.error(logThisMarker, generateLog(loggerMap))
  }

  private def generateLog(loggerMap: Map[String, Any]): String = {
    if (loggerMap.nonEmpty) {
      val jsonStr = write(loggerMap)
      return jsonStr.substring(1, jsonStr.length - 1)
    }
    ""
  }

}
