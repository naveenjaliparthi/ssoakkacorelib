package com.vz.security.sso.models

trait Command

case class AggMapper(payload: Map[String, Any]) extends Command

case class SlidingTick(time: Long) extends Command

case class ReducedResult(ip: Long, s: Int, f: Int)

