package com.vz.security.sso.models

import spray.json.DefaultJsonProtocol._
import spray.json.{JsFalse, JsNumber, JsString, JsTrue, JsValue, JsonFormat, _}
import scala.collection.immutable.Map
import Validator._
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

case class CacheConfig(id: String, name: String, cache_type: String, kafka_topic: Set[String],
                       eviction_policy: Option[CacheEvictionPolicy], lookup_attributes: Set[String],
                       output_attributes: List[String], category: String, persist: Boolean = true,
                       snapshot: Boolean = true, key: List[String], active: Boolean = true,
                       case_sensitive: Boolean = false) {
  require(validateString(id), "id is not valid in CacheConfig")
  require(validateString(name), "name is not valid in CacheConfig")
  require(validateString(cache_type), "cache_type is not valid in CacheConfig")
  require(kafka_topic.nonEmpty, "kafka_topic can not be empty")
  require(validateString(category), "category is not valid in CacheConfig")
  require(key.nonEmpty, "key can not be empty")
}

case class CacheEvictionPolicy(eviction_type: String,
                               @JsonDeserialize(contentAs = classOf[java.lang.Long])time: Option[Long] = None,
                               @JsonDeserialize(contentAs = classOf[java.lang.Long])size: Option[Long] = None)

case class CacheManagerOp(op: String, config: CacheConfig) {
  require(validateString(op), "op is not valid in CacheManagerOp")
}

case class CacheOp(op: String, cache_id: String, payload: Map[String, Any]) {
  require(validateString(op), "op is not valid in CacheOp")
}

//date format: "yyyy-MM-dd HH:mm:ss"
case class EventSourceDAO(cache_id: String, cache_payload: Map[String, Any], date: String) {
  require(validateString(cache_id), "cache_id is not valid in EventSourceDAO")
}

//date format: "yyyy-MM-dd HH:mm:ss". It is given optional to handle backward compatibility
case class SnaphotDAO(cache_id: String, cache_payload: String, date: Option[String]) {
  require(validateString(cache_id), "cache_id is not valid in SnaphotDAO")
}

object Validator {
  def validateString(v: String): Boolean = {
    Option(v).map(!_.isEmpty) == Option(true)
  }

}

/**
  * Alpakka elastic uses spray json to marshall/unmarshall
  * Defining implicit formatter's for only elastic models
  */
object SprayFormatter {

  implicit object AnyJsonFormat extends JsonFormat[Any] {
    def write(x: Any) = x match {
      case b: Boolean if b == true => JsTrue
      case b: Boolean if b == false => JsFalse
      case n: Int => JsNumber(n)
      case l: Long => JsNumber(l)
      case f: Float => JsNumber(f.toString)
      case d: Double => JsNumber(d.toString)
      case bd: BigDecimal => JsNumber(bd.toString)
      case s: String => JsString(s)
      case x => serializationError("error Cannot determine object type " + x.getClass.getName)
    }

    def read(value: JsValue) = value match {
      case JsTrue => true
      case JsFalse => false
      case JsNumber(n) =>
        if (n.isValidInt) n.intValue()
        else if (n.isValidLong) n.longValue()
        else if (n.isDecimalFloat) n.floatValue()
        else if (n.isDecimalDouble) n.doubleValue()
        else n.intValue()
      case JsString(s) => s.toString
      case x => deserializationError("error: Cannot deserialize " + x)
    }
  }

  implicit val cacheOpFormat = jsonFormat3(CacheOp)
  implicit val cacheEvictionFormat = jsonFormat3(CacheEvictionPolicy)
  implicit val cacheConfigFormat = jsonFormat13(CacheConfig)
  implicit val cacheMngrOpFormat = jsonFormat2(CacheManagerOp)
  implicit val eventSourceDAOFormat = jsonFormat3(EventSourceDAO)
  implicit val snaphotDAOFormat = jsonFormat3(SnaphotDAO)
}


