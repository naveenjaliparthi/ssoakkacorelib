package com.vz.security.sso.models

case class ChainInfo(actorType: String, threshold: String, shardValue: String, remainingChain: List[String])
