package com.vz.security.sso.models

case class KakfaPublisherPayload(topicName: List[String],
                                 kafkaList: List[String],
                                 topicWithKey: Boolean,
                                 payload: List[String]) {
  require(topicName.nonEmpty, "topicName can not be empty")
  require(payload.nonEmpty, "topicMessages can not be empty")
}

