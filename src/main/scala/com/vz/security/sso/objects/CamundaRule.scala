package com.vz.security.sso.objects

import java.util.UUID
import io.circe._
import io.circe.parser._
case class CamundaRule(timestamp: Long = 0, uuid: UUID = null, dmnUUID:UUID = null, dmnXML: String = null, name: String = null, module: String = null, desc: String = null, byUserId: String = null, operation: String = null, inputConfig: Option[String] = None)