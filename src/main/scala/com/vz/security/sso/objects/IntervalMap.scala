package com.vz.security.sso.objects

import scala.collection.immutable.TreeMap

case class Segment[T](start: Long, end: Long, value: T) {
  def contains(x: Long) = (start <= x) && ((x < end) || (x <= end))
  override def toString = "[%d,%d:%s]".format(start, end, value)
}
class IntervalMap[T] {
  private var segments = new TreeMap[Long, Segment[T]]
  private def add(s: Segment[T]): Unit = segments += (s.start -> s)
  private def destroy(s: Segment[T]): Unit = segments -= s.start
  def ceiling(x: Long): Option[Segment[T]] = {
    val from = segments.from(x)
    if (from.isEmpty) None
    else Some(segments(from.firstKey))
  }
  def floor(x: Long): Option[Segment[T]] = {
    val to = segments.to(x)
    if (to.isEmpty) None
    else Some(segments(to.lastKey))
  }
  def find(x: Long): Option[Segment[T]] = {
    floor(x).filter(_ contains x).orElse(ceiling(x))
  }

  def update(x: Long, y: Long, value: T): Unit = {
    require(x <= y)
    find(x) match {
      case None => add(Segment[T](x, y, value))
      case Some(s) => {
        if (x < s.start) {
          if (y < s.start) {
            add(Segment[T](x, y, value))
          } else {
            destroy(s)
            add(Segment[T](x, y, value))
          }
        } else if (x <= s.end) {
          destroy(s)
          add(Segment[T](x, y, value))
        }
        else{
          add(Segment[T](x, y, value))
        }
      }
    }
  }

  def get(x: Long): Option[T] = {
    for (seg <- floor(x); if (seg contains x)) yield seg.value
  }

  def apply(x: Long) = get(x).getOrElse{
    throw new NoSuchElementException(
      "No value set at index " + x
    )
  }

  def delete(x: Long, y:Long) = find(x) match{
    case Some(s)=> {
      if(s.start == x && s.end == y)
        {
          destroy(s)
        }
      else{
        throw new NoSuchElementException(
          "No value set found"
        )
      }
    }
    case None =>{
      throw new NoSuchElementException(
        "No value set found"
      )
    }

  }

  override def toString = segments.mkString("{", ",", "}")

  def getNumberOfElements = segments.size
}
