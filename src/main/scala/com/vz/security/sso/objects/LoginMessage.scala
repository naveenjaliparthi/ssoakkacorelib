package com.vz.security.sso.objects

import akka.actor.ActorRef

case class AuthenticationMessage(sessionID: Option[String] = None, url:Option[String] = None, method:Option[String] = None, deviceID:Option[String] = None, deviceFingerprint:Option[String] = None, IPAddress: String, UID: Option[String] = None, host: Option[String] = None, dataCenter: Option[String] = None, timeStamp: Option[Long] = None, userID:Option[String] = None, source: Option[String] = None, lob: Option[String] = None)

//case class ScoreResponse(score: Int, reason: String, error: Option[String] = None)

case class BlockedIP(ipAddress: String, timeStamp: Long, source: Option[String])

case class RiskyUser(userID: String, timeStamp: Long, source: Option[String])

// This case class will be used in the StreamsApp & GatewayApp for Serializing and Desirializing the Events
case class StreamEvent(IPAddress: String, userID: String, activity_type: String, timeStamp: String, isValidTransaction: Boolean)

case class AggregatedCountMessage(ipAddress: String, uniqueUserCount: Int, successCount: Int, failureCount: Int)

case class AggregatedUserCountMessage(userId: String, uniqueIPCount: Int)

case class UserCache(userID: String, action: String)

// Case class to be used in the Compromised IP Cache
case class IpCache(ipAddress: String, action: String)

case class RiskyCookie(cookieID: String, timeStamp: Long, source: Option[String])

case class SessionByUser(sessionId: String, userId: String, timeStamp: Long)