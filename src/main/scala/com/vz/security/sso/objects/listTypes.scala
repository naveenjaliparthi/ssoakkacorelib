package com.vz.security.sso.objects

object listTypes extends Enumeration {
  type listTypes = Value
  val Whitelist = Value("Whitelist")
  val Blacklist = Value("Blacklist")
  val Unknown = Value("Unknown")

  def withNameWithDefault(name: String): Value =
    values.find(_.toString.toLowerCase == name.toLowerCase()).getOrElse(Unknown)
}