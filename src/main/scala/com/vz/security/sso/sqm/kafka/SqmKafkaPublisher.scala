package com.vz.security.sso.sqm.kafka
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.kafka.GenerickKafkaPublisher
import com.vz.security.sso.models.KakfaPublisherPayload

import scala.collection.JavaConverters._
import scala.concurrent.Future


object SqmKafkaPublisher {

  val conf: Config = ConfigFactory.load()
  val sqmKafkaConf: Config = conf.getConfig("sso.sqm.kafka")
  val topicsList:List[String] = sqmKafkaConf.getStringList("topic").asScala.toList
  val topicWithKey = sqmKafkaConf.getBoolean("topicWithKey")
  val kafkaList:List[String] = sqmKafkaConf.getStringList("instances").asScala.toList

  def publishConfigToKafka(streamsQueryKafka: Option[String]): Future[Boolean] = {
    if (streamsQueryKafka.isEmpty) {
      return Future.failed(new NullPointerException("Config is missing"))
    }
    val payloadList: List[String] = List(streamsQueryKafka.get)
    val kafkaRequest = Option(KakfaPublisherPayload(topicsList, kafkaList, topicWithKey, payloadList))
    val future = GenerickKafkaPublisher.publishPayload(kafkaRequest)
    future
  }
}
