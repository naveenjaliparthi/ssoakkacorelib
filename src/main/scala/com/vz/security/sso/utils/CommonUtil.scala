package com.vz.security.sso.utils

import java.util.Date

object CommonUtil {

  /**
    * Fetches only required key value pairs from the inputMap
    */
  def filterKeys(inputMap: Map[String, Any], keys: List[String]): Map[String, Any] = {
    val nonNullMap = inputMap.filter(kv => kv._2 != null)
    keys.foldLeft(Map[String, Any]())((acc, key) => {
      acc + (key -> nonNullMap.getOrElse(key, ""))
    })
  }

  /**
    * Validates class existentce in given path. This can be used in require method while loading config
    * Returns false even if one path is not valid
    */
  def validateClassExistence(paths: Array[String]): Boolean = {
    val isValidList = paths.map(path => {
      try {
        Class.forName(path)
        true
      }
      catch {
        case ex: ClassNotFoundException =>
          println(s"No class found in given path: $path. Please correct and re-start the application")
          false
      }
    }
    )
    !isValidList.contains(false)
  }

  import java.text.SimpleDateFormat
  import java.util.TimeZone

  val PATTERN = "MM-dd-yy HH.mm.ss.sss"
  val sdf = new SimpleDateFormat(PATTERN)
  sdf.setTimeZone(TimeZone.getTimeZone("UTC"))

  def millisToUTC(millis: Long): String = {
    sdf.format(new Date(millis))
  }
}
