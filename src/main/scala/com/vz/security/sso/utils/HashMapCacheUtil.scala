package com.vz.security.sso.utils

import scala.collection.{Set, mutable}

/**
  * Created by khana8s on 10/2/2018.
  */
class HashMapCacheUtil extends mutable.Map[String, Long] {

  val verifiedAttributes: mutable.HashMap[String, Long] = mutable.HashMap[String, Long]()

  override def put(key: String, value: Long): Option[Long] = {
    removeExpiredElements()
    verifiedAttributes.put(key.toLowerCase, value)
  }

  override def remove(key: String): Option[Long] = {
    verifiedAttributes.remove(key.toLowerCase)
  }

  override def contains(key: String): Boolean = {
    verifiedAttributes.contains(key.toLowerCase)
  }

  override def get(key: String): Option[Long] = {
    verifiedAttributes.get(key.toLowerCase)
  }

  override def iterator: Iterator[(String, Long)] = verifiedAttributes.iterator map (e => (e._1, e._2))

  override def size(): Int = {
    verifiedAttributes.size
  }

  override def clear(): Unit = {
    verifiedAttributes.clear()
  }

  override def keySet(): Set[String] = {
    verifiedAttributes.keySet
  }

  // 30 days
  val numberOfDays = 30

  // This method removes expired elements from verifiedAttributes HashMap
  // If the difference between CurrentTime and the Element timeStamp is greater than 30 days,
  // delete that element from the cache
  def removeExpiredElements(): Unit = {
    verifiedAttributes.foreach {
      element =>
        if (System.currentTimeMillis() - element._2 > numberOfDays) {
          verifiedAttributes.remove(element._1)
        }
    }
  }

  override def +=(kv: (String, Long)): HashMapCacheUtil.this.type = ???

  override def -=(key: String): HashMapCacheUtil.this.type = ???
}
