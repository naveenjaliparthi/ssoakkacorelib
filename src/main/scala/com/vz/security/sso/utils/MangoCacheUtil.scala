package com.vz.security.sso.utils

import java.util.concurrent.TimeUnit

import com.vz.security.sso.common.exceptions.CustomExceptions.MangoException
import org.feijoas.mango.common.cache.CacheBuilder

class MangoCacheUtil(sessionMaxCacheSize: Long, sessionExpiryTimer: Long) {

  val sessionUserCache: org.feijoas.mango.common.cache.Cache[String, String] = CacheBuilder.newBuilder()
    .maximumSize(sessionMaxCacheSize)
    .expireAfterWrite(sessionExpiryTimer, TimeUnit.MINUTES)
    .build()

  def create(key: String, value: String): Boolean = {
    try {
      sessionUserCache.put(key.toLowerCase, value)
      true
    }
    catch {
      case ex: Exception =>
        throw MangoException(s"Failed to store cache for key: $key value: $value", ex.getMessage, ex.getCause)
    }
  }

  def read(key: String): Option[String] = {
    try {
      sessionUserCache.getIfPresent(key.toLowerCase)
    }
    catch {
      case ex: Exception =>
        throw MangoException(s"Failed to read cache for key: $key", ex.getMessage, ex.getCause)
    }
  }

  def update(key: String, value: String): Boolean = {
    try {
      sessionUserCache.put(key.toLowerCase, value)
      true
    }
    catch {
      case ex: Exception =>
        throw MangoException(s"Failed to udpate cache for key: $key with value: $value", ex.getMessage, ex.getCause)
    }
  }

  def delete(key: String): Boolean = {
    try {
      sessionUserCache.invalidate(key.toLowerCase)
      true
    }
    catch {
      case ex: Exception =>
        throw MangoException(s"Failed to delete cache for key: $key", ex.getMessage, ex.getCause)
    }
  }

  def readAll(): Option[List[String]] = {
    try {
      if (sessionUserCache.size() > 0) {
        Option(sessionUserCache.asMap().keys.toList)
      } else {
        None
      }
    }
    catch {
      case ex: Exception =>
        throw MangoException(s"Failed to read whole cache", ex.getMessage, ex.getCause)
    }
  }

  def search(key: String): Option[List[String]] = {
    try {
      val convertedKey = key.toLowerCase
      if (sessionUserCache.size() > 0) {
        Option(sessionUserCache.asMap().keys.filter(_.contains(convertedKey)).toList)
      } else {
        None
      }
    }
    catch {
      case ex: Exception =>
        throw MangoException(s"Failed to search cache for key: $key", ex.getMessage, ex.getCause)
    }
  }
}
