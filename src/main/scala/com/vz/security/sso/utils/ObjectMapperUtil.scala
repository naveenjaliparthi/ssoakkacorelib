package com.vz.security.sso.utils

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import com.vz.security.sso.common.exceptions.CustomExceptions.{MarshallException, UnmarshallException}

import scala.concurrent.{ExecutionContext, Future}

object ObjectMapperUtil {
  val mapper = new ObjectMapper()
    with ScalaObjectMapper
  mapper.registerModule(DefaultScalaModule)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

  def toJson(value: Any): String = {
    try {
      mapper.writeValueAsString(value)
    }
    catch {
      case ex: JsonProcessingException => throw MarshallException(s"Unable to marshall data from $value to json")
    }
  }

  def fromJson[T](json: String)(implicit m: Manifest[T]): T = {
    try {
      mapper.readValue[T](json)
    }
    catch {
      case ex: IllegalArgumentException =>
        throw UnmarshallException(s"Missing required fields in $json", ex.getMessage, ex.getCause)
      case ex: JsonProcessingException =>
        throw UnmarshallException(s"Unable to unmarshall data from $json", ex.getMessage, ex.getCause)
      case ex: NullPointerException =>
        throw UnmarshallException(s"Found null value while unmarshalling from json", ex.getMessage, ex.getCause)
    }
  }

  def fromJsonAsync[T](json: String, callerName: Option[String] = None)(implicit ec: ExecutionContext, m: Manifest[T]): Future[T] =
    Future {
      mapper.readValue[T](json)
    } recover {
      case ex: JsonProcessingException => throw UnmarshallException(s"callerName: ${callerName.getOrElse("Not specified")}. Unable to unmarshall data from $json", ex.getMessage, ex.getCause)
      case ex: NullPointerException => throw UnmarshallException(s"callerName: ${callerName.getOrElse("Not specified")}. Found null value while unmarshalling from json", ex.getMessage, ex.getCause)
    }
}
