package com.vz.security.sso.utils

import akka.actor.{Actor, ActorRef, ActorSystem, Props, ReceiveTimeout}
import akka.cluster.sharding.ShardRegion.{ExtractEntityId, ExtractShardId}
import akka.cluster.sharding.{ClusterSharding, ClusterShardingSettings, ShardRegion}
import akka.persistence._
import com.vz.security.sso.common.config.ConfigLoaderCore.grpcEnabled
import com.vz.security.sso.common.constants.SharedConstants
import com.vz.security.sso.common.constants.SharedConstants._
import com.vz.security.sso.grpc.services.ReplicationRequest
import com.vz.security.sso.grpc.util.GrpcUtil.replicateMessage
import com.vz.security.sso.logging.{VZLogger, VZMapLogger}
import com.vz.security.sso.models._

import scala.annotation.tailrec
import scala.collection.mutable

object ShardInventory {
  // To track number of actors created in Shard
  var numberOfActorsCreated = 0
}

/**
  * Extended by all persistent actors. Actor should have three steps defined
  * 1. Define Persistance model
  * 2. Business Logic using receiveWithReplicator. Do not override any other receive functions except receiveWithReplicator in client
  * 3. Recover strategy using handleSnapshotRecovery
  * 4. Define grpcToActorRequest to convert model from actor request to grpc request
  * 5. Define actorToGrpcRequest to convert model from grpc request to actor request
  */
trait PersistentShardActor extends PersistentActor {

  val mapLogger = new VZMapLogger(this)

  override def persistenceId: String = self.path.name

  override def snapshotPluginId: String = "akka-persistence-redis.snapshot"

  override def journalPluginId: String = "akka-persistence-redis.journal"

  override protected def onRecoveryFailure(cause: Throwable, event: Option[Any]): Unit = {
    val logger = new VZLogger(this, "persist_ssoEvent_actor_log")
    logger.addKeyValue("persistenceId", persistenceId)
    logger.addKeyValue("metadata", event)
    logger.addKeyValue("reason", cause.getMessage)
    logger.addKeyValue("stackTrace", cause.getStackTrace)
    logger.addKeyValue("log_source", "onRecoveryFailure")
    logger.error(cause)
    super.onRecoveryFailure(cause, event)
  }

  override protected def onPersistFailure(cause: Throwable, event: Any, seqNr: Long): Unit = {
    val logger = new VZLogger(this, "persist_ssoEvent_actor_log")
    logger.addKeyValue("persistenceId", persistenceId)
    logger.addKeyValue("metadata", event)
    logger.addKeyValue("reason", cause.getMessage)
    logger.addKeyValue("stackTrace", cause.getStackTrace)
    logger.addKeyValue("log_source", "onPersistFailure")
    logger.error(cause)
    super.onPersistFailure(cause, event, seqNr)
  }

  override def preStart(): Unit = {
    val logger = new VZLogger(this, "persist_ssoEvent_actor_log")
    logger.addKeyValue("operation", s"Actor Started : ${self.path.name}")
    logger.addKeyValue("persistenceId", persistenceId)
    logger.addKeyValue("log_source", "preStart()")
    logger.addKeyValue("numberOfActorsCreated", ShardInventory.numberOfActorsCreated += 1)
    logger.info
    super.preStart()
  }

  //Invoked when message is successfully persisted
  def onPersistSuccess(): Unit = {
    val logger = new VZLogger(this, "persist_ssoEvent_actor_log")
    logger.addKeyValue("type_alias", "persistSendEvent")
    logger.addKeyValue("eventPersist", "true")
    logger.info
  }

  /**
    * Handle Recovery
    */
  override def receiveRecover: Receive = {
    case SnapshotOffer(metadata, lastState: Any) =>
      val logger = new VZLogger(this, "ShardUtilSnapshotOffer")
      logger.addKeyValue("persistenceId", persistenceId)
      logger.addKeyValue("sequenceNr", metadata.sequenceNr)
      logger.addKeyValue("timestamp", metadata.timestamp)
      logger.info
      handleSnapshotRecovery(lastState, metadata)

    case RecoveryCompleted =>
      mapLogger.info("RecoveryCompleted", persistenceId, "status", "Actor Recovery Completed")

    case msg: Any =>
      val logMap = mutable.Map[String, Any]()
      logMap += ("Unknown message" -> msg)
      mapLogger.info("receiveRecover", persistenceId, logMap)
  }

  /**
    * This helps in message replication between data centers using GRPC channel
    * Messages other than replicatedRequest are sent to GRPC client iff grpcEnabled flag is set to true
    * All messages will be forwarded to receiveWithReplicator
    */
  override def receiveCommand: Receive = {
    case timeout: ReceiveTimeout =>
      receiveWithReplicator(timeout, sender(), false)

    case replicatedRequest: ReplicationRequest =>
      receiveWithReplicator(grpcToActorRequest(replicatedRequest), sender(), true)

    case message@_ =>
      if (grpcEnabled) {
        val msg = actorToGrpcRequestDuo(message)
        if (msg.rType != SharedConstants.unsupportedReplicationType) replicateMessage(msg)
      }
      receiveWithReplicator(message, sender(), false)
  }

  /**
    * Client application should override this function to process messages in Actor
    * Input: (Message, Sender, ReplicatedFlag)
    */
  def receiveWithReplicator: PartialFunction[(Any, ActorRef, Boolean), Unit]

  /**
    * Client application should override this function to process snapshot recovery
    */
  def handleSnapshotRecovery: PartialFunction[(Any, SnapshotMetadata), Unit]

  /**
    * Client application should override this function to convert GRPC request to Actor request
    * This request is further processed by receiveWithReplicator
    */
  def grpcToActorRequest: PartialFunction[ReplicationRequest, Any]

  /**
    * Client application should override this function to convert Actor request to GRPC request
    * This request is further replicated by receiveWithReplicator
    */
  def actorToGrpcRequest: PartialFunction[Any, ReplicationRequest]

  /**
    * Calls actorToGrpcRequest first and then actorToGrpcRequestDefault
    */
  def actorToGrpcRequestDuo: PartialFunction[Any, ReplicationRequest] =
    actorToGrpcRequest orElse actorToGrpcRequestDefault

  /**
    * This helps to filter out replicating unwanted actor messages
    */
  def actorToGrpcRequestDefault: PartialFunction[Any, ReplicationRequest] = {
    case m@_ => {
      val logInfo = mutable.Map[String, Any]()
      logInfo += ("persistenceId" -> persistenceId)
      logInfo += ("message" -> m.toString)
      mapLogger.debug(SharedConstants.unsupportedReplicationType, null, logInfo)
      ReplicationRequest(rMap = Map(), rType = SharedConstants.unsupportedReplicationType)
    }
  }

}

/**
  * Should extend by non persistent actors
  * 1. Business Logic using receiveWithReplicator. Do not override any other receive functions except receiveWithReplicator in client
  * 2. Define grpcToActorRequest to convert model from actor request to grpc request
  * 3. Define actorToGrpcRequest to convert model from grpc request to actor request
  */
trait NonPersistentShardActor extends Actor {

  val mapLogger = new VZMapLogger(this)

  override def receive: Receive = {
    case timeout: ReceiveTimeout =>
      receiveWithReplicator(timeout, sender(), false)

    case replicatedRequest: ReplicationRequest =>
      receiveWithReplicator(grpcToActorRequest(replicatedRequest), sender(), true)

    case message@_ =>
      if (grpcEnabled) {
        val msg = actorToGrpcRequestDuo(message)
        if (msg.rType != SharedConstants.unsupportedReplicationType) replicateMessage(msg)
      }
      receiveWithReplicator(message, sender(), false)
  }

  /**
    * Client application should override this function to process messages in Actor
    * Input: (Message, Sender, ReplicatedFlag)
    */
  def receiveWithReplicator: PartialFunction[(Any, ActorRef, Boolean), Unit]

  /**
    * Client application should override this function to convert GRPC request to Actor request
    * This request is further processed by receiveWithReplicator
    */
  def grpcToActorRequest: PartialFunction[ReplicationRequest, Any]

  /**
    * Client application should override this function to convert Actor request to GRPC request
    * This request is further replicated by receiveWithReplicator
    */
  def actorToGrpcRequest: PartialFunction[Any, ReplicationRequest]

  /**
    * Calls actorToGrpcRequest first and then actorToGrpcRequestDefault
    */
  def actorToGrpcRequestDuo: PartialFunction[Any, ReplicationRequest] =
    actorToGrpcRequest orElse actorToGrpcRequestDefault

  /**
    * This helps to filter out replicating unwanted actor messages
    */
  def actorToGrpcRequestDefault: PartialFunction[Any, ReplicationRequest] = {
    case m@_ => {
      val logInfo = mutable.Map[String, Any]()
      logInfo += ("actorName" -> self.path.name)
      logInfo += ("message" -> m.toString)
      mapLogger.debug(SharedConstants.unsupportedReplicationType, null, logInfo)
      ReplicationRequest(rMap = Map(), rType = SharedConstants.unsupportedReplicationType)
    }
  }

}


object ShardUtil {

  val mapLogger = new VZMapLogger(this)

  /**
    * Generic function to create a persistent shard
    *
    * @param shardname      Name of the shard
    * @param extractEntityF Function to extract entity
    * @param extractShardF  Function to extract shard region
    * @param role           Shard role
    * @param system         Actor system
    * @tparam T Actor/Entity inside shard that should extend PersistentShardActor
    * @return shard ref
    */
  def createPersistentShard[T <: Actor : Manifest](shardname: String,
                                                   extractEntityF: () => ShardRegion.ExtractEntityId,
                                                   extractShardF: () => ShardRegion.ExtractShardId,
                                                   role: String)(implicit system: ActorSystem): ActorRef = {

    val settings = ClusterShardingSettings(system).withRole(role)
    ClusterSharding(system).start(shardname, Props[T], settings, extractEntityF(), extractShardF())
  }

  /**
    * Generic function to create a non persistent shard
    *
    * @param shardname      Name of the shard
    * @param extractEntityF Function to extract entity
    * @param extractShardF  Function to extract shard region
    * @param role           Shard role
    * @param system         Actor system
    * @tparam T Actor/Entity inside shard that should extend NonPersistentShardActor
    * @return shard ref
    */
  def createNonPersistentShard[T <: Actor : Manifest](shardname: String,
                                                      extractEntityF: () => ShardRegion.ExtractEntityId,
                                                      extractShardF: () => ShardRegion.ExtractShardId,
                                                      role: String)(implicit system: ActorSystem): ActorRef = {
    val settings = ClusterShardingSettings(system).withRole(role)
    ClusterSharding(system).start(shardname, Props[T], settings, extractEntityF(), extractShardF())
  }

  /**
    * Proxy for user shard
    *
    * @param _typeName       Shard name
    * @param _role           Shard role
    * @param _numberOfShards Number of shards in cluster
    * @param system          Actor system
    * @return shardProxyRef
    */
  def userShardProxy(_typeName: String, _role: Option[String], _numberOfShards: Int)(implicit system: ActorSystem): ActorRef =
    ClusterSharding(system).startProxy(
      typeName = _typeName,
      role = _role,
      extractUserEntityId(),
      extractUserShardId(_numberOfShards)
    )

  def extractUserEntityId(): ShardRegion.ExtractEntityId = {
    case userAuthRiskScore: UserAuthRiskScore =>
      val userId = userAuthRiskScore.fields.getOrElse("userID", "N/A")
      (userId, userAuthRiskScore)

    case oauthUserRiskScoreRequest: OauthRiskScoreRequest =>
      val userId = oauthUserRiskScoreRequest.fields.getOrElse("userID", "N/A")
      (userId, oauthUserRiskScoreRequest)

    case pinData: PinDataWithUserId =>
      val userId = pinData.fields.getOrElse("userID", "N/A")
      (userId, pinData)

    case replicatedRequest: ReplicationRequest =>
      val userId = replicatedRequest.rMap.getOrElse("userID", "N/A")
      (userId, replicatedRequest)
  }

  def extractUserShardId(numberOfShards: Int): ShardRegion.ExtractShardId = {
    case userAuthRiskScore: UserAuthRiskScore =>
      val userId = userAuthRiskScore.fields.getOrElse("userID", "N/A")
      (math.abs(userId.hashCode()) % numberOfShards).toString

    case oauthUserRiskScoreRequest: OauthRiskScoreRequest =>
      val userId = oauthUserRiskScoreRequest.fields.getOrElse("userID", "N/A")
      (math.abs(userId.hashCode()) % numberOfShards).toString

    case pinData: PinDataWithUserId =>
      val userId = pinData.fields.getOrElse("userID", "N/A")
      (math.abs(userId.hashCode()) % numberOfShards).toString

    case replicatedRequest: ReplicationRequest =>
      val userId = replicatedRequest.rMap.getOrElse("userID", "N/A")
      (math.abs(userId.hashCode()) % numberOfShards).toString
  }

  /**
    * Proxy for generic shard
    *
    * @param _typeName       Shard name
    * @param _role           Shard role
    * @param _numberOfShards Number of shards in cluster
    * @param system          Actor system
    * @return shardProxyRef
    */
  def genericShardProxy(_typeName: String,
                        _role: Option[String],
                        _numberOfShards: Int)(implicit system: ActorSystem): ActorRef =
    ClusterSharding(system).startProxy(
      typeName = _typeName,
      role = _role,
      extractGenericEntityId(),
      extractGenericShardId(_numberOfShards)()
    )

  /**
    * This is used for both genericShardProxy and createPersistentShard
    * Returns function which returns ShardRegion.ExtractEntityId
    * NOTE: UserActor uses only SHARDVALUE as entityId. This will change later
    */
  def extractGenericEntityId: () => ExtractEntityId = {
    lazy val extractEntityId: () => ShardRegion.ExtractEntityId = () => {
      case genericMapRequest: GenericMapRequest =>
        val map = genericMapRequest.fields
        if (map.getOrElse(SHARDKEY, "N/A").equalsIgnoreCase(USERID)) {
          (map.getOrElse(SHARDVALUE, "N/A"), genericMapRequest)
        }
        else {
          (map.getOrElse(SHARDKEY, "N/A") + ":" + map.getOrElse(SHARDVALUE, "N/A"), genericMapRequest)
        }

      case replicatedRequest: ReplicationRequest =>
        val map = replicatedRequest.rMap
        if (map.getOrElse(SHARDKEY, "").equalsIgnoreCase(USERID)) {
          (map.getOrElse(SHARDVALUE, "N/A"), replicatedRequest)
        }
        else {
          (map.getOrElse(SHARDKEY, "N/A") + ":" + map.getOrElse(SHARDVALUE, "N/A"), replicatedRequest)
        }
    }
    extractEntityId
  }

  /**
    * This is used for both genericShardProxy and createPersistentShard
    * Function is wrapped inside def to hardcode numberOfShards in function
    * Returns function which returns ShardRegion.ExtractShardId
    * NOTE: UserActor uses only SHARDVALUE as entityId. This will change later
    */
  def extractGenericShardId(numberOfShards: Int): () => ExtractShardId = {
    lazy val extractShardId: () => ShardRegion.ExtractShardId = () => {
      case genericMapRequest: GenericMapRequest =>
        val map = genericMapRequest.fields
        if (map.getOrElse(SHARDKEY, "").equalsIgnoreCase(USERID)) {
          val userId = map.getOrElse(SHARDVALUE, "N/A")
          (math.abs(userId.hashCode()) % numberOfShards).toString
        } else {
          val id = map.getOrElse(SHARDKEY, "N/A") + ":" + map.getOrElse(SHARDVALUE, "N/A")
          (math.abs(id.hashCode()) % numberOfShards).toString
        }

      case replicatedRequest: ReplicationRequest =>
        val map = replicatedRequest.rMap
        if (map.getOrElse(SHARDKEY, "").equalsIgnoreCase(USERID)) {
          val userId = map.getOrElse(SHARDVALUE, "N/A")
          (math.abs(userId.hashCode()) % numberOfShards).toString
        } else {
          val id = map.getOrElse(SHARDKEY, "N/A") + ":" + map.getOrElse(SHARDVALUE, "N/A")
          (math.abs(id.hashCode()) % numberOfShards).toString
        }
    }
    extractShardId
  }

  /**
    * *****************************
    * *** REAL TIME QUERY SHARD ***
    * *****************************
    */
  /**
    * Proxy for RT shard
    *
    * @param _typeName       Shard name
    * @param _role           Shard role
    * @param _numberOfShards Number of shards in cluster
    * @param system          Actor system
    * @return shardProxyRef
    */
  def rtShardProxy(_typeName: String,
                   _role: Option[String],
                   _numberOfShards: Int)(implicit system: ActorSystem): ActorRef =
    ClusterSharding(system).startProxy(
      typeName = _typeName,
      role = _role,
      extractRTEntityId(),
      extractRTShardId(_numberOfShards)()
    )

  def getRTActorId(qid: String, shard: String, parallelism: Int, map: Map[String, String]): String = {
    try {
      qid + ":" + (map(shard).hashCode() % parallelism)
    }
    catch {
      case ex: Exception =>
        ""
    }
  }

  /**
    * Function is wrapped inside def to hardcode numberOfShards in function
    * Returns function which returns ShardRegion.ExtractShardId
    */
  def extractRTShardId(numberOfShards: Int): () => ExtractShardId = {
    lazy val extractShardId: () => ShardRegion.ExtractShardId = () => {
      case logMessage: GenericMapRequest =>
        (math.abs(logMessage.fields(ACTORID).hashCode()) % numberOfShards).toString

      case replicatedRequest: ReplicationRequest =>
        (math.abs(replicatedRequest.rMap(ACTORID).hashCode()) % numberOfShards).toString
    }
    extractShardId
  }

  /**
    * Returns function which returns ShardRegion.ExtractEntityId
    */
  def extractRTEntityId: () => ExtractEntityId = {
    lazy val extractEntityId: () => ShardRegion.ExtractEntityId = () => {
      case logMessage: GenericMapRequest =>
        (logMessage.fields(ACTORID), logMessage)

      case replicatedRequest: ReplicationRequest =>
        (replicatedRequest.rMap(ACTORID), replicatedRequest)
    }
    extractEntityId
  }

  /**
    * @param payload  all input params
    * @param myScore  computed risk score
    * @param myReason reason for decision
    * @return input map plus added key values for chain implementation and callNextActor flag
    */
  def computeNextPhase(payload: Map[String, String], myScore: Option[String] = None, myReason: Option[String] = None): (Map[String, String], String) = {
    try {
      val finalScoreAndReason = {
        val scoreAndRes = (payload.get(RISKSCORE), myScore) match {
          case (Some(prevScore), Some(currentScore)) =>
            if (currentScore.toInt > prevScore.toInt) (currentScore, myReason.getOrElse(NOREASON)) else (prevScore, payload.getOrElse(REASONFORDECISION, NOREASON))
          case (Some(prevScore), None) => (prevScore, payload.getOrElse(REASONFORDECISION, NOREASON))
          case (None, Some(currentScore)) => (currentScore, myReason.getOrElse(NOREASON))
          case (None, None) => (defaultRiskScore, defaultRiskScoreMessage)
        }
        (scoreAndRes._1.toInt, scoreAndRes._2)
      }
      val chainInfo = findNextActor(payload)
      //Pass to next actor
      if (chainInfo.nonEmpty && finalScoreAndReason._1 < payload.getOrElse(ACTORTHRESHOLD, MAXTHRESHOLD).toInt) {
        if (chainInfo.get.actorType == ACTORID) {
          (payload ++ Map(RISKSCORE -> finalScoreAndReason._1.toString,
            REASONFORDECISION -> finalScoreAndReason._2,
            chainInfo.get.actorType -> chainInfo.get.shardValue,
            ACTORTHRESHOLD -> chainInfo.get.threshold,
            ACTORSTOCALL -> chainInfo.get.remainingChain.mkString(CHAINDELIMITER)), RTSHARD)
        }
        else {
          (payload ++ Map(RISKSCORE -> finalScoreAndReason._1.toString,
            REASONFORDECISION -> finalScoreAndReason._2,
            SHARDKEY -> chainInfo.get.actorType,
            ACTORTHRESHOLD -> chainInfo.get.threshold,
            SHARDVALUE -> chainInfo.get.shardValue,
            ACTORSTOCALL -> chainInfo.get.remainingChain.mkString(CHAINDELIMITER)), GENERICSHARD)
        }
      }
      //Reply to gateway
      else {
        (payload ++ Map(RISKSCORE -> finalScoreAndReason._1.toString, REASONFORDECISION -> finalScoreAndReason._2, ACTORSTOCALL -> ""), REPLY)
      }
    }
    catch {
      case ex: Exception =>
        val logInfo = mutable.Map[String, Any]()
        payload.foreach(kv => logInfo += ("input_" + kv._1 -> kv._2))
        logInfo += ("myScore" -> myScore.getOrElse("NoScoreFound"))
        logInfo += ("myReason" -> myScore.getOrElse(NOREASON))
        mapLogger.error("computeNextPhase", null, ex, logInfo)
        (payload ++ Map(RISKSCORE -> defaultRiskScore, REASONFORDECISION -> defaultRiskScoreMessage, ACTORSTOCALL -> ""), REPLY)
    }
  }

  /**
    * Traverse through actor chain until it finds shard value and returns ChainInfo
    */
  def findNextActor(payload: Map[String, String]): Option[ChainInfo] = {
    @tailrec
    def fna(chain: List[String]): Option[ChainInfo] = {
      chain match {
        case Nil => None
        case first :: remaining =>
          val actorTypeAndThreshold = first.split(SCOREDELIMITER).map(_.trim)
          if (actorTypeAndThreshold.length == 2) {
            val actorType = actorTypeAndThreshold(0)
            val thresholdScore = actorTypeAndThreshold(1)
            val shardValue = payload.get(actorType)
            if (shardValue.getOrElse("").nonEmpty) Some(ChainInfo(actorType, thresholdScore, shardValue.get, remaining))
            else fna(remaining)
          }
          else if (actorTypeAndThreshold.length == 4) {
            val qid = actorTypeAndThreshold(0)
            val shard = actorTypeAndThreshold(1)
            val par = actorTypeAndThreshold(2).toInt
            val thresholdScore = actorTypeAndThreshold(3)
            val shardValue = getRTActorId(qid, shard, par, payload)
            if (shardValue.nonEmpty) Some(ChainInfo(ACTORID, thresholdScore, shardValue, remaining))
            else fna(remaining)
          }
          else {
            fna(remaining)
          }
      }
    }

    val actorChain = payload.getOrElse(ACTORSTOCALL, "").split(CHAINDELIMITER).toList.map(_.trim)
    try {
      fna(actorChain)
    }
    catch {
      case ex: Exception =>
        val logInfo = mutable.Map[String, Any]()
        payload.foreach(kv => logInfo += ("input_" + kv._1 -> kv._2))
        mapLogger.error("findNextActor", null, ex, logInfo)
        None
    }
  }


}
