package com.vz.security.sso.utils.streams

import akka.NotUsed
import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage.{CommittableMessage, CommittableOffset}
import akka.stream.ActorAttributes
import akka.stream.scaladsl.Flow
import com.vz.security.sso.common.config.ConfigLoaderCore
import com.vz.security.sso.logging.VZMapLogger
import com.vz.security.sso.models.Command
import com.vz.security.sso.utils.{CommonUtil, ObjectMapperUtil}
import com.vz.security.sso.utils.streams.SupervisionStrategyUtil.{flowDecider, unmarshallingFlowDecider}
import org.apache.kafka.clients.consumer.ConsumerRecord

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{Duration, FiniteDuration}

object FlowUtil {

  val logger = new VZMapLogger(this)

  /**
    * AKKA STREAMS UNMARSHALL FLOW
    * @deprecated Use unMarshallFlowCopy instead
    */
  def unMarshallFlow[T]()(implicit system: ActorSystem, ec: ExecutionContext, m: Manifest[T]) =
    Flow[ConsumerRecord[String, String]]
      .mapAsync(ConfigLoaderCore.parallelism) {
        consumerRecord =>
          ObjectMapperUtil.fromJsonAsync[T](consumerRecord.value())
      }
      .withAttributes(ActorAttributes.supervisionStrategy(unmarshallingFlowDecider))

  def unMarshallFlowCopy[T](callerName: Option[String] = None)(implicit system: ActorSystem, ec: ExecutionContext, m: Manifest[T]) =
    Flow[ConsumerRecord[String, String]]
      .mapAsync(ConfigLoaderCore.parallelism) {
        consumerRecord =>
          ObjectMapperUtil.fromJsonAsync[T](consumerRecord.value(), callerName)
      }
      .withAttributes(ActorAttributes.supervisionStrategy(unmarshallingFlowDecider))

  /**
    * Akka streams unmarshall flow with CommittableMessage
    * Only to plugin with kafka source
    */
  def unMarshallFlowWithCommittableMessage[T]()
                                             (implicit system: ActorSystem, ec: ExecutionContext, m: Manifest[T])
  : Flow[CommittableMessage[String, String], (T, CommittableOffset), NotUsed] = {
    Flow[CommittableMessage[String, String]]
      .map(
        consumerRecord =>
          (ObjectMapperUtil.fromJson[T](consumerRecord.record.value()), consumerRecord.committableOffset)
      )
      .withAttributes(ActorAttributes.supervisionStrategy(unmarshallingFlowDecider))
  }

  /**
    * Filters only required keys from input map
    */
  def mapperFlow(keys: List[String])(implicit system: ActorSystem, ec: ExecutionContext) = {
    logger.info("MapperFlow", null, "status", s"Started with keys $keys")
    Flow[Map[String, Any]]
      .map(inputMap => CommonUtil.filterKeys(inputMap, keys))
      .withAttributes(ActorAttributes.supervisionStrategy(flowDecider))
  }

  /**
    * @param buffer   elements is buffered
    * @param duration Duration to hold elements
    * @tparam T Type parameter
    */
  def flowWithgroupBy[T](buffer: Int, duration: FiniteDuration) = {
    logger.info("FlowWithgroupBy", null, "status", s"Started with buffer $buffer and duration: ${duration.toString()}")
    Flow[T].groupedWithin(buffer, duration)
      .withAttributes(ActorAttributes.supervisionStrategy(flowDecider))
  }

  /**
    * This flow has core logic where it maintains a state
    * Creates windows over that state and holds upstream data
    * Triggers reduce logic when it receives tick to slide
    */
  def slidingWindowFlow(reducer: Vector[Map[String, Any]] => Vector[(String, String, String)],
                        windowInSec: Int) = {
    logger.info("slidingWindowFlow", null, "status", s"Started with windowInSec $windowInSec")
    Flow[Command].statefulMapConcat { () =>
      val statefulHelper = new SlidingWindowingHelper(reducer, windowInSec)
      ev => statefulHelper.action(ev)
    }.withAttributes(ActorAttributes.supervisionStrategy(flowDecider))
  }

  def FilterKeyFlow(filterKey: String): Flow[ConsumerRecord[String, String], ConsumerRecord[String, String], NotUsed] = {
    logger.info("FilterKeyFlow", null, "status", s"Started with filterKey $filterKey")
    Flow[ConsumerRecord[String, String]]
      .filter(c => {
        if (Option(c.key()).isDefined) c.key().equalsIgnoreCase(filterKey)
        else false
      })
      .withAttributes(ActorAttributes.supervisionStrategy(flowDecider))
  }

  def FilterKeyFlowWithCommittableOffset(filterKey: String): Flow[CommittableMessage[String, String], CommittableMessage[String, String], NotUsed] = {
    logger.info("FilterKeyFlowWithCommittableOffset", null, "status", s"Started with filterKey $filterKey")
    Flow[CommittableMessage[String, String]]
      .filter(c => {
        if (Option(c.record.key()).isDefined) c.record.key().equalsIgnoreCase(filterKey)
        else {
          false
        }
      })
      .withAttributes(ActorAttributes.supervisionStrategy(flowDecider))
  }
}
