package com.vz.security.sso.utils.streams

import akka.actor.{ActorRef, ActorSystem}
import akka.dispatch.MessageDispatcher
import akka.stream.scaladsl.{Flow, SourceQueueWithComplete}
import akka.stream.{ActorAttributes, ActorMaterializer}
import com.vz.security.sso.logging.VZMapLogger
import com.vz.security.sso.utils.streams.SinkUtil.kafkaRestartableSinkNoKey
import com.vz.security.sso.utils.streams.SupervisionStrategyUtil.flowDecider

import scala.collection.mutable
import scala.concurrent.ExecutionContext

object KafkaPublisherUtil {

  val logger = new VZMapLogger(this)

  /**
    * Queue stream which takes tuple of topic, value and publish to kafka
    *
    * @param bootStrapServers kafka bootstrap servers
    * @param kafkaProps       kafka properties as a map
    * @param streamName       logging purpose to distinguish streams throughout app
    */
  def kafkaPublisherStream(bootStrapServers: String,
                           kafkaProps: Option[Map[String, String]],
                           streamName: Option[String])
                          (implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext): ActorRef = {
    val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
    logmap += ("bootStrapServers" -> bootStrapServers)
    logmap += ("kafkaProps" -> kafkaProps.getOrElse(Map()))
    logmap += ("streamName" -> streamName.getOrElse("NameNotDefined"))
    logger.info("KafkaPublisher", null, logmap)

    val kafkaStreamInput: ActorRef = SourceUtil.actorSource[(String, String)](10000)
      .to(kafkaRestartableSinkNoKey(bootStrapServers, kafkaProps))
      .withAttributes(ActorAttributes.supervisionStrategy(SupervisionStrategyUtil.systemDecider))
      .run()
    kafkaStreamInput
  }

}
