package com.vz.security.sso.utils.streams

import akka.actor.{ActorRef, ActorSystem}
import akka.kafka.{ConsumerMessage, ProducerSettings}
import akka.kafka.ConsumerMessage.CommittableOffsetBatch
import akka.kafka.scaladsl.Producer
import akka.stream.ActorAttributes
import akka.stream.alpakka.elasticsearch.scaladsl.{ElasticsearchFlow, ElasticsearchSink}
import akka.stream.alpakka.elasticsearch.{ElasticsearchWriteSettings, RetryAtFixedRate}
import akka.stream.scaladsl.{Flow, Keep, Sink}
import com.vz.security.sso.common.config.ConfigLoaderCore
import com.vz.security.sso.common.config.ConfigLoaderCore._
import com.vz.security.sso.common.connectors.KafkaConnector
import com.vz.security.sso.logging.VZMapLogger
import com.vz.security.sso.utils.streams.SupervisionStrategyUtil.sinkDecider
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import spray.json.JsonWriter

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

object SinkUtil {

  val logger = new VZMapLogger(this)

  /**
    * AKKA STREAMS KAFKA PRODUCER SETTINGS
    */
  private def setupProducerSettings(bootStrapServers: String, props: Option[Map[String, String]])(implicit system: ActorSystem): ProducerSettings[String, String] =
    KafkaConnector.configKafkaProducerSettings(
      bootStrapServers,
      new StringSerializer,
      new StringSerializer,
      props
    )

  /**
    * Kafka Sink which takes tuple of topic, key, value
    * props is defined to connect to secure kafka
    * java.security.auth.login.config should be set in client application to enable secure kafka
    * ex: System.setProperty("java.security.auth.login.config", conf.getString("sso.cache.kafka.jaas"))
    */
  def kafkaRestartableSink(bootStrapServers: String, props: Option[Map[String, String]])(implicit system: ActorSystem, ec: ExecutionContext) = {
    logger.info("kafkaRestartableSink", null, "status", s"Started with bootStrapServers: $bootStrapServers and props: $props")
    Flow[(String, String, String)]
      .mapAsync(ConfigLoaderCore.producerParallelism) {
        tup => {
          Future {
            new ProducerRecord(tup._1, tup._2, tup._3)
          }
        }
      }
      .toMat(Producer.plainSink(setupProducerSettings(bootStrapServers, props)))(Keep.right)
      .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
  }


  /**
    * Kafka Sink which takes tuple of topic, value
    * props is defined to connect to secure kafka
    * java.security.auth.login.config should be set in client application to enable secure kafka
    * ex: System.setProperty("java.security.auth.login.config", conf.getString("sso.cache.kafka.jaas"))
    */
  def kafkaRestartableSinkNoKey(bootStrapServers: String, props: Option[Map[String, String]])(implicit system: ActorSystem, ec: ExecutionContext) = {
    logger.info("kafkaRestartableSinkNoKey", null, "status", s"Started with bootStrapServers: $bootStrapServers and props: $props")
    Flow[(String, String)]
      .mapAsync(ConfigLoaderCore.producerParallelism) {
        tup => {
          Future {
            new ProducerRecord[String, String](tup._1, tup._2)
          }
        }
      }
      .toMat(Producer.plainSink(setupProducerSettings(bootStrapServers, props)))(Keep.right)
      .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
  }

  /**
    * Sink that sends message to shard region
    */
  def shardSink[T](shardref: ActorRef)(implicit system: ActorSystem, ec: ExecutionContext)
  = {
    logger.info("shardSink", null, "status", "started")
    Flow[T]
      .mapAsync(10)(
        message => Future {
          shardref ! message
        }
      )
      .toMat(Sink.ignore)(Keep.right)
      .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
  }

  /**
    * Sink that sends message to shard region
    * and commit offsets to kafka
    */
  def shardSinkWithCommittableOffset[T](shardref: ActorRef)
                                       (implicit system: ActorSystem, ec: ExecutionContext)
  = {
    logger.info("shardSinkWithCommittableOffset", null, "status", "started")
    Flow[(T, ConsumerMessage.CommittableOffset)]
      .mapAsync(10)(
        message => Future {
          shardref ! message
          message._2
        }
      )
      .batch(max = 2000, first => CommittableOffsetBatch.empty.updated(first)) { (batch, elem) =>
        batch.updated(elem)
      }
      .mapAsync(3)(_.commitScaladsl())
      .toMat(Sink.ignore)(Keep.right)
      .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
  }

  lazy val sinkSettings =
    ElasticsearchWriteSettings()
      .withBufferSize(elasticSinkBufferSize)
      .withRetryLogic(RetryAtFixedRate(maxRetries = elasticSinkMaxRetries, retryInterval = elasticSinkRetryIntervalInSec.second))

  /**
    * Elastic Sink that writes to elastic database
    */
  def elasticSink[T](_indexName: String, _typeName: String)(implicit fmt: JsonWriter[T], system: ActorSystem, ec: ExecutionContext) = {
    import com.vz.security.sso.common.connectors.ElasticConnector.client
    logger.info("elasticSink", null, "status", s"Started elasticSink with Index: ${_indexName}, typeName: ${_typeName}, bufferSize: ${elasticSinkBufferSize} and Maxretries: ${elasticSinkMaxRetries}")
    ElasticsearchSink.create[T](
      indexName = _indexName,
      typeName = _typeName,
      sinkSettings
    ).withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
  }

  /**
    * Elastic Sink that writes to elastic database
    * and commit offsets to kafka
    */
  def elasticSinkWithCommittableOffset[T](_indexName: String, _typeName: String)
                                         (implicit fmt: JsonWriter[T], system: ActorSystem, ec: ExecutionContext) = {
    import com.vz.security.sso.common.connectors.ElasticConnector.client
    logger.info("elasticSinkWithCommittableOffset", null, "status", s"Started elasticSinkWithCommittableOffset with Index: ${_indexName}, typeName: ${_typeName} bufferSize: ${elasticSinkBufferSize} and Maxretries: ${elasticSinkMaxRetries}")
    ElasticsearchFlow.createWithPassThrough[T, ConsumerMessage.CommittableOffset](
      indexName = _indexName,
      typeName = _typeName,
      sinkSettings
    ).map { result =>
      if (!result.success) throw new Exception("Failed to write message to elastic")
      // Commit to kafka
      result.message.passThrough
    }.batch(max = 2000, first => CommittableOffsetBatch.empty.updated(first)) { (batch, elem) =>
      batch.updated(elem)
    }
      .mapAsync(3)(_.commitScaladsl())
      .toMat(Sink.ignore)(Keep.right)
      .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
  }

  /**
    * Sink to commit offsets to kafka
    */
  def committableOffsetSink()(implicit system: ActorSystem, ec: ExecutionContext) = {
    logger.info("committableOffsetSink", null, "status", "started")
    Flow[ConsumerMessage.CommittableOffset]
      .batch(max = 2000, first => CommittableOffsetBatch.empty.updated(first)) { (batch, elem) =>
        batch.updated(elem)
      }
      .mapAsync(3)(_.commitScaladsl())
      .toMat(Sink.ignore)(Keep.right)
      .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
  }
}

