package com.vz.security.sso.utils.streams

import com.vz.security.sso.common.constants.SharedConstants._
import com.vz.security.sso.logging.VZMapLogger
import com.vz.security.sso.models.{AggMapper, Command, SlidingTick}
import com.vz.security.sso.utils.CommonUtil.millisToUTC

class SlidingWindowingHelper(reducer: Vector[Map[String, Any]] => Vector[(String, String, String)],
                             windowDurationInSec: Int) {

  val logger = new VZMapLogger(this)

  /**
    * In-Memory Time Series Map
    */
  val tsMap = new TimeSeriesMap[Map[String, Any]]()

  /**
    * Performs action based on command
    *
    * @AggMapper: Stores upstream data in maputil
    * @SlidingTick: 1. Applies reduce logic on latest window data
    *               2. Cleans expired elements from maputil
    */
  def action(command: Command): Vector[(String, String, String)] = {
    try {
      command match {
        case AggMapper(payload) =>
          val time = payload.get(now)
          time match {
            case Some(value: Long) => store(value, payload)
            case None =>
              logger.error("action", null, "error", s"could not find time in payload $payload")
              Vector()
          }
        case SlidingTick(time) =>
          cleanup(time)
          reduce(time)
      }
    }
    catch {
      case ex: Exception =>
        logger.error("action", null, ex, "CustomMessage", s"Unable to execute command: $command")
        Vector()
    }
  }

  /**
    * Stores data in time series map
    */
  private def store(key: Long, value: Map[String, Any]): Vector[(String, String, String)] = {
    tsMap.add(key, value)
    Vector[(String, String, String)]()
  }

  /**
    * Cleans expired messages from maputil
    */
  private def cleanup(tickTime: Long) = {
    val removeBefore = tickTime - windowDurationInSec * 1000l
    tsMap.removeExpiredElements(removeBefore)
  }

  /**
    * @param tickTime defines when to slide
    * @return latest window duration data
    */
  private def collectWindowData(tickTime: Long): Vector[Map[String, Any]] = {
    val fromTime = tickTime - windowDurationInSec * 1000l
    val resultMap = tsMap.getRange(fromTime, tickTime)
    logger.debug("collectWindowData", null, "details", s"Collected ${resultMap.size} records from ${millisToUTC(fromTime)} to ${millisToUTC(tickTime)} UTC")
    resultMap.map(_._2)
  }

  /**
    * @param tickTime Sliding time
    * @return Tuple of kafkaTopic, ip, AggregatedCountMessage
    */
  private def reduce(tickTime: Long): Vector[(String, String, String)] = {
    val windowData = collectWindowData(tickTime)
    reducer(windowData)
  }
}