package com.vz.security.sso.utils.streams

import akka.NotUsed
import akka.actor.{ActorSystem, Cancellable}
import akka.kafka.scaladsl.Consumer
import akka.kafka.scaladsl.Consumer.Control
import akka.kafka.{ConsumerMessage, ConsumerSettings, Subscriptions}
import akka.stream.alpakka.elasticsearch.{ElasticsearchSourceSettings, ReadResult}
import akka.stream.alpakka.elasticsearch.scaladsl.ElasticsearchSource
import akka.stream.scaladsl.{RestartSource, Source}
import akka.stream.{ActorAttributes, OverflowStrategy}
import com.vz.security.sso.common.config.ConfigLoaderCore
import com.vz.security.sso.common.connectors.KafkaConnector
import com.vz.security.sso.logging.VZMapLogger
import com.vz.security.sso.models.SlidingTick
import com.vz.security.sso.utils.ObjectMapperUtil
import com.vz.security.sso.utils.streams.SupervisionStrategyUtil.sourceDecider
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import spray.json.JsonReader

import scala.collection.mutable
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

object SourceUtil {

  val logger = new VZMapLogger(this)

  /**
    * AKKA STREAMS KAFKA CONSUMER SOURCE
    */
  private def setupConsumerSettings(groupId: String, bootStrapServers: String, props: Map[String, String])(implicit system: ActorSystem) =
    KafkaConnector.configkafkaConsumerSettings(bootStrapServers,
      groupId,
      new StringDeserializer,
      new StringDeserializer,
      props)(system)


  private def configPlainKafkaSource[K, V](_consumerSettings: ConsumerSettings[K, V], _topics: Set[String])
  : Source[ConsumerRecord[K, V], Control] =
    Consumer.plainSource(_consumerSettings, Subscriptions.topics(_topics))

  private def startPlainConsumerSource(groupid: String, topics: Set[String], bootStrapServers: String, props: Map[String, String])(implicit system: ActorSystem) =
    configPlainKafkaSource(setupConsumerSettings(groupid, bootStrapServers, props), topics).map { e => e }

  /**
    * Restartable Kafka with plain Consumer Source.
    * java.security.auth.login.config should be set in client application to enable secure kafka
    * ex: System.setProperty("java.security.auth.login.config", conf.getString("sso.cache.kafka.jaas"))
    */
  def kafkaConsumerPlainSource(groupid: String, topics: Set[String], bootStrapServers: String, props: Map[String, String])(implicit system: ActorSystem)
  : Source[ConsumerRecord[String, String], NotUsed] = {
    var sourceRetryCount: Long = 1
    RestartSource.withBackoff(
      minBackoff = ConfigLoaderCore.minBackoff.seconds,
      maxBackoff = ConfigLoaderCore.maxBackoff.seconds,
      randomFactor = ConfigLoaderCore.randomFactor
    ) { () =>
      var sourceInfoMap = mutable.Map[String, Any]()
      sourceInfoMap += ("bootStrapServers" -> bootStrapServers)
      sourceInfoMap += ("topics" -> topics)
      sourceInfoMap += ("groupid" -> groupid)
      sourceInfoMap += ("props" -> ObjectMapperUtil.toJson(props))
      if (sourceRetryCount == 1) {
        sourceRetryCount = sourceRetryCount + 1
        logger.info("kafkaConsumerPlainSource", null, sourceInfoMap)
      }
      else {
        sourceRetryCount = sourceRetryCount + 1
        sourceInfoMap += ("kafkaConsumerPlainSource" -> s"Restarting consumer on failure $sourceRetryCount")
        logger.info("kafkaConsumerPlainSource", null, sourceInfoMap)
      }
      startPlainConsumerSource(groupid, topics, bootStrapServers, props)
    }
  }.withAttributes(ActorAttributes.supervisionStrategy(sourceDecider))

  private def configCommittableKafkaSource[K, V](_consumerSettings: ConsumerSettings[K, V], _topics: Set[String])
  : Source[ConsumerMessage.CommittableMessage[K, V], Control] =
    Consumer.committableSource(_consumerSettings, Subscriptions.topics(_topics))

  private def startCommittableConsumerSource(groupid: String, topics: Set[String], bootStrapServers: String, props: Map[String, String])(implicit system: ActorSystem) =
    configCommittableKafkaSource(setupConsumerSettings(groupid, bootStrapServers, props), topics).map { e => e }

  /**
    * Restartable Kafka with committable Consumer Source.
    * java.security.auth.login.config should be set in client application to enable secure kafka
    * ex: System.setProperty("java.security.auth.login.config", conf.getString("sso.cache.kafka.jaas"))
    */
  def kafkaConsumerCommittableSource(groupid: String, topics: Set[String], bootStrapServers: String, props: Map[String, String])(implicit system: ActorSystem)
  : Source[ConsumerMessage.CommittableMessage[String, String], NotUsed] = {
    var sourceRetryCount: Long = 1
    RestartSource.withBackoff(
      minBackoff = ConfigLoaderCore.minBackoff.seconds,
      maxBackoff = ConfigLoaderCore.maxBackoff.seconds,
      randomFactor = ConfigLoaderCore.randomFactor
    ) { () =>
      var sourceInfoMap = mutable.Map[String, Any]()
      sourceInfoMap += ("bootStrapServers" -> bootStrapServers)
      sourceInfoMap += ("topics" -> topics)
      sourceInfoMap += ("groupid" -> groupid)
      sourceInfoMap += ("props" -> ObjectMapperUtil.toJson(props))
      if (sourceRetryCount == 1) {
        sourceRetryCount = sourceRetryCount + 1
        logger.info("kafkaConsumerCommittableSource", null, sourceInfoMap)
      }
      else {
        sourceRetryCount = sourceRetryCount + 1
        sourceInfoMap += ("kafkaConsumerCommittableSource" -> s"Restarting consumer on failure $sourceRetryCount")
        logger.info("kafkaConsumerCommittableSource", null, sourceInfoMap)
      }
      startCommittableConsumerSource(groupid, topics, bootStrapServers, props)
    }
  }.withAttributes(ActorAttributes.supervisionStrategy(sourceDecider))


  /**
    *  Source.queue pushes elements to the queue and they
    * will be emitted to the stream if there is demand from downstream,
    * otherwise they will be buffered until request for
    * demand is received.
    *
    * @param buffersize Maximum buffer size
    */
  def queueSource[T](buffersize: Int) = {
    Source.queue[T](buffersize, OverflowStrategy.dropHead)
      .map(event => {
        event
      })
  }.withAttributes(ActorAttributes.supervisionStrategy(sourceDecider))

  /**
    * Source with actor ref which also handles backpressure
    *
    * @param buffersize Maximum buffer size
    */
  def actorSource[T](buffersize: Int) = {
    Source.actorRef[T](buffersize, OverflowStrategy.dropHead)
      .map(event => {
        event
      })
  }.withAttributes(ActorAttributes.supervisionStrategy(sourceDecider))

  /**
    * Source which triggers currentMillis every given finite duration parameter.
    */
  //Todo: Enhance this to produce user desired output instead of hardcoded SlidingTick
  def tickSource(initialDelay: Int, finiteDuration: Int): Source[SlidingTick, Cancellable] = {
    logger.info("tickSource", null, "status", s"starting with initialDelay: $initialDelay and finiteDuration: $finiteDuration")
    Source
      .tick(initialDelay.seconds, finiteDuration.second, "")
      .map(s => SlidingTick(System.currentTimeMillis()))
  }.withAttributes(ActorAttributes.supervisionStrategy(sourceDecider))

  // Elastic Source
  def elasticSource[T](_indexName: String, _typeName: String, _query: String)
                      (implicit fmt: JsonReader[T], system: ActorSystem, ec: ExecutionContext) = {
    import com.vz.security.sso.common.connectors.ElasticConnector.client

    var elasticInfoMap = mutable.Map[String, Any]()
    elasticInfoMap += ("_indexName" -> _indexName)
    elasticInfoMap += ("_typeName" -> _typeName)
    elasticInfoMap += ("_query" -> _query)
    logger.info("ElasticSource", null, elasticInfoMap)

    ElasticsearchSource.create(
      indexName = _indexName,
      typeName = _typeName,
      query = _query
    )
      .map { message: ReadResult[spray.json.JsObject] =>
        (message.id, message.source.convertTo[T])
      }
  }.withAttributes(ActorAttributes.supervisionStrategy(sourceDecider))

  // Elastic Source with search params
  def elasticSourceWithSearchParams[T](_indexName: String, _typeName: String, _searchParams: Map[String, String])
                                      (implicit fmt: JsonReader[T], system: ActorSystem, ec: ExecutionContext) = {
    import com.vz.security.sso.common.connectors.ElasticConnector.client
    ElasticsearchSource.create(
      indexName = _indexName,
      typeName = Some(_typeName),
      searchParams = _searchParams,
      ElasticsearchSourceSettings()
    )
      .map { message: ReadResult[spray.json.JsObject] =>
        (message.id, message.source.convertTo[T])
      }
  }.withAttributes(ActorAttributes.supervisionStrategy(sourceDecider))

}
