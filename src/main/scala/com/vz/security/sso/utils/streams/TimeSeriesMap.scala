package com.vz.security.sso.utils.streams

import scala.collection.immutable.TreeMap

class TimeSeriesMap[T] extends Serializable {
  private var treeMap = new TreeMap[Long, T]

  def add(ts: Long, v: T): Unit = {
    treeMap += (ts -> v)
  }

  def delete(ts: Long): Unit = {
    treeMap -= ts
  }

  def ceiling(ts: Long): Option[T] = {
    val from = treeMap.from(ts)
    if (from.isEmpty) None
    else Some(treeMap(from.firstKey))
  }

  def getRange(x: Long, y: Long): Vector[(Long, T)] = {
    treeMap.range(x, y).toVector
  }

  def getAll: TreeMap[Long, T] = {
    treeMap
  }

  def floor(ts: Long): Option[T] = {
    val to = treeMap.to(ts)
    if (to.isEmpty) None
    else Some(treeMap(to.lastKey))
  }

  def get(x: Long): Option[T] = {
    treeMap.get(x)
  }

  def getNumberOfElements: Int = {
    treeMap.size
  }

  def removeExpiredElements(t: Long): Boolean = {
    treeMap = treeMap.range(t, System.currentTimeMillis() + 20000l)
    true
  }

  override def toString: String = {
    treeMap.mkString("{", ",", "}")
  }
}
