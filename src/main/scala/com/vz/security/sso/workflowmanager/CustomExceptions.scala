package com.vz.security.sso.workflowmanager

import com.vz.security.sso.common.exceptions.CustomExceptions._

object CustomExceptions {

  case class ExpressionParserException(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends GenericException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }

  case class IDGenerationException(customMessage: String = "", override val message: String = "", override val cause: Throwable = None.orNull)
    extends HarmlessException {

    override def getMessage: String = customMessage + concatStr + message

    override def getCause: Throwable = cause
  }

}
