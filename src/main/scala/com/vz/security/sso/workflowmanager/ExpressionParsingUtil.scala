package com.vz.security.sso.workflowmanager

//import java.util.regex.Pattern//todo: regex expression

import com.vz.security.sso.common.config.ConfigLoaderCore.{exprRegex, opRegex}
import com.vz.security.sso.workflowmanager.CustomExceptions.ExpressionParserException
//import com.vz.security.sso.workflowmanager.ExpressionParsingUtil.{delimiter, getName, parseOperation}//todo: regex expression

import scala.reflect.runtime.{currentMirror, _}
import scala.tools.reflect.ToolBox
import scala.util.Try
import scala.util.matching.Regex

object ExpressionParsingUtil {

  val getName = this.getClass.getName.split("\\$").last
  val delimiter = "~"
  val cm = universe.runtimeMirror(getClass.getClassLoader)
  val toolBox = cm.mkToolBox()

  def count(fn: Map[String, Any] => Boolean)(implicit grp: Vector[Map[String, Any]]): Int = {
    grp.count(m => fn(m))
  }

  def distinct(key: String)(implicit grp: Vector[Map[String, Any]]): Int = {
    grp.flatMap(m => m.get(key)).distinct.size
  }

  def validate(fn: Map[String, Any] => Boolean)(implicit map: Map[String, Any]): Boolean = {
    fn(map)
  }

  /**
   * Regex pattern to parse operations. For example
   * GTPattern keyA:>:20
   * LTPattern keyA:<:10
   * EQPattern keyA:=:Tom
   */
  //  val regPatternTemp:String = """[0-9a-zA-Z-#()\[\]_/-/.! ]+""".stripMargin //todo: regex expression
  val GTPattern: Regex =
  s"""($opRegex):([>]):($opRegex)""".r
  val LTPattern: Regex = s"""($opRegex):([<]):($opRegex)""".r
  val EQPattern: Regex = s"""($opRegex):([=]):($opRegex)""".r
  val NotEQPattern: Regex = s"""($opRegex):(!=):($opRegex)""".r
  val ContainsPattern: Regex = s"""($opRegex):([contains]+):($opRegex)""".r
  //  val MatchPattern = Pattern.compile(s"""($regPattern):([match]+):($regPatternTemp)""".stripMargin).pattern().r //todo: regex expression
  val NonEmptyPattern: Regex =
    s"""($opRegex):(isNonEmpty)""".r
  val EmptyPattern: Regex = s"""($opRegex):(isEmpty)""".r
  val GEPattern: Regex = s"""($opRegex):(>=):($opRegex)""".r
  val LEPattern: Regex = s"""($opRegex):(<=):($opRegex)""".r

  def parseOperation(exp: String): String = {
    exp match {
      case GTPattern(a, b, c) =>
        MathFn(a, b, c)

      case LTPattern(a, b, c) =>
        MathFn(a, b, c)

      case GEPattern(a, b, c) =>
        MathFn(a, b, c)

      case LEPattern(a, b, c) =>
        MathFn(a, b, c)

      case EQPattern(a, b, c) =>
        EqFn(a, c)

      case NotEQPattern(a, b, c) =>
        NotEqFn(a, c)

      case ContainsPattern(a, b, c) =>
        ContainsFn(a, c)

      //      case a@MatchPattern => //todo: regex expression
      //        MatchFn()

      case NonEmptyPattern(a, b) =>
        IsNonEmptyFn(a)

      case EmptyPattern(a, b) =>
        IsEmptyFn(a)

      case _ =>
        throw ExpressionParserException(s"Unable to parse expression $exp")
    }
  }

  def isLong(aString: String): Boolean = Try(aString.toLong).isSuccess

  def isDouble(aString: String): Boolean = Try(aString.toDouble).isSuccess

  /**
   * Generates function which can run operation on given input Map[String, Any]
   * and gives out Boolean output
   *
   * @param key   Key to fetch from input map
   * @param op    Operation to perform on input map
   * @param value Value to be compared to value of key in map
   * @return True/False
   */
  def MathFn(key: String, op: String, _value: String) = {
    (isLong(_value), isDouble(_value)) match {
      case (true, _) =>
        val value: Long = _value.toLong
        s"""val fn: Map[String, Any] => Boolean = map => {
           |    map.getOrElse("$key", "").toString match {
           |      case ln if $getName.isLong(ln) => ln.toLong $op ${value}l
           |      case _ => false
           |    }
           |  }
           |  fn""".stripMargin

      case (_, true) =>
        val value: Double = _value.toDouble
        s"""val fn: Map[String, Any] => Boolean = map => {
           |    map.getOrElse("$key", "").toString match {
           |      case db if $getName.isDouble(db) => db.toDouble $op ${value}d
           |      case _ => false
           |    }
           |  }
           |  fn""".stripMargin

      case _ =>
        throw ExpressionParserException(s"Value is neither Long nor Double")
    }
  }

  /**
   * Generates function which equates value in given input Map[String, Any] to passed value
   * and gives out Boolean output
   *
   * @param key   Key to fetch from input map
   * @param value Value to be compared to value of key in map
   * @return True/False
   */
  def EqFn(key: String, value: String) = {
    s"""val fn: Map[String, Any] => Boolean = map => {
       |      (map.get("$key"), "$value") match {
       |        case (Some(k), v) => k.toString.equalsIgnoreCase(v.toString)
       |        case _ => false
       |      }
       |    }
       |    fn""".stripMargin
  }

  /**
   * Generates function which checks equality in given input Map[String, Any] to passed value
   * and gives out Boolean output
   *
   * @param key   Key to fetch from input map
   * @param value Value to be compared to value of key in map
   * @return True/False
   */
  def NotEqFn(key: String, value: String) = {
    s"""val fn: Map[String, Any] => Boolean = map => {
       |      (map.get("$key"), "$value") match {
       |        case (Some(k), v) => !k.toString.equalsIgnoreCase(v.toString)
       |        case _ => false
       |      }
       |    }
       |    fn""".stripMargin
  }

  /**
   * Generates function which contains value in given input Map[String, Any] to passed value
   * and gives out Boolean output
   *
   * @param key   Key to fetch from input map
   * @param value Value to be compared to value of key in map
   * @return True/False
   */
  def ContainsFn(key: String, value: String) = {
    val lowerCaseValue = value.toLowerCase
    s"""val fn: Map[String, Any] => Boolean = map => {
       |      (map.get("$key"), "$lowerCaseValue") match {
       |        case (Some(k), v) => k.toString.toLowerCase.contains(v.toString)
       |        case _ => false
       |      }
       |    }
       |    fn""".stripMargin
  }


  /**
   * Generates function which checks if value in given input Map[String, Any] is empty
   * and gives out Boolean output
   *
   * @param key   Key to fetch from input map
   * @param value Value to be compared to value of key in map
   * @return True/False
   */
  def IsEmptyFn(key: String) = {
    s"""val fn: Map[String, Any] => Boolean = map => {
       |      (map.get("$key")) match {
       |        case (Some(k)) => k.toString.trim.isEmpty
       |        case _ => true
       |      }
       |    }
       |    fn""".stripMargin
  }

  /**
   * Generates function which checks if value in given input Map[String, Any] is non-empty
   * and gives out Boolean output
   *
   * @param key   Key to fetch from input map
   * @param value Value to be compared to value of key in map
   * @return True/False
   */
  def IsNonEmptyFn(key: String) = {
    s"""val fn: Map[String, Any] => Boolean = map => {
       |      (map.get("$key")) match {
       |        case (Some(k)) => k.toString.trim.nonEmpty
       |        case _ => false
       |      }
       |    }
       |    fn""".stripMargin
  }

  def isMatch(in: String)(implicit reg: Regex): Boolean = {
    reg.pattern.matcher(in.toString).matches
  }

  /**
   * Generates function which contains value in given input Map[String, Any] to passed value
   * and gives out Boolean output
   *
   * @param key   Key to fetch from input map
   * @param value Value to be compared to value of key in map
   * @return True/False
   */
  def MatchFn(key: String, regex: String) = {
    val reg: Regex = s"""$regex""".r
    s"""val fn: Map[String, Any] => Boolean = map => {
       |      (map.get("$key")) match {
       |        case (Some(k)) => $getName.isMatch(k.toString)($reg)
       |        case _ => false
       |      }
       |    }
       |    fn""".stripMargin
  }

  /**
   * Regex pattern to parse functions. For example
   * CountPattern count(keyA:>:20)
   * DistinctPattern distinct(keyA)
   */
  val CountPattern: Regex =
    s"""count$exprRegex""".r

  val DistinctPattern: Regex =
    s"""distinct$exprRegex""".r

  val PlainPattern: Regex =
    exprRegex.r
  //  Pattern.compile("""\([0-9a-zA-Z:><=.\[\]_/!-/ ]+\)""".stripMargin).pattern().r //todo: regex expression

  /**
   * Break expression on delimiter. Replace each element with runnable code
   * If it matches CountPattern or DistinctPattern then replace with code
   * else replace same
   */
  def parseExpression(expression: String) = {
    val expVector = expression.split(delimiter).toVector
    expVector.foldLeft("")((parsedExpression, pattern) => {
      pattern match {
        case CountPattern(a) =>
          parsedExpression + s"""({$getName.count({${parseOperation(a)}})})"""

        case DistinctPattern(a) =>
          parsedExpression + s"""({$getName.distinct("$a")})"""

        //todo: regex expression - a@PlainPattern
        case PlainPattern(a) =>
          parsedExpression + s"""({$getName.validate({${parseOperation(a)}})})"""

        case d@_ =>
          parsedExpression + d
      }
    })
  }

  /**
   * Convert user given String expression to runnable code which takes
   * Input: Vector[Map[String, Any]
   * Output: Boolean
   * Example: """count(ipAddress:<:10)~/~distinct(ipAddress)~>1"""
   */
  def compileHavingExpression(expression: String): Vector[Map[String, Any]] => Boolean = {
    val tb = currentMirror.mkToolBox()
    val exp = parseExpression(expression)
    val tree = tb.parse(
      s"""
         |def wrapper(grp: Vector[Map[String, Any]]): Boolean = {
         |  implicit val lis = grp
         |  $exp
         |}
         |wrapper _
        """.stripMargin)
    val f = tb.compile(tree)
    val wrapper = f()
    wrapper.asInstanceOf[Vector[Map[String, Any]] => Boolean]
  }

  /**
   * Convert user given String expression to runnable code which takes
   * Input: Map[String, Any]
   * Output: Boolean
   * Example: """(ipAddress:<:10)~&&~(userName:=:Tom)"""
   */
  def compileWhereExpression(expression: String): Map[String, Any] => Boolean = {
    val tb = currentMirror.mkToolBox()
    val exp = parseExpression(expression)
    val tree = tb.parse(
      s"""
         |def wrapper(map: Map[String, Any]): Boolean = {
         |  implicit val m = map
         |  $exp
         |}
         |wrapper _
        """.stripMargin)
    val f = tb.compile(tree)
    val wrapper = f()
    wrapper.asInstanceOf[Map[String, Any] => Boolean]
  }

  /**
   * Convert user given String expression to runnable code which takes
   * Input: Map[String, String]
   * Output: String
   * Example: """val first = map.getOrElse("fn", "").toString; val last = map.getOrElse("ln", "").toString; first + "_" + last"""
   */
  def compileStringExpression(_expression: String): Map[String, String] => String = {
    val expression = StringContext treatEscapes _expression //To remove escape characters
    val tb = currentMirror.mkToolBox()
    val tree = tb.parse(
      s"""
         |def wrapper(map: Map[String, String]): String = {
         |  implicit val m = map
         |  $expression
         |}
         |wrapper _
        """.stripMargin)
    val f = tb.compile(tree)
    val wrapper = f()
    wrapper.asInstanceOf[Map[String, String] => String]
  }
}
