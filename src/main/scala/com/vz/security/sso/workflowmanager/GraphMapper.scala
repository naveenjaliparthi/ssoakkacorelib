package com.vz.security.sso.workflowmanager

import scalax.collection.Graph
import scalax.collection.edge.LDiEdge
import scalax.collection.io.json.descriptor.predefined.LDi
import scalax.collection.io.json.{Descriptor, JsonGraphCoreCompanion, NodeDescriptor, _}

object GraphMapper {

  val NodeDesc: NodeDescriptor[ServerNode] = new NodeDescriptor[ServerNode](typeId = "Nodes") {
    override def id(node: Any): String = node match {
      case ServerNode(id) => id
    }
  }

  val descriptor = new Descriptor[ServerNode](
    defaultNodeDescriptor = NodeDesc,
    defaultEdgeDescriptor = LDi.descriptor[ServerNode, String](null),
    namedNodeDescriptors = Seq(NodeDesc),
    namedEdgeDescriptors = Seq(LDi.descriptor[ServerNode, String](null)))

  val graphCompanion = new JsonGraphCoreCompanion[Graph](Graph)

  def toJson(graph: Graph[ServerNode, LDiEdge]): String = {
    graph.toJson(descriptor)
  }

  def fromJson(json: String): Graph[ServerNode, LDiEdge] = {
    graphCompanion.fromJson[ServerNode, LDiEdge](json, descriptor)
  }

}