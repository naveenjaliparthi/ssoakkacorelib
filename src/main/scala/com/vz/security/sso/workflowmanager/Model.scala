package com.vz.security.sso.workflowmanager

import com.vz.security.sso.common.config.ConfigLoaderCore
import com.vz.security.sso.common.constants.SharedConstants
import com.vz.security.sso.logging.VZMapLogger
import com.vz.security.sso.utils.ObjectMapperUtil
import spray.json.DefaultJsonProtocol._


/**
  * Maintaining two sets of models to avoid tight coupling between client and server
  */
case class ServerNode(id: String)

case class ServerEdge(n1: String, n2: String, label: String)

case class ServerGraph(nodes: List[ServerNode], edges: List[ServerEdge])

object ServerGraph {
  def apply(clientGraph: ClientGraph): ServerGraph = {
    val nodes = clientGraph.nodes.map(n => ServerNode(n.nodeId))
    val edges = clientGraph.connections.map(e => ServerEdge(e.pageSourceId, e.pageTargetId, e.label))
    ServerGraph(nodes, edges)
  }
}

case class LabelFormula(formula: String, label: String) {
  require(ClientWorkflowValidator.validateString(formula), "formula is invalid in LabelFormula")
  require(ClientWorkflowValidator.validateString(label), "label is invalid in LabelFormula")
}

case class ClientNode(nodeId: String, name: String, nType: String, actions: List[LabelFormula], metadata: Option[Map[String, String]] = None) {
  require(ClientWorkflowValidator.validateString(nodeId), "nodeId is invalid in Node")
  require(ClientWorkflowValidator.validateString(name), "name is invalid in Node")
  require(ClientWorkflowValidator.validateMetadata(nType, metadata), s"Metadata is invalid for nType $nType")
}

case class ClientEdge(connectionId: String, pageSourceId: String, pageTargetId: String, label: String) {
  require(ClientWorkflowValidator.validateString(pageSourceId), "pageSourceId is invalid in Edge")
  require(ClientWorkflowValidator.validateString(pageTargetId), "pageTargetId is invalid in Edge")
  require(ClientWorkflowValidator.validateString(label), "label is invalid in Edge")
}

case class ClientGraph(nodes: List[ClientNode], connections: List[ClientEdge]) {
  require(nodes != null && nodes.nonEmpty, "nodes are empty in Node")
  require(connections != null && connections.nonEmpty, "connections are empty in Node")
}

case class ClientWFPaylaod(id: String, name: String, module: String, datetime: String, startNodeId: String, flowchart: ClientGraph) {
  require(ClientWorkflowValidator.validateString(id), "id is invalid in ClientWorkflow")
  require(ClientWorkflowValidator.validateString(name), "name is invalid in ClientWorkflow")
  require(ClientWorkflowValidator.validateString(module), "module is invalid in ClientWorkflow")
  require(ClientWorkflowValidator.validateString(datetime), "dateTime is invalid in ClientWorkflow")
  require(ClientWorkflowValidator.validateString(startNodeId), "startNodeId is invalid in ClientWorkflow")
  require(flowchart != null, "flowGraph is null in ClientWorkflow")
}

case class ClientWF(op: String, payload: ClientWFPaylaod) {
  require(ClientWorkflowValidator.validateString(op), "op is invalid in ClientWF")
  require(payload != null, "payload is null in ClientWF")
}

/**
  * Alpakka elastic uses spray json to marshall/unmarshall
  * Defining implicit formatter's for only elastic models
  */
object GraphsSprayFormatter {
  implicit val LabelFormulaFormat = jsonFormat2(LabelFormula)
  implicit val ClientNodeFormat = jsonFormat5(ClientNode)
  implicit val ClientEdgeFormat = jsonFormat4(ClientEdge)
  implicit val ClientGraphFormat = jsonFormat2(ClientGraph)
  implicit val ClientWFPaylaodFormat = jsonFormat6(ClientWFPaylaod)
}

object ClientWorkflowValidator {

  val logger = new VZMapLogger(this)

  /**
    * Edges n1 -> n2 should be unique
    * Labels should be unique
    */
  def isValid(clientWorkflow: ClientWFPaylaod): Boolean = {
    (clientWorkflow.flowchart.connections.size == clientWorkflow.flowchart.connections.map(e => (e.pageSourceId, e.pageTargetId)).distinct.size) &&
      (clientWorkflow.flowchart.connections.size == clientWorkflow.flowchart.connections.map(_.label).distinct.size)
  }

  def validateMetadata(nType: String, metadata: Option[Map[String, String]]): Boolean = {
    if (nType.equalsIgnoreCase(ConfigLoaderCore.KAFKA)) metadata.isDefined && metadata.get.getOrElse(ConfigLoaderCore.kafkaTopicsKey, "").nonEmpty
    else if (nType.equalsIgnoreCase(ConfigLoaderCore.PERSISTENCESHARD)) {
      metadata.isDefined &&
        metadata.get.getOrElse(SharedConstants.SHARDKEY, "").nonEmpty
    }
    else if (nType.equalsIgnoreCase(ConfigLoaderCore.RTSTREAMSHARD)) {
      metadata.isDefined &&
        metadata.get.getOrElse(ConfigLoaderCore.rtQIdKey, "").nonEmpty &&
        metadata.get.getOrElse(ConfigLoaderCore.rtShardKey, "").nonEmpty &&
        metadata.get.getOrElse(ConfigLoaderCore.rtParKey, "").nonEmpty
    }
    else true
  }

  def validateString(v: String): Boolean = {
    Option(v).map(!_.isEmpty) == Option(true)
  }

  def validateWFG(wf: String): Boolean = {
    try {
      val wfm = ObjectMapperUtil.fromJson[ClientWF](wf)
      if (wfm != null) true else false
    }
    catch {
      case ex: Exception =>
        logger.error("validateWFG", null, ex, "wf", wf)
        false
    }
  }

}

case class LabelFunction(function: Map[String, Any] => Boolean, label: String)

case class EnrichFunction(function: Map[String, String] => String, label: String)