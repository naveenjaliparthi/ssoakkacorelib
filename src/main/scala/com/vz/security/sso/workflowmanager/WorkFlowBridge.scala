package com.vz.security.sso.workflowmanager

import akka.actor.{Actor, ActorRef}
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import com.vz.security.sso.logging.VZMapLogger
import com.vz.security.sso.models.GenericMapRequest

import scala.collection.mutable

object WorkFlowBridge {
  val config = ConfigFactory.load()
  val metadataKeys = config.getString("work-flow.metadataKeys").split(",").map(_.trim).toSet
}

import WorkFlowBridge._

class WorkFlowBridge(wfm: WorkFlowManager) extends Actor {

  val logger = new VZMapLogger(this)

  val actorName: String = self.path.name

  logger.info("WorkFlowBridgeAudit", actorName, "metadataKeys", metadataKeys.mkString(", "))

  implicit val system = context.system

  implicit val mat = ActorMaterializer()(context) //Mat created within context because if actor dies then all processes should stop

  override def receive: Receive = {
    case request: GenericMapRequest ⇒
      val requestStartTime: Long = System.nanoTime
      val origSender: ActorRef = sender()
      val logMap = mutable.Map[String, Any]()
      logMap += ("request" -> request.fields)
      logMap += ("fromWFMModule" -> request.fields.getOrElse("module", ""))
      val toWFMModule = request.fields.getOrElse("toWFMModule", "")
      logMap += ("toWFMModule" -> toWFMModule)
      if (toWFMModule.nonEmpty) {
        //Remove previous wfm metadata and add `to module`
        val wfmMap = request.fields.filterNot(kv => metadataKeys.contains(kv._1)) + ("module" -> toWFMModule)
        val wfmResponse = wfm.callWorkFlowConnector(toWFMModule, wfmMap, origSender, None, None)
        wfmResponse match {
          case Left(value) =>
            sender ! GenericMapRequest(value)
            logMap += ("response" -> value)
            logMap += ("action" -> "toWFMModule not found")
            logMap += ("toNodes" -> "Play")
          case Right(value) =>
            logMap += ("action" -> "Forward")
            logMap += ("toNodes" -> value)
        }
      } else {
        sender ! GenericMapRequest(request.fields)
        logMap += ("response" -> request.fields)
        logMap += ("action" -> "toWFMModule is empty")
        logMap += ("toNodes" -> "Play")
      }
      logToSplunk(requestStartTime, logMap)

    case message@_ =>
      val logMap = mutable.Map[String, Any]()
      logMap += ("message" -> "Unhandled message in WorkFlowBridge Actor")
      logMap += ("request" -> message)
      logger.info("WorkFlowBridgeUnhandledRequest", actorName, logMap)
  }

  def logToSplunk(requestStartTime: Long, logMap: mutable.Map[String, Any]): Unit = {
    val requestEndTime: Long = System.nanoTime
    val elapsedTime: Int = if (requestEndTime > requestStartTime) ((requestEndTime - requestStartTime) / 1000000).toInt else 0
    logMap += ("responseTime" -> elapsedTime)
    logger.info("WorkFlowBridgeAudit", "NoSessionId", logMap)
  }

}
