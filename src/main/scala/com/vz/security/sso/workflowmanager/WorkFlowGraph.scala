package com.vz.security.sso.workflowmanager

import akka.actor.{ActorRef, ActorSystem}
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.vz.security.sso.common.config.ConfigLoaderCore._
import com.vz.security.sso.common.constants.SharedConstants
import com.vz.security.sso.common.constants.SharedConstants._
import com.vz.security.sso.logging.VZMapLogger
import com.vz.security.sso.models.GenericMapRequest
import com.vz.security.sso.utils.{ObjectMapperUtil, ShardUtil}
import scalax.collection.Graph
import scalax.collection.edge.LDiEdge

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class WorkFlowGraph(clientWorkflow: ClientWFPaylaod, gatePassMap: Map[String, ActorRef])
                   (implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext) {

  implicit val timeout: Timeout = wfAskTimeout.millisecond

  val logger = new VZMapLogger(this)

  val cwf: ClientWFPaylaod = clientWorkflow //To access wf for logging
  val createdOn: Long = System.currentTimeMillis() //For logging

  val actionMap: Map[String, List[LabelFunction]] = WorkFlowGraph.computeActionMap(clientWorkflow) // 1.

  val enrichMap: Map[String, List[EnrichFunction]] = WorkFlowGraph.computeEnrichMap(clientWorkflow) //2.

  val graph: Graph[ServerNode, LDiEdge] = WorkFlowGraph.buildGraph(clientWorkflow)

  val nodeIdMapping: Map[String, ClientNode] = clientWorkflow.flowchart.nodes.foldLeft(Map[String, ClientNode]())((acc, node) => {
    acc + (node.nodeId -> node)
  })

  def getGatePass(nodeid: String): Option[ActorRef] = {
    if (nodeIdMapping.get(nodeid).isDefined) {
      gatePassMap.get(nodeIdMapping(nodeid).nType)
    }
    else None
  }

  def generatePayload(requestMap: Map[String, String], nodeid: String): Either[List[(String, String)], GenericMapRequest] = {
    val cNode = nodeIdMapping(nodeid)
    if (cNode.nType == KAFKA) {
      val enrich = addOnAttributes(requestMap, cNode)
      //Safeguarding returns Score in INT format
      val payload = ObjectMapperUtil.toJson(WorkFlowGraph.scoreToIntMap(enrich))
      Left(cNode.metadata.get(kafkaTopicsKey).split(",").map(topic => (topic.trim, payload)).toList)
    }
    else if (cNode.nType == PERSISTENCESHARD || cNode.nType == AAPERSISTENCESHARD) {
      val enrich = requestMap +
        (SharedConstants.WFNODEID -> nodeid) + (SharedConstants.WFID -> clientWorkflow.id) + (SharedConstants.NTYPE -> cNode.nType) +
        (SharedConstants.SHARDKEY -> cNode.metadata.get(SharedConstants.SHARDKEY)) +
        (SharedConstants.SHARDVALUE -> requestMap(cNode.metadata.get(SharedConstants.SHARDKEY)))
      Right(GenericMapRequest(addOnAttributes(enrich, cNode)))
    }
    else if (cNode.nType == RTSTREAMSHARD || cNode.nType == AARTSTREAMSHARD) {
      val enrich = requestMap +
        (SharedConstants.WFNODEID -> nodeid) + (SharedConstants.WFID -> clientWorkflow.id) + (SharedConstants.NTYPE -> cNode.nType) +
        (SharedConstants.ACTORID -> ShardUtil.getRTActorId(cNode.metadata.get(rtQIdKey), cNode.metadata.get(rtShardKey), cNode.metadata.get(rtParKey).toInt, requestMap))
      Right(GenericMapRequest(addOnAttributes(enrich, cNode)))
    }
    else {
      val enrich = requestMap +
        (SharedConstants.WFNODEID -> nodeid) + (SharedConstants.WFID -> clientWorkflow.id) + (SharedConstants.NTYPE -> cNode.nType)
      Right(GenericMapRequest(addOnAttributes(enrich, cNode)))
    }
  }

  private def enrichPayload(requestMap: Map[String, String], nodeid: String) = {
    val enrichFunctionList = enrichMap.getOrElse(nodeid, List[EnrichFunction]())
    val enrichKV = enrichFunctionList.foldLeft(requestMap)((acc, ef) => {
      val key = if (ef.label.contains(":")) ef.label.split(":")(0) else ef.label
      acc + (key -> ef.function(acc))
    })
    enrichKV
  }

  private def addOnAttributes(requestMap: Map[String, String], clientNode: ClientNode) = {
    if (clientNode.metadata.isEmpty) requestMap
    else {
      val copyKeysMap = {
        if (clientNode.metadata.get.get(COPYKEYS).isDefined) {
          val keys = clientNode.metadata.get(COPYKEYS).split(KEYSDELIMITER).map(_.trim).toSet
          WorkFlowGraph.copyKeys(keys, requestMap)
        }
        else {
          Map[String, String]()
        }
      }
      val enrichKeysMap = {
        if (clientNode.metadata.get.get(ENRICHKEYS).isDefined) {
          val keys = clientNode.metadata.get(ENRICHKEYS).split(KEYSDELIMITER).map(_.trim).toSet
          WorkFlowGraph.enrichKeys(keys, requestMap)
        }
        else {
          Map[String, String]()
        }
      }
      val completeMap = requestMap ++ copyKeysMap ++ enrichKeysMap
      val filterKeysMap = {
        if (clientNode.metadata.get.get(FILTERKEYS).isDefined) {
          val keys = clientNode.metadata.get(FILTERKEYS).split(KEYSDELIMITER).map(_.trim).toSet
          WorkFlowGraph.filterKeys(keys, completeMap) //Passing completeMap
        }
        else {
          completeMap
        }
      }
      filterKeysMap
    }
  }


  /**
   * @return Returns future of response by initiating chain
   */
  def callWorkFlow(requestMap: Map[String, String]): Future[Any] = {
    {
      val callerNodeId = clientWorkflow.startNodeId
      val enrichedMap = enrichPayload(requestMap, callerNodeId)
      val nextNodes = findNextSteps(enrichedMap, callerNodeId)
      if (nextNodes.size > 1) {
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("callerNodeId" -> callerNodeId)
        logmap += ("request" -> enrichedMap)
        logmap += ("nextStepsList" -> nextNodes)
        logmap += ("status" -> "first node should at max have only one element in nextStepsList")
        logger.error("callWorkFlowWithoutRef", "NoSessionId", logmap)
      }
      if (nextNodes.nonEmpty && getGatePass(nextNodes.head).isDefined) {
        val kafkaOrActor = generatePayload(enrichedMap, nextNodes.head)
        kafkaOrActor match {
          case Left(value) =>
            val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
            logmap += ("callerNodeId" -> callerNodeId)
            logmap += ("request" -> enrichedMap)
            logmap += ("nextStepsList" -> nextNodes)
            logmap += ("status" -> "Ask doesn't work on kafka")
            logger.debug("callWorkFlowWithoutRef", "NoSessionId", logmap)
            Future {
              GenericMapRequest(enrichedMap)
            }
          case Right(value) => getGatePass(nextNodes.head).get ? value
        }
      }
      else {
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("callerNodeId" -> callerNodeId)
        logmap += ("request" -> enrichedMap)
        logmap += ("nextStepsList" -> nextNodes)
        logmap += ("status" -> "Not calling next actor")
        logger.debug("callWorkFlowWithoutRef", "NoSessionId", logmap)
        Future {
          GenericMapRequest(enrichedMap)
        }
      }
    } recover {
      case ex: Exception =>
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("error" -> "Exception while calling next node")
        logmap += ("request" -> requestMap)
        logmap += ("action" -> "Unable to process message")
        logmap += ("callerNodeId" -> clientWorkflow.startNodeId)
        logmap += ("module" -> clientWorkflow.module)
        logmap += ("wfid" -> clientWorkflow.id)
        logmap += ("errorName" -> ex.getMessage)
        logmap += ("stackTrace" -> ex.getStackTrace.mkString("\r\n\t"))
        logger.warn("callWorkFlowWithoutRef", "sessionNotDefined", logmap)
        GenericMapRequest(requestMap)
    }
  }

  /**
   * @return string message to represent action
   */
  def callWorkFlow(requestMap: Map[String, String], callerNodeId: String, originalSender: ActorRef): List[String] = {
    try {
      val enrichedMap = enrichPayload(requestMap, callerNodeId)
      val actions = new ListBuffer[String]()
      val cNode = nodeIdMapping(callerNodeId)
      var updatedSender = originalSender
      //Enforce reply to sender
      if (cNode.metadata.isDefined && cNode.metadata.get.getOrElse("reply", "false").toBoolean) {
        updatedSender ! GenericMapRequest(enrichedMap)
        updatedSender = ActorRef.noSender
        actions += "Enforce reply to sender"
      }
      val nextNodes = findNextSteps(enrichedMap, callerNodeId)
      nextNodes.foreach(nextNodeId => {
        if (getGatePass(nextNodeId).isDefined) {
          val kafkaOrActor = generatePayload(enrichedMap, nextNodeId)
          kafkaOrActor match {
            case Left(value) => value.foreach(v => getGatePass(nextNodeId).get ! v)
            case Right(value) => getGatePass(nextNodeId).get.tell(value, updatedSender)
          }
          actions += s"Forwarded to ${
            nodeIdMapping(nextNodeId).nType
          }:$nextNodeId"
        }
      })
      if (actions.isEmpty) {
        updatedSender ! GenericMapRequest(enrichedMap)
        actions += "Replied"
      }
      val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
      logmap += ("callerNodeId" -> callerNodeId)
      logmap += ("request" -> enrichedMap)
      logmap += ("actions" -> actions.toList)
      logger.debug("callWorkFlowWithRef", "NoSessionId", logmap)
      actions.toList
    }
    catch {
      case ex: Exception =>
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("callerNodeId" -> callerNodeId)
        logmap += ("request" -> requestMap)
        logmap += ("status" -> "message dropped")
        logmap += ("errorName" -> ex.getMessage)
        logmap += ("stackTrace" -> ex.getStackTrace.mkString("\r\n\t"))
        logmap += ("action" -> "Replied")
        originalSender ! GenericMapRequest(requestMap)
        logger.warn("callWorkFlowWithRef", "NoSessionId", logmap)
        List("Error in callWorkFlowWithRef - Replied")
    }
  }

  /**
   * @return Returns future of response by initiating chain
   */
  def callWorkFlowConnector(requestMap: Map[String, String], originalSender: ActorRef): String = {
    try {
      val callerNodeId = clientWorkflow.startNodeId
      val enrichedMap = enrichPayload(requestMap, callerNodeId)
      val nextNodes = findNextSteps(enrichedMap, callerNodeId)
      if (nextNodes.size > 1) {
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("callerNodeId" -> callerNodeId)
        logmap += ("request" -> enrichedMap)
        logmap += ("nextStepsList" -> nextNodes)
        logmap += ("status" -> "first node should at max have only one element in nextStepsList")
        logger.error("callWorkFlowConnector", "NoSessionId", logmap)
      }
      if (nextNodes.nonEmpty && getGatePass(nextNodes.head).isDefined) {
        val kafkaOrActor = generatePayload(enrichedMap, nextNodes.head)
        kafkaOrActor match {
          case Left(value) =>
            val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
            logmap += ("callerNodeId" -> callerNodeId)
            logmap += ("request" -> enrichedMap)
            logmap += ("nextStepsList" -> nextNodes)
            logmap += ("status" -> "Connector doesn't work on kafka")
            logger.debug("callWorkFlowConnector", "NoSessionId", logmap)
            "Connector doesn't work on kafka"
          case Right(value) =>
            getGatePass(nextNodes.head).get.tell(value, originalSender)
            s"Forwarded to ${
              nodeIdMapping(nextNodes.head).nType
            }:${nextNodes.head}"
        }
      }
      else {
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("callerNodeId" -> callerNodeId)
        logmap += ("request" -> enrichedMap)
        logmap += ("nextStepsList" -> nextNodes)
        logmap += ("status" -> "Not calling next actor")
        logger.debug("callWorkFlowWithoutRef", "NoSessionId", logmap)
        originalSender ! GenericMapRequest(enrichedMap)
        "Replied"
      }
    }
    catch {
      case ex: Exception =>
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("error" -> "Exception while calling next node")
        logmap += ("request" -> requestMap)
        logmap += ("action" -> "Unable to process message")
        logmap += ("callerNodeId" -> clientWorkflow.startNodeId)
        logmap += ("module" -> clientWorkflow.module)
        logmap += ("wfid" -> clientWorkflow.id)
        logmap += ("errorName" -> ex.getMessage)
        logmap += ("stackTrace" -> ex.getStackTrace.mkString("\r\n\t"))
        logger.warn("callWorkFlowWithoutRef", "sessionNotDefined", logmap)
        GenericMapRequest(requestMap)
        "Replied due to exception"
    }
  }


  def findNextSteps(requestMap: Map[String, String], senderName: String): List[String] = {
    val nodeLabelFunctions = actionMap.getOrElse(senderName, List[LabelFunction]())
    val labelList = WorkFlowGraph.runLabelFunctions(requestMap, nodeLabelFunctions)
    labelList.map(lb => {
      val nextNode = getNodeByLabel(lb)
      if (nextNode.isDefined) Some(nextNode.get.id) else None
    })
      .filter(_.isDefined)
      .map(_.get)
  }

  def getNodeByLabel(labelName: String): Option[ServerNode] = {
    try {
      val label = graph.edges.find(_.label == labelName)
      Some(label.get.tail.head.value)
    }
    catch {
      case ex: Exception =>
        None
    }
  }

}

object WorkFlowGraph {

  val logger = new VZMapLogger(this)

  def apply(clientWorkflow: ClientWFPaylaod, gatePassMap: Map[String, ActorRef])
           (implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext): WorkFlowGraph = new WorkFlowGraph(clientWorkflow, gatePassMap)

  def computeActionMap(clientWorkflow: ClientWFPaylaod): Map[String, List[LabelFunction]] = {
    try {
      clientWorkflow.flowchart.nodes.foldLeft(Map[String, List[LabelFunction]]())((acc, n) => {
        val lfList: List[LabelFunction] = n.actions.foldLeft(List[LabelFunction]())((acc, lf) => {
          //todo: move to wfm config
          //1. Backward compatability if label doesn't contain ":"
          //2. Process only labels with output boolean
          if (!lf.label.toLowerCase.contains(":") || lf.label.toLowerCase.contains(":boolean")) {
            LabelFunction(ExpressionParsingUtil.compileWhereExpression(lf.formula), lf.label) :: acc
          } else {
            acc
          }
        })
        acc + (n.nodeId -> lfList)
      })
    }
    catch {
      case ex: Throwable => //toolbox throws Throwable
        var loggerMap = mutable.Map[String, Any]()
        loggerMap += ("clientGraph" -> clientWorkflow)
        logger.error("computeActionsMapCompilationError", "NoSessionId", ex, loggerMap)
        Map[String, List[LabelFunction]]()
    }
  }

  def computeEnrichMap(clientWorkflow: ClientWFPaylaod): Map[String, List[EnrichFunction]] = {
    try {
      clientWorkflow.flowchart.nodes.foldLeft(Map[String, List[EnrichFunction]]())((acc, n) => {
        val efList: List[EnrichFunction] = n.actions.foldLeft(List[EnrichFunction]())((acc, lf) => {
          //1. Process only labels with out String
          if (lf.label.toLowerCase.contains(":string")) {
            EnrichFunction(ExpressionParsingUtil.compileStringExpression(lf.formula), lf.label) :: acc
          } else {
            acc
          }
        })
        acc + (n.nodeId -> efList)
      })
    }
    catch {
      case ex: Throwable => //toolbox throws Throwable
        var loggerMap = mutable.Map[String, Any]()
        loggerMap += ("clientGraph" -> clientWorkflow)
        logger.error("enrichMapCompilationError", "NoSessionId", ex, loggerMap)
        Map[String, List[EnrichFunction]]()
    }
  }

  def buildGraph(clientWorkflow: ClientWFPaylaod): Graph[ServerNode, LDiEdge] = {
    val graphJson = ObjectMapperUtil.toJson(ServerGraph(clientWorkflow.flowchart))
    GraphMapper.fromJson(graphJson)
  }

  def getNodeT(graph: Graph[ServerNode, LDiEdge], outer: ServerNode): graph.NodeT = graph get outer

  def runLabelFunctions(requestMap: Map[String, String], lfList: List[LabelFunction]): List[String] = {
    lfList.filter(lf => lf.function(requestMap)).map(_.label)
  }

  /**
   *
   * @param keys Set("a:b","c:d")
   * @param map  Input map
   * @return Divides a:b as key->b, value->Map("a") and adds to input map
   */
  def copyKeys(keys: Set[String], map: Map[String, String]): Map[String, String] = {
    try {
      if (keys.isEmpty) Map[String, String]()
      else {
        keys.foldLeft(Map[String, String]())((acc, key) => {
          val kArray = key.split(VALUEDELIMITER)
          acc + (kArray(1).trim -> map.getOrElse(kArray(0).trim, ""))
        })
      }
    }
    catch {
      case ex: Exception =>
        Map[String, String]()
    }
  }

  /**
   *
   * @param keys Set("a:b","c:d")
   * @param map  Input map
   * @return Divide a:b as key->a, value->b and adds to input map
   */
  def enrichKeys(keys: Set[String], map: Map[String, String]): Map[String, String] = {
    try {
      if (keys.isEmpty) Map[String, String]()
      else {
        keys.foldLeft(Map[String, String]())((acc, key) => {
          val kArray = key.split(VALUEDELIMITER)
          acc + (kArray(0).trim -> kArray(1).trim)
        })
      }
    }
    catch {
      case ex: Exception =>
        Map[String, String]()
    }
  }

  /**
   *
   * @param keys Set("a","c")
   * @param map  Input map
   * @return Filter a, c from map
   */
  def filterKeys(keys: Set[String], map: Map[String, String]): Map[String, String] = {
    try {
      if (keys.isEmpty) map
      else map.filterKeys(k => keys.contains(k))
    }
    catch {
      case ex: Exception =>
        map
    }
  }

  /**
   *
   * @return Convert score to INT if present
   */
  def scoreToIntMap(map: Map[String, String]) = {
    try {
      if (map.get(SCORE).isDefined) map + (SCORE -> map(SCORE).toString.toInt) else map
    }
    catch {
      case ex: Exception =>
        map
    }
  }


}
