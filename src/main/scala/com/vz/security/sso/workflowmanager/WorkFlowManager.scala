package com.vz.security.sso.workflowmanager


import java.util.UUID

import akka.actor.{ActorRef, ActorSystem}
import akka.cluster.routing.{ClusterRouterGroup, ClusterRouterGroupSettings}
import akka.routing.RandomGroup
import akka.stream.scaladsl.{Flow, GraphDSL, Keep, RunnableGraph, Sink}
import akka.stream.{ActorAttributes, ActorMaterializer, ClosedShape}
import akka.{Done, NotUsed}
import com.vz.security.sso.common.config.ConfigLoaderCore
import com.vz.security.sso.common.config.ConfigLoaderCore._
import com.vz.security.sso.common.constants.SharedConstants
import com.vz.security.sso.common.constants.SharedConstants._
import com.vz.security.sso.logging.VZMapLogger
import com.vz.security.sso.utils.ShardUtil
import com.vz.security.sso.utils.streams.SupervisionStrategyUtil.{flowDecider, sinkDecider}
import com.vz.security.sso.utils.streams.{FlowUtil, KafkaPublisherUtil, SourceUtil}
import com.vz.security.sso.workflowmanager.WorkFlowManager._

import scala.collection.mutable
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

class WorkFlowManager(etniActorRef: Option[ActorRef] = None,
                      vcvsActorRef: Option[ActorRef] = None,
                      modelHostRef: Option[ActorRef] = None,
                      initialRiskScoreRef: Option[ActorRef] = None,
                      genericRiskScoreRef: Option[ActorRef] = None,
                      persShardRef: Option[ActorRef] = None,
                      rtShardRef: Option[ActorRef] = None,
                      additionalRef: Map[String, ActorRef] = Map[String, ActorRef](),
                      bootStrapServers: String,
                      kafkaConsumerProps: Map[String, String],
                      kafkaProducerProps: Option[Map[String, String]])
                     (implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext) {
  val logger = new VZMapLogger(this)

  /**
   * Logs all running WF
   */
  system.scheduler.schedule(20.minutes, ConfigLoaderCore.wfLogTimeInMin.minutes) {
    logWF
  }

  //Publisher which takes tuple (topic, value)
  val kafkaPublisher: ActorRef =
    KafkaPublisherUtil.kafkaPublisherStream(bootStrapServers, kafkaProducerProps, Some("WorkFlowProducer"))

  val gatePassMap: mutable.HashMap[String, ActorRef] = {
    val gp = gatePass()
    additionalRef.foreach(kv => gp += kv) //Add additional ref given by client app
    val infoLog = mutable.Map[String, Any]()
    infoLog += ("gatepassList" -> gp.keySet)
    infoLog += ("status" -> "Gatepass initialized")
    logger.info("gatepass", "NoSessionId", infoLog)
    gp
  }

  /**
   * Load from elastic search and start listening to kafka from latest
   */
  def initialize(): Unit = {
    logger.info("WFMManager", null, "status", "Initialization Started")
    rehydrate.foreach(r => {
      logWF //Log all wf after rehydration
      workflowInMemoryStream.run()
    })
  }

  def callWorkFlow(module: String, requestMap: Map[String, String], myScore: Option[String], myReason: Option[String]): Either[Map[String, String], Future[Any]] = {
    val updatedMap = updateRiskScore(requestMap, myScore, myReason)
    workFlowMap.get(module) match {
      case None =>
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("error" -> "module not found")
        logmap += ("request" -> requestMap)
        logmap += ("module" -> module)
        logmap += ("myScore" -> myScore)
        logmap += ("myReason" -> myReason)
        logger.warn("callWorkFlowWithoutRef", "sessionNotDefined", logmap)
        Left(updatedMap)
      case Some(workFlowGraph) => Right(workFlowGraph.callWorkFlow(updatedMap))
    }
  }

  def callWorkFlow(module: String, requestMap: Map[String, String], originalSender: ActorRef, myScore: Option[String], myReason: Option[String]): Either[Map[String, String], List[String]] = {
    val callerNodeId = requestMap.getOrElse(SharedConstants.WFNODEID, "")
    val updatedMap = updateRiskScore(requestMap, myScore, myReason)
    workFlowMap.get(module) match {
      case None =>
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("error" -> "module not found")
        logmap += ("request" -> requestMap)
        logmap += ("module" -> module)
        logmap += ("myScore" -> myScore)
        logmap += ("myReason" -> myReason)
        logger.warn("callWorkFlowWithRef", "sessionNotDefined", logmap)
        Left(updatedMap)
      case Some(workFlowGraph) => Right(workFlowGraph.callWorkFlow(updatedMap, callerNodeId, originalSender))
    }
  }

  def callWorkFlowConnector(module: String, requestMap: Map[String, String], originalSender: ActorRef, myScore: Option[String], myReason: Option[String]): Either[Map[String, String], String] = {
    val updatedMap = updateRiskScore(requestMap, myScore, myReason)
    workFlowMap.get(module) match {
      case None =>
        val logmap: mutable.Map[String, Any] = mutable.Map[String, Any]()
        logmap += ("error" -> "module not found")
        logmap += ("request" -> requestMap)
        logmap += ("module" -> module)
        logmap += ("myScore" -> myScore)
        logmap += ("myReason" -> myReason)
        logger.warn("callWorkFlowConnector", "sessionNotDefined", logmap)
        Left(updatedMap)
      case Some(workFlowGraph) => Right(workFlowGraph.callWorkFlowConnector(updatedMap, originalSender))
    }
  }

  /**
   * Update RISKSCORE and REASONFORDECISION in input map
   */
  def updateRiskScore(payload: Map[String, String], myScore: Option[String] = None, myReason: Option[String] = None): Map[String, String] = {
    val scoreAndRes = (payload.get(RISKSCORE), myScore) match {
      case (Some(prevScore), Some(currentScore)) =>
        if (currentScore.toInt > prevScore.toInt) (currentScore, myReason.getOrElse(NOREASON)) else (prevScore, payload.getOrElse(REASONFORDECISION, NOREASON))
      case (Some(prevScore), None) => (prevScore, payload.getOrElse(REASONFORDECISION, NOREASON))
      case (None, Some(currentScore)) => (currentScore, myReason.getOrElse(NOREASON))
      case (None, None) => (defaultRiskScore, defaultRiskScoreMessage)
    }
    payload + (RISKSCORE -> scoreAndRes._1) + (REASONFORDECISION -> scoreAndRes._2)
  }

  private def gatePass(): mutable.HashMap[String, ActorRef] = {
    val etniActor =
      if (etniActorRef.isDefined) etniActorRef.get
      else {
        system.actorOf(
          ClusterRouterGroup(RandomGroup(Nil),
            ClusterRouterGroupSettings(
              totalInstances = totalNoOfInstancesEtni,
              routeesPaths = List(etniActorPath),
              allowLocalRoutees = localroutesetni,
              useRoles = Set(servicesRole))).props())
      }

    val vcvsActor = if (vcvsActorRef.isDefined) vcvsActorRef.get
    else {
      system.actorOf(
        ClusterRouterGroup(RandomGroup(Nil), ClusterRouterGroupSettings(
          totalInstances = totalNoOfInstancesvcvs, routeesPaths = List(vcvsActorPath),
          allowLocalRoutees = localroutesvcvs, useRoles = Set(servicesRole))).props())
    }

    val modelHostActor = if (modelHostRef.isDefined) modelHostRef.get
    else {
      system.actorOf(
        ClusterRouterGroup(RandomGroup(Nil), ClusterRouterGroupSettings(
          totalInstances = totalNoOfInstancesMl, routeesPaths = List(mlActorPath),
          allowLocalRoutees = localroutesMl, useRoles = Set(mlRole))).props())
    }

    val initialRiskScore = if (initialRiskScoreRef.isDefined) initialRiskScoreRef.get
    else {
      system.actorOf(
        ClusterRouterGroup(RandomGroup(Nil), ClusterRouterGroupSettings(
          totalInstances = totalNoOfInstancesStatic, routeesPaths = List(rulesAppPath),
          allowLocalRoutees = localroutesStatic, useRoles = Set(staticRole))).props())
    }

    val genericRiskScore = if (genericRiskScoreRef.isDefined) genericRiskScoreRef.get
    else {
      system.actorOf(
        ClusterRouterGroup(RandomGroup(Nil), ClusterRouterGroupSettings(
          totalInstances = totalNoOfInstancesStatic, routeesPaths = List(genericRulesActorPath),
          allowLocalRoutees = localroutesStatic, useRoles = Set(staticRole))).props())
    }

    val persShard = if (persShardRef.isDefined) persShardRef.get
    else
      ShardUtil.genericShardProxy(
        _typeName = genericShardName,
        _role = genericShardRole,
        _numberOfShards = numberOfGenericShards
      )(system)

    val rtShard = if (rtShardRef.isDefined) rtShardRef.get
    else
      ShardUtil.rtShardProxy(
        _typeName = rtShardName,
        _role = rtShardRole,
        _numberOfShards = numberOfRTShards
      )(system)
    mutable.HashMap(ETNI -> etniActor, VCVS -> vcvsActor, MODELHOST -> modelHostActor, KAFKA -> kafkaPublisher,
      INITIALRISKSCORE -> initialRiskScore, GENERICRISKSCORE -> genericRiskScore, PERSISTENCESHARD -> persShard, RTSTREAMSHARD -> rtShard)
  }

  /**
   * Update gatepass based on ClientWF
   * Returns
   * TRUE - If able to find/create all gatepass
   * FALSE - If not able to find/create at least one gatepass
   */
  def updateGatePass(clientWorkflow: ClientWF): Boolean = {
    val nodeTypeMap = clientWorkflow.payload.flowchart.nodes.map(n => (n.nType, n.metadata)).toMap
    val existingGatePassTypes = gatePassMap.keySet + GATEWAY //GATEWAY doesn't exist in gatePassMap. Not required to have that in gatepass
    val newNTypeMap = nodeTypeMap -- existingGatePassTypes
    val newActRefs = newNTypeMap.map(kv => (kv._1, getActorRef(kv._2))).filter(_._2.nonEmpty).mapValues(_.get)
    if (newNTypeMap.size == newActRefs.size) {
      newActRefs.foreach(kv => gatePassMap += kv)
      true // Allow only if able to create actorRef for all new nTypes
    }
    else {
      false // Don't allow  if not able to create actorRef for at least one nType
    }
  }

  /**
   * Create actor ref based on provided metadata from node
   */
  def getActorRef(metadataOp: Option[Map[String, String]]): Option[ActorRef] = {
    try {
      if (metadataOp.isEmpty) None // Can't create actorRef without metadata
      else {
        val metadata = metadataOp.get
        metadata.get(gpType) match {
          case Some(`router`) =>
            (metadata.get(totalInstances), metadata.get(routeesPaths), metadata.get(allowLocalRoutees), metadata.get(useRoles)) match {
              case (Some(ins), Some(path), Some(allow), Some(roles)) =>
                val actRef = system.actorOf(
                  ClusterRouterGroup(RandomGroup(Nil), ClusterRouterGroupSettings(
                    totalInstances = ins.toInt, routeesPaths = path.split(",").map(_.trim).toList,
                    allowLocalRoutees = allow.toBoolean, useRoles = roles.split(",").map(_.trim).toSet)).props())
                Some(actRef)
              case _ => None // Can't create actorRef with incomplete info
            }

          case Some(`shard`) =>
            (metadata.get(shardName), metadata.get(shardRole), metadata.get(numberOfShards)) match {
              case (Some(name), Some(role), Some(num)) =>
                val actRef = ShardUtil.genericShardProxy(
                  _typeName = name,
                  _role = Some(role),
                  _numberOfShards = num.toInt
                )(system)
                Some(actRef)
              case _ => None // Can't create actorRef with incomplete info
            }

          case Some(`rtShard`) =>
            (metadata.get(shardName), metadata.get(shardRole), metadata.get(numberOfShards)) match {
              case (Some(name), Some(role), Some(num)) =>
                val actRef = ShardUtil.rtShardProxy(
                  _typeName = name,
                  _role = Some(role),
                  _numberOfShards = num.toInt
                )(system)
                Some(actRef)
              case _ => None // Can't create actorRef with incomplete info
            }

          case _ => None // Can't create actorRef for unknown gpType
        }
      }
    }
    catch {
      case t: Throwable =>
        None // Exception while creating actorref
    }
  }

  private def updateWorkFlows(clientWorkflow: ClientWF, callerName: String): Boolean = {
    val infoLog = mutable.Map[String, Any]()
    infoLog += ("op" -> clientWorkflow.op)
    infoLog += ("id" -> clientWorkflow.payload.id)
    infoLog += ("module" -> clientWorkflow.payload.module)
    infoLog += ("name" -> clientWorkflow.payload.name)
    infoLog += ("datetime" -> clientWorkflow.payload.datetime)
    infoLog += ("startNodeId" -> clientWorkflow.payload.startNodeId)
    infoLog += ("callerName" -> callerName)
    infoLog += ("isWFExist" -> isWFExist(clientWorkflow.payload))
    clientWorkflow.op match {
      case ADD =>
        try {
          //updateGatePass adds new actor ref's to gate pass
          if (updateGatePass(clientWorkflow)) {
            val newWF = WorkFlowGraph(clientWorkflow.payload, gatePassMap.toMap)
            workFlowMap += (clientWorkflow.payload.module -> newWF)
            infoLog += ("status" -> "Pass")
            logger.info("WorkFlowProcess", null, infoLog)
            true
          }
          else {
            infoLog += ("status" -> "Fail")
            infoLog += ("reason" -> "update gate pass failed")
            logger.info("WorkFlowProcess", null, infoLog)
            false
          }
        }
        catch {
          case ex: Exception =>
            infoLog += ("status" -> "Fail")
            infoLog += ("reason" -> "Failed to initialize work flow class")
            logger.error("WorkFlowProcess", null, infoLog)
            false
        }

      case UPDATE =>
        if (!isWFExist(clientWorkflow.payload)) {
          infoLog += ("status" -> "Fail")
          infoLog += ("reason" -> "Work flow not exist with  module / Update gatepass failed")
          logger.error("WorkFlowProcess", null, infoLog)
          false
        }
        else {
          try {
            //updateGatePass adds new actor ref's to gate pass
            if (updateGatePass(clientWorkflow)) {
              val newWF = WorkFlowGraph(clientWorkflow.payload, gatePassMap.toMap)
              workFlowMap += (clientWorkflow.payload.module -> newWF)
              infoLog += ("status" -> "Pass")
              logger.info("WorkFlowProcess", null, infoLog)
              true
            }
            else {
              infoLog += ("status" -> "Fail")
              infoLog += ("reason" -> "update gate pass failed")
              logger.info("WorkFlowProcess", null, infoLog)
              false
            }
          }
          catch {
            case ex: Exception =>
              infoLog += ("status" -> "Fail")
              infoLog += ("reason" -> "Failed to initialize work flow class")
              logger.error("WorkFlowProcess", null, infoLog)
              false
          }
        }

      case REMOVE =>
        if (!isWFExist(clientWorkflow.payload)) {
          infoLog += ("status" -> "Fail")
          infoLog += ("reason" -> "Work flow doesn't exist with given module")
          logger.error("WorkFlowProcess", null, infoLog)
          false
        }
        else {
          workFlowMap -= clientWorkflow.payload.module
          infoLog += ("status" -> "Pass")
          logger.info("WorkFlowProcess", null, infoLog)
          true
        }

      case _ =>
        infoLog += ("status" -> "Fail")
        infoLog += ("action" -> s"${clientWorkflow.op} cannot be Performed")
        logger.error("WorkFlowProcess", null, infoLog)
        false
    }
  }

  /**
   * Graph helps to dynamically add/update/remove workflows
   */
  private def workflowInMemoryStream: RunnableGraph[NotUsed] = {
    logger.info("workflowInMemoryStream", null, "status", "Stream started")
    val processSink =
      Flow[ClientWF]
        .map(wf => {
          try {
            updateWorkFlows(wf, "InMemory")
          }
          catch {
            case th: Throwable =>
              //To avoid breaking flow
              true
          }
        })
        .toMat(Sink.ignore)(Keep.right)
        .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))

    val validationFlow =
      Flow[ClientWF]
        .filter(wf => {
          val validFlow = ClientWorkflowValidator.isValid(wf.payload)
          if (!validFlow) {
            val errorMap: mutable.Map[String, Any] = mutable.Map[String, Any]()
            errorMap += ("status" -> "validation failed")
            errorMap += ("workFlow" -> wf)
            logger.error("WorkFlowValidationFailed", "NoSessionId", errorMap)
          }
          validFlow
        })
        .withAttributes(ActorAttributes.supervisionStrategy(flowDecider))

    RunnableGraph.fromGraph(g = GraphDSL.create() {
      implicit b =>

        import GraphDSL.Implicits._

        //Source
        val kafkaSource = b.add(SourceUtil.kafkaConsumerPlainSource(UUID.randomUUID().toString,
          workFlowTopic,
          bootStrapServers,
          kafkaConsumerProps)).out

        //Flows
        val MessageUnMarshallFlow = b.add(FlowUtil.unMarshallFlowCopy[ClientWF](Some("workflowInMemoryStream")))
        val ValidationFlow = b.add(validationFlow)

        //Sink
        val ProcessSink = b.add(processSink)

        kafkaSource ~> MessageUnMarshallFlow ~> ValidationFlow ~> ProcessSink
        ClosedShape
    })
  }

  /**
   * Fetch all wfm from Elastic and add to inventory
   */
  private def rehydrate: Future[Done] = {
    logger.info("elastic_wfm_rehydration", null, "status", s"work flow rehydration process started")
    import com.vz.security.sso.workflowmanager.GraphsSprayFormatter._
    lazy val getAllQuery = """{"match_all":{}}""".stripMargin
    SourceUtil.elasticSource[ClientWFPaylaod](wfgIndex, wfgType, getAllQuery)
      .map(q => {
        try {
          updateWorkFlows(ClientWF(ADD, q._2), "rehydration")
        }
        catch {
          case th: Throwable =>
            //To avoid breaking rehydration process
            true
        }
      })
      .withAttributes(ActorAttributes.supervisionStrategy(sinkDecider))
      .runWith(Sink.ignore)
  }


  /**
   * Logs all running queries
   */
  def logWF: Unit = {
    var queryInfoMap = mutable.Map[String, Any]()
    workFlowMap.foreach(q => {
      val map = Map("name" -> q._2.cwf.name,
        "wfid" -> q._2.cwf.id,
        "startTime" -> q._2.createdOn,
        "wf" -> q._2.cwf)
      queryInfoMap += (q._1 -> map)
    })
    logger.info("RunningWF", null, queryInfoMap)
  }

}

object WorkFlowManager {

  def apply(etniActorRef: Option[ActorRef] = None,
            vcvsActorRef: Option[ActorRef] = None,
            modelHostRef: Option[ActorRef] = None,
            initialRiskScore: Option[ActorRef] = None,
            genericRiskScore: Option[ActorRef] = None,
            persShardRef: Option[ActorRef] = None,
            rtShardRef: Option[ActorRef] = None,
            additionalRef: Map[String, ActorRef] = Map[String, ActorRef](),
            bootStrapServers: String,
            kafkaConsumerProps: Map[String, String],
            kafkaProducerProps: Option[Map[String, String]])
           (implicit system: ActorSystem, mat: ActorMaterializer, ec: ExecutionContext): WorkFlowManager =
    new WorkFlowManager(etniActorRef, vcvsActorRef, modelHostRef, initialRiskScore, genericRiskScore, persShardRef, rtShardRef, additionalRef,
      bootStrapServers, kafkaConsumerProps, kafkaProducerProps)

  var workFlowMap: mutable.HashMap[String, WorkFlowGraph] = mutable.HashMap[String, WorkFlowGraph]()

  def isWFExist(clientWorkflow: ClientWFPaylaod): Boolean = {
    workFlowMap.get(clientWorkflow.module).isDefined
  }

  private val router = "router"
  private val shard = "shard"
  private val rtShard = "rtShard"
  private val shardName = "shardName"
  private val shardRole = "shardRole"
  private val numberOfShards = "numberOfShards"
  private val gpType = "gpType"
  private val totalInstances = "totalInstances"
  private val routeesPaths = "routeesPaths"
  private val allowLocalRoutees = "allowLocalRoutees"
  private val useRoles = "useRoles"

}

