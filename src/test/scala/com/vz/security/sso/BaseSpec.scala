package com.vz.security.sso

import org.scalatest._
import org.scalatest.concurrent.ScalaFutures

trait BaseSpec extends FlatSpecLike
  with MustMatchers
  with BeforeAndAfterAll
  with BeforeAndAfterEach
  with ScalaFutures

