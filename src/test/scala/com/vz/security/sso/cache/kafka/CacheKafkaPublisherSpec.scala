package com.vz.security.sso.cache.kafka

import akka.actor.ActorSystem
import akka.testkit.TestKit
import com.vz.security.sso.camunda.redis.CamundaRedisClient
import com.vz.security.sso.models.{CacheConfig, CacheEvictionPolicy, CacheManagerOp, CacheOp}
import com.vz.security.sso.objects.CamundaRule
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.scalatest.concurrent.ScalaFutures

class CacheKafkaPublisherSpec extends TestKit(ActorSystem("SecurityAnalyticsCluster"))
  with Matchers with WordSpecLike with ScalaFutures with BeforeAndAfterAll {

  override def beforeAll(): Unit = {

  }

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "The Cache Manager Kafka Publisher" should {
    "should return successful when publishing a cache configuration" in {
      val config = Option(CacheManagerOp("add", CacheConfig("TestMohita2", "TestMohita2", "MANGO", Set("mango_topic"), Option(CacheEvictionPolicy("type",Option(10),Option(10))), Set("IP", "IpAddress"), List(), "IP", true, false, List("IP"), true, false)))
      val resp = CacheKafkaPublisher.publishConfigToKafka(config)
      whenReady(resp) {
        value => value shouldBe true
      }
    }
  }

  
  "The Cache Kafka Publisher" should {

    "should return successful when publishing cache data" in {
      val list = Option(
        List(
          CacheOp("add", "MangoCacheTestMohitha", Map(
            "first" -> "123.168.1.1",
            "test2" -> "123.168.1.100"
          ))
        )
      )
      val resp = CacheKafkaPublisher.publishCacheData(list, Set("mango_topic"))
      whenReady(resp) {
        value => value shouldBe true
      }
    }
  }
}
