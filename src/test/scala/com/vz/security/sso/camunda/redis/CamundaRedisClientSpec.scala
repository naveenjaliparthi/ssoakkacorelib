package com.vz.security.sso.camunda.redis

import java.util.UUID

import akka.actor.ActorSystem
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import akka.testkit.TestKit
import com.github.sebruck.EmbeddedRedis
import com.vz.security.sso.objects.CamundaRule
import org.scalatest._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Seconds, Span}
import redis.embedded.RedisServer


class CamundaRedisClientSpec extends TestKit(ActorSystem("SecurityAnalyticsCluster"))
  with Matchers with WordSpecLike with ScalaFutures  with EmbeddedRedis with BeforeAndAfterAll {

  implicit val defaultPatience = PatienceConfig(timeout = Span(60, Seconds), interval = Span(500, Millis))
  var redis: RedisServer = _
  var redisPort: Int = _

  override def beforeAll(): Unit = {
    redis = startRedis(7386) // A random free port is chosen
    redisPort = redis.ports().get(0)
    CamundaRedisClient.initialize(system)
  }

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
    stopRedis(redis)
  }

  "The Camunda Redis Client" should {

    "return true when new rulebook is added" in {
      val rule = new CamundaRule(timestamp = (System.currentTimeMillis / 1000), uuid = UUID.randomUUID(), dmnUUID = UUID.randomUUID(), dmnXML = "", name = "Test1", module = "test1", desc = "lets test it", byUserId = "v160771", operation="add")
      whenReady(CamundaRedisClient.addRulebook(Option(rule))) {
        value => value shouldBe true
      }
    }

    "return true when retrieving all rulebook ids in system" in {
      whenReady(CamundaRedisClient.getAllRulebooks) {
        value => (value.isEmpty) shouldBe true
      }
    }


    "return true when new rulebook version is added" in {
      val rule = new CamundaRule(timestamp = (System.currentTimeMillis / 1000), UUID.fromString("059bd2c3-e85b-4051-9734-a2c602f8bf2e"), dmnUUID = UUID.randomUUID(), dmnXML = "", name = "IG Rule", module = "ig", desc = "lets test it", byUserId = "v160771", operation="add")
      CamundaRedisClient.addRulebook(Option(rule))
      val newVersion = rule.copy(dmnUUID = UUID.randomUUID(), dmnXML = "new xml")
      whenReady(CamundaRedisClient.addRulebookVersion(Option(newVersion))) {
        value => value shouldBe true
      }
    }

    "return list of rule versions when specific rulebook id is provided" in {
      whenReady(CamundaRedisClient.getRulebookVersions(UUID.fromString("af63594f-9457-432b-bf9c-6b562798d568"), 0, 0 )){
        ruleVersionList => (ruleVersionList.length > 0) shouldBe true
      }
    }

    "return true when multiple dmn files are deleted" in {
      whenReady(CamundaRedisClient.deleteRulebookDmns(Option(List(UUID.fromString("a7f970f1-5214-42de-9d59-f6ec77af3093"), UUID.fromString("6e965ec0-fa2c-4af2-8192-d8af32b7f915"), UUID.fromString("1c185579-f9be-4a78-9f56-67319358713b"), UUID.fromString("786503e9-95ec-48c9-9348-5006d6bb2813"))))) {
        value => value shouldBe true
      }
    }

    "return true when total # of rulebook versions are maintained at max configured limit" in {
      val rule = new CamundaRule(timestamp = (System.currentTimeMillis / 1000), UUID.fromString("059bd2c3-e85b-4051-9734-a2c602f8bf2e"), dmnUUID = UUID.randomUUID(), dmnXML = "", name = "IG Rule", module = "ig", desc = "lets test it", byUserId = "v160771", operation="add")
      whenReady(CamundaRedisClient.maintainMaxRulebookVersions(Option(rule))){
        value => value shouldBe true
      }
    }

  }
}
