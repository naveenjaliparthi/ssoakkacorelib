package com.vz.security.sso.camunda.redis

import java.io.{FileInputStream, InputStream}
import java.nio.file.Paths

import org.camunda.bpm.dmn.engine.impl.DefaultDmnEngineConfiguration
import org.camunda.bpm.dmn.engine.{DmnDecision, DmnDecisionTableResult, DmnEngineConfiguration}
import org.camunda.bpm.engine.variable.Variables
import org.camunda.feel.integration.CamundaFeelEngineFactory

object RuleEngineTest {

    def main(args: Array[String]): Unit = {
      val IPCategories: List[String] = List("COMPROMISED", "DYNAMICBLOCK")
//      val UserIDCategories: List[String] = List("BLACKLIST", "DYNAMICBLOCK")
//      val Source: String = ".VERIZONWIRELESS.COM"
//      val LOB: String = "VZT"
//      val Country: String = "US"
//      val State: String = "CA"
      val variables = Variables
        .putValue("IPCategories", IPCategories)
//        .putValue("UserIDCategories", UserIDCategories)
//        .putValue("Source", Source)
//        .putValue("LOB", LOB)
//        .putValue("Country", Country)
//        .putValue("State", State)

      // create a new default DMN engine
      val dmnEngineConfig  = DmnEngineConfiguration.createDefaultDmnEngineConfiguration().asInstanceOf[DefaultDmnEngineConfiguration]
      dmnEngineConfig.setDefaultInputEntryExpressionLanguage("feel")
      val dmnEngine = dmnEngineConfig.feelEngineFactory(new CamundaFeelEngineFactory).buildEngine()

//      var dmnFile: InputStream = this.getClass.getResourceAsStream("StaticRulesTest.dmn")
       val dmnFile = new FileInputStream(Paths.get("/Users/khana8s/projects/ssoakkacorelib/src/test/scala/com/vz/security/sso/camunda/redis/StaticRulesTest.dmn").toFile)
      val decision: DmnDecision = dmnEngine.parseDecision("decision", dmnFile)
      val tableResult: DmnDecisionTableResult = dmnEngine.evaluateDecisionTable(decision, variables)

      // print result
      val finalResult = tableResult.get(0).getEntryMap.entrySet().iterator().forEachRemaining(println)
//      val finalResult1 = tableResult.get(0).getEntryMap.get("ReasonName")
//      println("finalResult :: " + finalResult + " ::: finalResult1 :: " + finalResult1)
    }
}