package com.vz.security.sso.compression.gzip

import akka.actor.ActorSystem
import akka.serialization.SerializationExtension
import com.typesafe.config.ConfigFactory

import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

class GzipTest
  extends WordSpec
    with Matchers
    with BeforeAndAfterAll {
/*
  "A sequence of bytes" when {
    "compressed with Gzip" should {
      "represent the original value when decompressed" in {

        val originalBytes = "Hello World".getBytes

        val compressed = Gzip.compressBytes(originalBytes)

        val decompressed = Gzip.decompressBytes(compressed)

        originalBytes should equal (decompressed)

      }

      "represent the original value when decompressed and be shorter than the decompressed value" in {

        val originalBytes = TestCaseClass(
          "49dd9537-0a90-4d99-b0ad-bb6f13de3c01",
          1221, 232132321313L, List(1,2,3),
          Array(-8,58,-71,17,-128,-72,-101,34,102,-77,22,-98,-75,-16,-86,34,-20,-96,41,62,-35,63,-68,-99,-98,-81,121,-112,-85,66,-95,39,
            -78,18,-122,37,-128,-72,-101,-34,102,-57,22,98,-75,-16,-6,34,-20,-96,41,62,-35,63,67,-99,-98,81,121,-112,-85,66,-95,39,
            -48,88,-71,87,-128,-7,-101,34,102,-77,22,-98,-65,16,-86,74,-20,6,-44,12,-35,93,-68,-99,-98,-81,121,-112,-85,46,-95,9,18)
        ).serialise

        val compressed = Gzip.compressBytes(originalBytes)

        val decompressed = Gzip.decompressBytes(compressed)

        originalBytes should equal (decompressed)

        compressed.length should be < decompressed.length

      }

      "test size savings for realistic message object" in {

        lazy implicit val actorSystem: ActorSystem = ActorSystem("test-system", ConfigFactory.empty())
        lazy val serialization = SerializationExtension(actorSystem)

        val snapshotBytes = serialization.serialize(UserAuthRiskScore(
          Some("07646ccd-6a81-4211-a721-6f0f762d682d"), Some(120152398L), Some("US"),Some("lvn"), Some("50"), None)
        ).get

        val compressed = Gzip.compressBytes(snapshotBytes)

        val decompressed = Gzip.decompressBytes(compressed)

//        println(compressed.length)
//        println(decompressed.length)

        snapshotBytes should equal (decompressed)

        compressed.length should be < decompressed.length

      }
    }

  }
*/
}