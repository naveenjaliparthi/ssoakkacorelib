package com.vz.security.sso.encryption

import java.time.Instant
import java.util.UUID

import com.vz.security.sso.BaseSpec

class EncryptionUtilSpec extends BaseSpec {

  override def afterEach: Unit = {
  }

  override def afterAll: Unit = {
  }

  /**
   * ###################
   * #### AES256 #######
   * ###################
   */
  "AES256" should "should encrypt a string" in {
    val profileTokenEnc = EncryptionUtil.encrypt(EncryptInfoObj.PROFILE_TOKEN)(_)

    val profileTokenDec = EncryptionUtil.decrypt(EncryptInfoObj.PROFILE_TOKEN)(_)

    val uid = UUID.randomUUID().toString

    import java.time.Duration
    val start = Instant.now
    val encText = profileTokenEnc(uid + uid + uid)
    val end = Instant.now
    val decText = profileTokenDec(encText)
    val end2 = Instant.now
    System.out.println(Duration.between(start, end)) // prints PT1M3.553S
    System.out.println(Duration.between(end, end2)) // prints PT1M3.553S

    println("ENCRYPTED TEXT " + encText)
    println("DECRYPTED TEXT " + decText)
    assert(decText == uid + uid + uid)
  }

  "AES256" should "should encrypt a Map" in {
    val profileTokenEnc = EncryptionUtil.encryptMap(EncryptInfoObj.PROFILE_TOKEN)(_)

    val profileTokenDec = EncryptionUtil.decryptMap(EncryptInfoObj.PROFILE_TOKEN)(_)

    val uid1 = UUID.randomUUID().toString
    val uid2 = UUID.randomUUID().toString
    val uid3 = UUID.randomUUID().toString

    import java.time.Duration
    val start = Instant.now
    val inputMap = Map("uuid1" -> uid1, "uuid2" -> uid2, "uuid3" -> uid3)
    val encMap = profileTokenEnc(inputMap)
    val end = Instant.now
    val decMap = profileTokenDec(encMap)
    val end2 = Instant.now
    System.out.println(Duration.between(start, end)) // prints PT1M3.553S
    System.out.println(Duration.between(end, end2)) // prints PT1M3.553S

    println("ENCRYPTED MAP " + encMap)
    println("DECRYPTED MAP " + decMap)
    assert(decMap == inputMap)
  }

}
