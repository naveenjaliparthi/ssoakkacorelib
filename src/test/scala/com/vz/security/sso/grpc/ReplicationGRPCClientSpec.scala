package com.vz.security.sso.grpc

import akka.actor.{Actor, ActorSystem, Props}
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import com.typesafe.config.{Config, ConfigFactory}
import com.vz.security.sso.BaseSpec
import com.vz.security.sso.grpc.client.{ReplicationGrpcConfig, ReplicationGrpcLbClient, ReplicationGrpcPlainClient}
import com.vz.security.sso.grpc.server.{ReplicationGrpcServer, ReplicationGrpcServerConfig}
import com.vz.security.sso.grpc.services.ReplicationRequest
import com.vz.security.sso.grpc.util.GrpcUtil
import org.scalatest.time.{Millis, Seconds, Span}

import scala.concurrent.ExecutionContext

class ReplicationGRPCClientSpec extends TestKit(ActorSystem("SecurityTestCluster", TestConfig.config)) with BaseSpec {

  val testActor1 = system.actorOf(Props[TestActorServer1], name = "testActor1")
  val testActor2 = system.actorOf(Props[TestActorServer2], name = "testActor2")
  implicit val actorSystem: ActorSystem = system
  implicit val ec: ExecutionContext = system.dispatcher
  implicit val mat: ActorMaterializer = ActorMaterializer()

  val config = ConfigFactory.load()
  val grpcHost = "localhost"

  implicit val defaultPatience: PatienceConfig = PatienceConfig(timeout = Span(60, Seconds), interval = Span(500, Millis))
  val payload = Map("msg" -> "GRPC works across languages and platforms!")
  val mType = "test"

  override def afterEach: Unit = {
    println()
  }

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  /**
    * ####################
    * #### PLAIN CLIENT ##
    * ####################
    */
  "Replication Grpc Plain Client" should "send message to one server" in {
    new ReplicationGrpcServer(ReplicationGrpcServerConfig(grpcServerConfig("localhost", 5000)), testActor1)
    val grpcClientConfigPlain = ReplicationGrpcConfig(grpcPlainClientConfig(grpcHost, 5000))
    val client = new ReplicationGrpcPlainClient(grpcClientConfigPlain)
    val t0 = System.currentTimeMillis()
    whenReady(client.replicateMessage(ReplicationRequest(rMap = payload, rType = mType))) {
      status => {
        val t1 = System.currentTimeMillis()
        println("Elapsed time: " + (t1 - t0) + "ms")
        status must be
        200
      }
    }
  }

  /**
    * ###################
    * #### LB CLIENT ####
    * ###################
    */
  "Replication Grpc LB Client" should "send message with one server" in {
    GrpcUtil.grpcReplicationInit(system, ec, mat, testActor1, grpcLBClientConfigOneSever(grpcHost, 5001), grpcServerConfig("localhost", 5001))
    val t0 = System.currentTimeMillis()
    GrpcUtil.replicateMessage(ReplicationRequest(rMap = payload, rType = mType))
  }

  "Replication Grpc LB Client" should "send message with two servers" in {
    GrpcUtil.grpcReplicationInit(system, ec, mat, testActor2, grpcLBClientConfigTwoServers(grpcHost, 5002, 5003), grpcServerConfig("localhost", 5002))
    new ReplicationGrpcServer(ReplicationGrpcServerConfig(grpcServerConfig("localhost", 5003)), testActor1) // Starting second server directly
    val t0 = System.currentTimeMillis()
    GrpcUtil.replicateMessage(ReplicationRequest(rMap = payload, rType = mType))
  }

  /**
    * GrpcUtil is singleton so directly accessing server and client
    */
  "Replication Grpc LB Client" should "cross communicate between apps" in {
    // APP 1 - testActor1
    val server1 = new ReplicationGrpcServer(ReplicationGrpcServerConfig(grpcServerConfig("localhost", 5004)), testActor1)
    val client1 = new ReplicationGrpcLbClient(grpcLBClientConfigOneSever(grpcHost, 5005).head, "supervisor1")

    // APP 2 - testActor2
    val server2 = new ReplicationGrpcServer(ReplicationGrpcServerConfig(grpcServerConfig("localhost", 5005)), testActor2)
    val client2 = new ReplicationGrpcLbClient(grpcLBClientConfigOneSever(grpcHost, 5004).head, "supervisor2")

    // Client from app1 to server in app2
    whenReady(client1.replicateMessage(ReplicationRequest(rMap = Map("msg" -> " - message goes to test actor2"), rType = mType))) {
      status => {
        status must be
        200
      }
    }

    // Client from app2 to server in app1
    whenReady(client2.replicateMessage(ReplicationRequest(rMap = Map("msg" -> " - message goes to test actor1"), rType = mType))) {
      status => {
        status must be
        200
      }
    }
  }

  private def grpcServerConfig(grpcHost: String, grpcPort: Int): Config = {
    ConfigFactory.parseString(
      s"""{
         |  host = ${grpcHost}
         |  port = ${grpcPort}
         |  timeout = 2000ms
         |}""".stripMargin)
  }

  private def grpcPlainClientConfig(grpcHost: String, grpcPort: Int): Config = {
    ConfigFactory.parseString(
      s""" {
         |    host = ${grpcHost}
         |    port = ${grpcPort}
         |    deadline = 5s,
         |    enabletls = false,
         |  }""".stripMargin)
  }

  private def grpcLBClientConfigOneSever(grpcHost: String, grpcPort: Int): List[Config] =
    List(ConfigFactory.parseString(
      s"""{
         |    dcname = "test"
         |    timeout = 5s
         |    grpcs = [
         |      {
         |        grpc {
         |          host = ${grpcHost}
         |          port = ${grpcPort}
         |          deadline = 5s
         |        }
         |        max-failures = 10
         |        call-timeout = 10000000
         |        reset-timeout = 15000
         |      }
         |    ]
         |  }""".stripMargin))

  private def grpcLBClientConfigTwoServers(grpcHost: String, grpcPort1: Int, grpcPort2: Int): List[Config] = {
    List(ConfigFactory.parseString(
      s"""{
         |    timeout = 5s
         |    grpcs = [
         |      {
         |        grpc {
         |          host = ${grpcHost}
         |          port = ${grpcPort1}
         |          deadline = 5s
         |        }
         |        max-failures = 10
         |        call-timeout = 10000000
         |        reset-timeout = 15000
         |      },
         |      {
         |        grpc {
         |          host = ${grpcHost}
         |          port = ${grpcPort2}
         |          deadline = 5s
         |        }
         |        max-failures = 10
         |        call-timeout = 10000000
         |        reset-timeout = 15000
         |      }
         |    ]
         |  }""".stripMargin))
  }
}

private class TestActorServer1 extends Actor {
  def receive = {
    case replicatedRequest: ReplicationRequest =>
      println("Test Actor Server1: Received replication request " + replicatedRequest)
    case _ => println("Test Actor Server1: Unsupported request type")
  }
}

private class TestActorServer2 extends Actor {
  def receive = {
    case replicatedRequest: ReplicationRequest =>
      println("Test Actor Server2: Received replication request " + replicatedRequest)
    case _ => println("Test Actor Server2: Unsupported request type")
  }
}
