package com.vz.security.sso.grpc

import com.typesafe.config.{Config, ConfigFactory}

object TestConfig {

  val config: Config = ConfigFactory.parseString("akka.cluster.roles = [persistent]").withFallback(ConfigFactory.load())

}
