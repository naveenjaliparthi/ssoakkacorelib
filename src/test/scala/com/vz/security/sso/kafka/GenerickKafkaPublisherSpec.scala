package com.vz.security.sso.kafka

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.scalatest.concurrent.ScalaFutures
import com.vz.security.sso.models.KakfaPublisherPayload

class GenerickKafkaPublisherSpec extends TestKit(ActorSystem("SecurityAnalyticsCluster"))
  with Matchers with WordSpecLike with ScalaFutures with BeforeAndAfterAll {

  "The Kafka Generic Publisher" should {
    "should return list of kafka instance names when requested" in {
      val resp = GenerickKafkaPublisher.getKafkaInstanceNames
      resp.length should be > 0
      resp should equal(List("SDC-PLAIN", "SDC", "TDC", "AWS"))
    }

    "should return kafka properties for given kafka instance name that has key" in {
      val prop = GenerickKafkaPublisher.getKafkaProperties("SDC", hasKey = true)
      prop.get should not be null
      prop.get.getProperty("key.serializer") should not be null
    }

    "should return kafka properties for given kafka instance name that doesn't have key" in {
      val prop = GenerickKafkaPublisher.getKafkaProperties("SDC", hasKey = false)
      prop.get should not be null
      //prop.get.getProperty("key") should be null
    }

    "should return true on successful posting of kakfa payload with key" in {
      val req = new KakfaPublisherPayload(
        topicName = List("cache_manager_temp_topic"),
        kafkaList = List("SDC"),
        topicWithKey = true,
        payload = List( "1:test1", "test:test2")
      )
      val res = GenerickKafkaPublisher.publishPayload(Option(req))
      whenReady(res) {
        value => value shouldBe true
      }
    }

    "should return true on successful posting of kakfa payload without key" in {
      val req = new KakfaPublisherPayload(
        topicName = List("cache_manager_temp_topic"),
        kafkaList = List("SDC", "TDC"),
        topicWithKey = false,
        payload = List( "test21", "test22")
      )
      val res = GenerickKafkaPublisher.publishPayload(Option(req))
      whenReady(res) {
        value => value shouldBe true
      }
    }
  }
}
