package com.vz.security.sso.sqm.kafka

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.scalatest.concurrent.ScalaFutures

class SqmKafkaPublisherSpec extends TestKit(ActorSystem("SecurityAnalyticsCluster"))
  with Matchers with WordSpecLike with ScalaFutures with BeforeAndAfterAll {
  override def beforeAll(): Unit = {
  }

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "The Streams Query Manager" should {
    "should return successful when publishing a query configuration" in {
      val queryConfig = Option("{\"id\":\"2343\",\"name\":\"statusCheckTest\",\"action\":\"add\",\"parallelism\":2,\"windowsize\":54654,\"slide\":566,\"from\":\"Test\",\"fields\":[\"ipAddress\",\"reason\",\"type\"],\"where\":\"(reason:=PS)~&&~(type:=tp)\",\"shard\":\"ipAddress\",\"group\":[\"ipAddress\"],\"having\":\"count(reason:=PS)~>10\",\"select\":[\"ipAddress\"],\"to\":\"Test12\"}")
      val resp = SqmKafkaPublisher.publishConfigToKafka(queryConfig)
      whenReady(resp) {
        vslue => vslue shouldBe(true)
      }
    }
  }
}
