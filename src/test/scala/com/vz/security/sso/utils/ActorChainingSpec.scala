package com.vz.security.sso.utils

import com.vz.security.sso.BaseSpec
import com.vz.security.sso.common.constants.SharedConstants._

class ActorChainingSpec extends BaseSpec {

  val xmdnKey = "xMdn"
  val useridKey = "userID"
  val rtKey = "af1cda07-2ebb-4361-b57c-0494b58ade5a:userID:5"
  val shardValueRT = "af1cda07-2ebb-4361-b57c-0494b58ade5a:1"

  val initalPayload = Map(RISKSCORE -> "50", REASONFORDECISION -> "camunda",
    ACTORSTOCALL -> s"${xmdnKey}:75${CHAINDELIMITER}${useridKey}:100", xmdnKey -> "1234", useridKey -> "u99")

  /**
    * #####################
    * ## STATIC REQUESTS ##
    * #####################
    */
  "ActorChainImpl" should "compute request to next actor with two actors in chain" in {
    val payload = initalPayload
    val nextPhase = ShardUtil.computeNextPhase(payload)
    val expectedMap = payload ++ Map(ACTORSTOCALL -> s"${useridKey}:100", SHARDKEY -> xmdnKey, SHARDVALUE -> "1234", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, GENERICSHARD))
  }

  "ActorChainImpl" should "compute request to next actor with xmdn actor in chain" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${xmdnKey}:75")
    val nextPhase = ShardUtil.computeNextPhase(payload)
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", SHARDKEY -> xmdnKey, SHARDVALUE -> "1234", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, GENERICSHARD))
  }

  "ActorChainImpl" should "compute request to next actor with userid actor in chain" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${useridKey}:100")
    val nextPhase = ShardUtil.computeNextPhase(payload)
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", SHARDKEY -> useridKey, SHARDVALUE -> "u99", ACTORTHRESHOLD -> "100")
    assert(nextPhase == (expectedMap, GENERICSHARD))
  }

  "ActorChainImpl" should "compute request to next actor with rt actor in chain" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${rtKey}:100")
    val nextPhase = ShardUtil.computeNextPhase(payload)
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", ACTORID -> shardValueRT, ACTORTHRESHOLD -> "100")
    assert(nextPhase == (expectedMap, RTSHARD))
  }

  "ActorChainImpl" should "compute request to gateway with no actors in chain" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> "")
    val nextPhase = ShardUtil.computeNextPhase(payload)
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "")
    assert(nextPhase == (expectedMap, REPLY))
  }

  "ActorChainImpl" should "call second actor because of missing first actor value in payload" in {
    val payload = initalPayload + (xmdnKey -> "")
    val nextPhase = ShardUtil.computeNextPhase(payload)
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", SHARDKEY -> useridKey, SHARDVALUE -> "u99", ACTORTHRESHOLD -> "100")
    assert(nextPhase == (expectedMap, GENERICSHARD))
  }

  "ActorChainImpl" should "call gateway because of missing actor value in payload - 2 actors" in {
    val payload = initalPayload ++ Map(xmdnKey -> "", useridKey -> "")
    val nextPhase = ShardUtil.computeNextPhase(payload)
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "")
    assert(nextPhase == (expectedMap, REPLY))
  }

  "ActorChainImpl" should "call gateway because of missing actor value in payload - 1 actor" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${useridKey}:100", useridKey -> "")
    val nextPhase = ShardUtil.computeNextPhase(payload)
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "")
    assert(nextPhase == (expectedMap, REPLY))
  }


  /**
    * ###################
    * ## XMDN REQUESTS ##
    * ###################
    */
  "ActorChainImpl" should "compute request to user actor from xmdn" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${useridKey}:100", ACTORTHRESHOLD -> "100")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("xmdnReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", SHARDKEY -> useridKey, SHARDVALUE -> "u99", RISKSCORE -> "75",
      REASONFORDECISION -> "xmdnReason", ACTORTHRESHOLD -> "100")
    assert(nextPhase == (expectedMap, GENERICSHARD))
  }

  "ActorChainImpl" should "compute request to rt actor from xmdn" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${rtKey}:100", ACTORTHRESHOLD -> "100")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("xmdnReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", ACTORID -> shardValueRT, RISKSCORE -> "75",
      REASONFORDECISION -> "xmdnReason", ACTORTHRESHOLD -> "100")
    assert(nextPhase == (expectedMap, RTSHARD))
  }

  "ActorChainImpl" should "compute request to gateway from xmdn actor - reached threshold" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${useridKey}:100", ACTORTHRESHOLD -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("xmdnReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "xmdnReason", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, REPLY))
  }


  "ActorChainImpl" should "compute request to gateway from xmdn actor - reached threshold & no chain" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> "", ACTORTHRESHOLD -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("100"), Some("xmdnReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "100",
      REASONFORDECISION -> "xmdnReason", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, REPLY))
  }

  "ActorChainImpl" should "honor gateway score if it is matching with xmdn - reached threshold" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${useridKey}:100", ACTORTHRESHOLD -> "75", RISKSCORE -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("xmdnReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "camunda", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, REPLY))
  }

  "ActorChainImpl" should "honor gateway score if it is matching with xmdn - No Chain" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("xmdnReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "camunda")
    assert(nextPhase == (expectedMap, REPLY))
  }

  /**
    * ###################
    * ## USER REQUESTS ##
    * ###################
    */

  "ActorChainImpl" should "compute request to xmdn actor from user" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${xmdnKey}:100", ACTORTHRESHOLD -> "100")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("userReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", SHARDKEY -> xmdnKey, SHARDVALUE -> "1234", RISKSCORE -> "75",
      REASONFORDECISION -> "userReason", ACTORTHRESHOLD -> "100")
    assert(nextPhase == (expectedMap, GENERICSHARD))
  }

  "ActorChainImpl" should "compute request to rt actor from user" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${rtKey}:100", ACTORTHRESHOLD -> "100")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("userReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", ACTORID -> shardValueRT, RISKSCORE -> "75",
      REASONFORDECISION -> "userReason", ACTORTHRESHOLD -> "100")
    assert(nextPhase == (expectedMap, RTSHARD))
  }


  "ActorChainImpl" should "compute request to gateway from user actor - reached threshold" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${xmdnKey}:100", ACTORTHRESHOLD -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("userReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "userReason", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, REPLY))
  }


  "ActorChainImpl" should "compute request to gateway from user actor - reached threshold & no chain" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> "", ACTORTHRESHOLD -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("userReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "userReason", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, REPLY))
  }

  "ActorChainImpl" should "honor gateway score if it is matching with user - reached threshold" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${xmdnKey}:100", ACTORTHRESHOLD -> "75", RISKSCORE -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("userReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "camunda", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, REPLY))
  }

  "ActorChainImpl" should "honor gateway score if it is matching with user - No Chain" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> "", ACTORTHRESHOLD -> "75", RISKSCORE -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("userReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "camunda")
    assert(nextPhase == (expectedMap, REPLY))
  }

  /**
    * ###################
    * ## RT REQUESTS ##
    * ###################
    */

  "ActorChainImpl" should "compute request to xmdn actor from RT" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${xmdnKey}:100", ACTORTHRESHOLD -> "100")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("rtReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", SHARDKEY -> xmdnKey, SHARDVALUE -> "1234", RISKSCORE -> "75",
      REASONFORDECISION -> "rtReason", ACTORTHRESHOLD -> "100")
    assert(nextPhase == (expectedMap, GENERICSHARD))
  }


  "ActorChainImpl" should "compute request to gateway from RT actor - reached threshold" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${xmdnKey}:100", ACTORTHRESHOLD -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("rtReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "rtReason", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, REPLY))
  }


  "ActorChainImpl" should "compute request to gateway from RT actor - reached threshold & no chain" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> "", ACTORTHRESHOLD -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("rtReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "rtReason", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, REPLY))
  }

  "ActorChainImpl" should "honor gateway score if it is matching with user - reached threshold -rt" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> s"${xmdnKey}:100", ACTORTHRESHOLD -> "75", RISKSCORE -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("rtReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "camunda", ACTORTHRESHOLD -> "75")
    assert(nextPhase == (expectedMap, REPLY))
  }

  "ActorChainImpl" should "honor gateway score if it is matching with user - No Chain -rt" in {
    val payload = initalPayload ++ Map(ACTORSTOCALL -> "", ACTORTHRESHOLD -> "75", RISKSCORE -> "75")
    val nextPhase = ShardUtil.computeNextPhase(payload, Some("75"), Some("rtReason"))
    val expectedMap = payload ++ Map(ACTORSTOCALL -> "", RISKSCORE -> "75",
      REASONFORDECISION -> "camunda")
    assert(nextPhase == (expectedMap, REPLY))
  }

}
