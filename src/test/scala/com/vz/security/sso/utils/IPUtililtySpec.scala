package com.vz.security.sso.utils

import com.vz.security.sso.BaseSpec

class IPUtililtySpec extends BaseSpec {

  val ipv4 = "127.0.0.1"
  val ipv6 = "2605:e000:fb47:c700:75ca:c74b:8efc:2117"

  "IPUtililtySpec" should "return Long value" in {
    val result = IPUtililty.ipToLong(ipv4)
    println("value is :: " + result)
    assert(result > 0)
  }

  "IPUtililtySpec" should "return 0" in {
    val result = IPUtililty.ipToLong(ipv6)
    println("value is :: " + result)
    assert(result == 0)
  }

}
