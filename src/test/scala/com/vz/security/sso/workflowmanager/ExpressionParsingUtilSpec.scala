package com.vz.security.sso.workflowmanager

import com.vz.security.sso.BaseSpec

class ExpressionParsingUtilSpec extends BaseSpec {

  "StringCompiler" should "compile string operation" in {
    val eq = """map(\"fn\")+map(\"ln\")"""
    val testData = StringContext treatEscapes eq
    val whereEX = ExpressionParsingUtil.compileStringExpression(testData)
    val map = Map("fn" -> "SPS", "ln" -> "otp")
    println(whereEX(map))
  }


  "StringCompiler" should "compile speed operation" in {
        val speedEq = """var speedAsMPH = 0.0;if (map.contains("lat")&& map.contains("long")&& map.contains("time")) {val lon1: Double = map.getOrElse("long", "0.0").toDouble;val lon2: Double = map.getOrElse("longitude", "0.0").toDouble;val lat1: Double = map.getOrElse("lat", "0.0").toDouble;val lat2: Double = map.getOrElse("latitude", "0.0").toDouble;val glTimestamp = map.getOrElse("time","0").toLong;val sysTimestamp = System.currentTimeMillis()/1000;val timeInHours = (sysTimestamp - glTimestamp) / (60 * 60).toDouble;val theta = lon1 - lon2;var dist = Math.sin(lat1 * Math.PI / 180.0) * Math.sin(lat2 * Math.PI / 180.0) + Math.cos(lat1 * Math.PI / 180.0) * Math.cos(lat2 * Math.PI / 180.0) * Math.cos(theta * Math.PI / 180.0);dist = Math.acos(dist);dist = dist * 180.0 / Math.PI;dist = dist * 60 * 1.1515;speedAsMPH = dist / timeInHours};speedAsMPH.toString"""
        val epochEq = """import java.text.SimpleDateFormat;import java.util.Date;if (map.contains("time")) {val sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");val dt = sdf.parse(map.getOrElse("time",new Date().toString));val epoch = (dt.getTime)/1000;epoch.toString} else """""
        val randomNumberEq = """val start = 1;val end   = 5;val rnd = new scala.util.Random;val randomNumber = start + rnd.nextInt( (end - start) + 1 );randomNumber.toString"""
        val pageRefererEq =  """import scala.util.Try;import java.net.URL;val url = Try {new URL(map.getOrElse("pagereferer", ""))}.toOption;if (url.isDefined) url.get.getHost else """""
        val eq = """(module:=:generic_route)~&&~((~(mtn:isEmpty)~~&&~~(randomNumber:=:2)~)~||~(~(mtn:isEmpty)~~&&~~(randomNumber:!=:2)~)~||~(~(mtn:isNonEmpty)~~&&~~(randomNumber:!=:2)~))"""
        val whereEX = ExpressionParsingUtil.compileWhereExpression(eq)
        val ssoMap = Map("IPAddress"->"184.98.249.0","time" -> "Wed May 13 05:00:00 EDT 2020","module"->"ssotest","stateCode"->"AZ","actorsToCall"->"xMdn:75,UserId:75","reasonForDecision"->"US_IP","cityName"->"El Mirage","httpRoute"->"sso","fn_ln"->"","mtn"->"1111222222","lob"->"vzt1","uuid"->"vzepixma1","toRulEngine"->"true","riskscore"->"0","safegaurdingErrorcode"->"-2","countryCode"->"US","nodeType"->"initialRiskScore","NetworkId"->"12083961","wfId"->"123-gvuhb-123","wfNodeId"->"vuiy8hbi89-hbihb2hbjk","userId"->"testnetwork2","lat"-> "48.887535095214844","latitude"-> "76.7675","long"-> "-69.93431854248047","longitude"-> "-18.2844","time"-> "1589522400")
        val map = Map("module"->"generic_route","randomNumber"->"2","mtn"->"1111222222")
    println("!!!!!!!!!!!"+whereEX(map))
  }


  "WhereCompiler" should "compile test one operation" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(~(etni:=:Y)~)~||~((~(etni:=:N)~)~&&~(~(omdnStatus:=:PO)~~||~~(omdnStatus:=:NH)~))""")
    val listOfMaps = List(Map("etni" -> "Y", "omdnStatus" -> ""),
      Map("etni" -> "Y", "omdnStatus" -> "AI"),
      Map("etni" -> "Y", "omdnStatus" -> "AR"),
      Map("etni" -> "N", "omdnStatus" -> "NH"),
      Map("etni" -> "N", "omdnStatus" -> "PO"),
      Map("etni" -> "N", "omdnStatus" -> ""),
      Map("etni" -> "N", "omdnStatus" -> "AI"),
      Map("etni" -> "", "omdnStatus" -> "AI"),
      Map("etni" -> "", "omdnStatus" -> "PO"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, true, true, true, true, false, false,false,false))
  }

  "WhereCompiler" should "compile AND operation" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:=:SPS)~&&~(type:=:otp)""")
    val listOfMaps = List(Map("reason" -> "SPS", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "Tomm"),
      Map("reason" -> "SP", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, false, false, true))
  }

  "WhereCompiler" should "compile only one operation" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:=:SPS)""")
    val listOfMaps = List(Map("reason" -> "SPS", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "Tomm"),
      Map("reason" -> "SP", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, true, false, true))
  }

  "WhereCompiler" should "compile only one operation without case sensitive" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:=:sps)""")
    val listOfMaps = List(Map("reason" -> "SPS", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "Tomm"),
      Map("reason" -> "SP", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, true, false, true))
  }

  "WhereCompiler" should "compile OR operation" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:=:SPS)~||~(type:=:otp)""")
    val listOfMaps = List(Map("reason" -> "SPS", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "Tomm"),
      Map("reason" -> "SP", "type" -> "ot"),
      Map("reason" -> "SPS", "type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, true, false, true))
  }

  "WhereCompiler" should "compile multi operation" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(~(reason:=:A)~||~(reason:=:B)~)~&&~(~(type:=:C)~||~(type:=:D)~)""")
    val listOfMaps = List(
      Map("reason" -> "A", "type" -> "C"),
      Map("reason" -> "A", "type" -> "D"),
      Map("reason" -> "B", "type" -> "C"),
      Map("reason" -> "B", "type" -> "D"),
      Map("reason" -> "B", "type" -> "B"),
      Map("reason" -> "D", "type" -> "D")
    )
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, true, true, true, false, false))
  }

  "WhereCompiler" should "compile when no data match" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:=:SPS)~||~(type:=:otp)""")
    val listOfMaps = List(Map("reason" -> "SP", "type" -> "ot"),
      Map("reason" -> "SS", "type" -> "Tomm"),
      Map("reason" -> "S", "type" -> "ot"),
      Map("reason" -> "PS", "type" -> "tp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(false, false, false, false))
  }

  "WhereCompiler" should "compile CONTAINS operation with partial success" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:contains:SPS)""")
    val listOfMaps = List(Map("reason" -> "SPS", "type" -> "otp"),
      Map("reason" -> "SPSSS", "type" -> "Tomm"),
      Map("reason" -> "SP", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, true, false, true))
  }

  "WhereCompiler" should "compile CONTAINS operation with 100% success" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:contains:SPS)""")
    val listOfMaps = List(Map("reason" -> "SPS", "type" -> "otp"),
      Map("reason" -> "SPSSS", "type" -> "Tomm"),
      Map("reason" -> "sSPs", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, true, true, true))
  }

  "WhereCompiler" should "compile CONTAINS operation with 0% success" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:contains:SPS)""")
    val listOfMaps = List(Map("reason" -> "S", "type" -> "otp"),
      Map("reason" -> "SSSS", "type" -> "Tomm"),
      Map("reason" -> "SP", "type" -> "otp"),
      Map("reason" -> "PS", "type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(false, false, false, false))
  }

  "WhereCompiler" should "compile CONTAINS with &&" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:contains:SPS)~&&~(type:=:otp)""")
    val listOfMaps = List(Map("reason" -> "SPS", "type" -> "otp"),
      Map("reason" -> "SPSSS", "type" -> "Tomm"),
      Map("reason" -> "SP", "type" -> "otp"),
      Map("reason" -> "SPSS", "type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, false, false, true))
  }

  "WhereCompiler" should "compile CONTAINS with ||" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:contains:SPS)~||~(type:=:otp)""")
    val listOfMaps = List(Map("reason" -> "SPS", "type" -> "otp"),
      Map("reason" -> "SPSSS", "type" -> "Tomm"),
      Map("reason" -> "SP", "type" -> "otp"),
      Map("reason" -> "SPSS", "type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, true, true, true))
  }

  "WhereCompiler" should "compile CreditCheckVelocity condition" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(input_request_url:contains:/onedp/checkout/intiateCreditCheck)~&&~(type_alias:=:static_hit_ig_log)""")
    val listOfMaps = List(Map("input_request_url" -> "SPS", "type_alias" -> "static_hit_ig_log"),
      Map("input_request_url" -> "http://www.verizonwireless.com:80/onedp/checkout/intiateCreditCheck", "type_alias" -> "static_hit_ig_log"),
      Map("input_request_url" -> "http://www.verizonwireless.com:80/onedp/checkout/intiateCreditCheck", "type_alias" -> "static_hit_ig"),
      Map("input_request_url" -> "http://www.verizonwireless.com:80/ondp/checkout/intiateCreditCheck", "type_alias" -> "static_hit_ig_log"),
      Map("input_request_url" -> "http://www.verizonwireless.com:80/onedp/checkout/intiateCrditCheck", "type_alias" -> "static_hit_ig_log"),
      Map("input_request_url" -> "http://www.verizonwireless.com:80/onedp/chckout/intiateCreditCheck", "type_alias" -> "static_hit_ig_log"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(false, true, false, false, false, false))
  }

  "WhereCompiler" should "compile math operation on Int" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(ipAddress:<:22)~||~(userName:=:Tom)""")
    val listOfMaps = List(Map("ipAddress" -> "10", "userName" -> "otp"),
      Map("ipAddress" -> "10", "userName" -> "Tom"),
      Map("ipAddress" -> "20", "userName" -> "otp"),
      Map("ipAddress" -> "30", "userName" -> "Tom"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, true, true, true))
  }

  "WhereCompiler" should "compile math operation on Long" in {
    val longNum: Long = System.currentTimeMillis()
    val whereEX = ExpressionParsingUtil.compileWhereExpression(s"""(ipAddress:>=:$longNum)""")
    val listOfMaps = List(Map("ipAddress" -> (longNum + 10).toString, "userName" -> "otp"),
      Map("ipAddress" -> (longNum).toString, "userName" -> "Tomm"),
      Map("ipAddress" -> "20", "userName" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, true, false))
  }

  "WhereCompiler" should "compile math operation on Double" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(ipAddress:>:22.20)""")
    val listOfMaps = List(Map("ipAddress" -> 10.00.toString, "userName" -> "otp"),
      Map("ipAddress" -> 25.00.toString, "userName" -> "Tom"),
      Map("ipAddress" -> 22.10.toString, "userName" -> "otp"),
      Map("ipAddress" -> 22.63.toString, "userName" -> "Tom"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(false, true, false, true))
  }

  "WhereCompiler" should "compile Not equal" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:!=:sps)""")
    val listOfMaps = List(Map("reason" -> "SPS", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "Tomm"),
      Map("reason" -> "SP", "type" -> "otp"),
      Map("reason" -> "SPS", "type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(false, false, true, false))
  }

//  "WhereCompiler" should "compile regex pattern" in {
//    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:match:[0-9]+)""")
//    val listOfMaps = List(Map("reason" -> "2", "type" -> "otp"),
//      Map("reason" -> "678", "type" -> "Tomm"),
//      Map("reason" -> "SP", "type" -> "otp"),
//      Map("reason" -> "SPS", "type" -> "otp"))
//    val result = listOfMaps.map(m => whereEX(m))
//    assert(result == List(false, false, true, false))
//  }

  "WhereCompiler" should "compile isNonEmpty pattern" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:isNonEmpty)""")
    val listOfMaps = List(Map("reason" -> "2", "type" -> "otp"),
      Map("reason" -> "", "type" -> "Tomm"),
      Map("reason" -> "   ", "type" -> "otp"),
      Map("type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, false, false, false))
  }

  "WhereCompiler" should "compile isEmpty pattern" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(reason:isEmpty)""")
    val listOfMaps = List(Map("reason" -> "2", "type" -> "otp"),
      Map("reason" -> "", "type" -> "Tomm"),
      Map("reason" -> "   ", "type" -> "otp"),
      Map("type" -> "otp"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(false, true, true, true))
  }

  /**
    * ############
    * ## HAVING ##
    * ############
    */

  /**
    * This is the way having expression is compiled in Generic actor
    */
  def runWhereExpression(input: List[Vector[Map[String, Any]]], expression: String): List[Boolean] = {
    val filterFx: Vector[Map[String, Any]] => Boolean = {
      ExpressionParsingUtil.compileHavingExpression(expression)
    }
    input.map(filterFx(_))
  }

  "HavingCompiler" should "compile COUNT expression" in {
    val expression = """count(reason:=:SPS)~>2"""
    val inputVector1 = Vector(Map("reason" -> "SPS", "userName" -> 10),
      Map("reason" -> "SPS", "userName" -> 3),
      Map("reason" -> "SPS", "userName" -> 13))
    val inputVector2 = Vector(Map("reason" -> "SPS", "userName" -> 10),
      Map("reason" -> "SPS", "userName" -> 3),
      Map("reason" -> "SP", "userName" -> 13))
    val inputVector3 = Vector(Map("reason" -> "SPS", "userName" -> 10),
      Map("reason" -> "SP", "userName" -> 3),
      Map("reason" -> "SP", "userName" -> 13))
    val result = runWhereExpression(List(inputVector1, inputVector2, inputVector3), expression)
    assert(result == List(true, false, false))
  }

  "HavingCompiler" should "compile COUNT with contains expression" in {
    val expression = """count(reason:contains:SPS)~>2"""
    val inputVector1 = Vector(Map("reason" -> "SPSS", "userName" -> 10),
      Map("reason" -> "SSPS", "userName" -> 3),
      Map("reason" -> "SPS", "userName" -> 13))
    val inputVector2 = Vector(Map("reason" -> "SPS", "userName" -> 10),
      Map("reason" -> "SPSS", "userName" -> 3),
      Map("reason" -> "SP", "userName" -> 13))
    val inputVector3 = Vector(Map("reason" -> "SPS", "userName" -> 10),
      Map("reason" -> "SP", "userName" -> 3),
      Map("reason" -> "SPP", "userName" -> 13))
    val result = runWhereExpression(List(inputVector1, inputVector2, inputVector3), expression)
    assert(result == List(true, false, false))
  }

  "HavingCompiler" should "compile DISTINCT expression" in {
    val expression = """distinct(userName)~>2"""
    val inputVector1 = Vector(Map("reason" -> "SPS", "userName" -> 10),
      Map("reason" -> "SPS", "userName" -> 3),
      Map("reason" -> "SPS", "userName" -> 13))
    val inputVector2 = Vector(Map("reason" -> "SPS", "userName" -> 10),
      Map("reason" -> "SPS", "userName" -> 10),
      Map("reason" -> "SP", "userName" -> 13))
    val inputVector3 = Vector(Map("reason" -> "SPS", "userName" -> 10),
      Map("reason" -> "SP", "userName" -> 13),
      Map("reason" -> "SP", "userName" -> 13))
    val result = runWhereExpression(List(inputVector1, inputVector2, inputVector3), expression)
    assert(result == List(true, false, false))
  }

  "HavingCompiler" should "compile COUNT plus COUNT expression" in {
    val expression = """(~count(reason:=:A)~+~count(reason:=:B)~)~>2"""
    val inputVector1 = Vector(Map("reason" -> "A", "userName" -> 10),
      Map("reason" -> "B", "userName" -> 3),
      Map("reason" -> "A", "userName" -> 13))
    val inputVector2 = Vector(Map("reason" -> "A", "userName" -> 10),
      Map("reason" -> "B", "userName" -> 3),
      Map("reason" -> "SP", "userName" -> 13))
    val inputVector3 = Vector(Map("reason" -> "A", "userName" -> 10),
      Map("reason" -> "A", "userName" -> 3),
      Map("reason" -> "A", "userName" -> 13))
    val inputVector4 = Vector(Map("reason" -> "B", "userName" -> 10),
      Map("reason" -> "B", "userName" -> 3),
      Map("reason" -> "B", "userName" -> 13))
    val result = runWhereExpression(List(inputVector1, inputVector2, inputVector3, inputVector4)
      , expression)
    assert(result == List(true, false, true, true))
  }

  "HavingCompiler" should "compile COUNT and DISTINCT expression with ==" in {
    val expression = """(~count(reason:=:A)~)~/~(~distinct(userName)~)~==2"""
    val inputVector1 = Vector(Map("reason" -> "A", "userName" -> "10"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "A", "userName" -> "10"))
    val inputVector2 = Vector(Map("reason" -> "A", "userName" -> "10"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "B", "userName" -> "13"))
    val result = runWhereExpression(List(inputVector1, inputVector2)
      , expression)
    assert(result == List(true, false))
  }

  "HavingCompiler" should "compile COUNT and DISTINCT expression with == and not case sensitive" in {
    val expression = """(~count(reason:=:a)~)~/~(~distinct(userName)~)~==2"""
    val inputVector1 = Vector(Map("reason" -> "A", "userName" -> "10"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "A", "userName" -> "10"))
    val inputVector2 = Vector(Map("reason" -> "A", "userName" -> "10"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "B", "userName" -> "13"))
    val result = runWhereExpression(List(inputVector1, inputVector2)
      , expression)
    assert(result == List(true, false))
  }

  "HavingCompiler" should "complex expression" in {
    val expression = """(~count(reason:=:A)~+~count(reason:=:B)~)~/~distinct(userName)~>1"""
    val inputVector1 = Vector(Map("reason" -> "A", "userName" -> "10"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "B", "userName" -> "3"),
      Map("reason" -> "B", "userName" -> "10"))
    val inputVector2 = Vector(Map("reason" -> "A", "userName" -> "10"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "A", "userName" -> "3"),
      Map("reason" -> "B", "userName" -> "13"))
    val result = runWhereExpression(List(inputVector1, inputVector2)
      , expression)
    assert(result == List(true, false))
  }


  "HavingCompiler" should "compute IpVelocity" in {
    val expression = """((~count(activityType:=:F)~*1d)/(~count(activityType:=:S)~+~count(activityType:=:F)~))*100>30&&~distinct(userID)~>2"""
    val inputVector1 = Vector(
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "10"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "11"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "F", "userID" -> "12"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "F", "userID" -> "12")
    )

    //Failure percent less i.e. 25%
    val inputVector2 = Vector(
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "F", "userID" -> "10"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "11"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "12"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "12")
    )

    //Distinct users less i.e. 2
    val inputVector3 = Vector(
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "F", "userID" -> "10"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "F", "userID" -> "10"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "11"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "11")
    )

    //Failure percent less i.e. 25% and Distinct users less i.e. 2
    val inputVector4 = Vector(
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "F", "userID" -> "10"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "10"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "11"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "11")
    )

    //100% Failure. Corner case for division
    val inputVector5 = Vector(
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "F", "userID" -> "10"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "F", "userID" -> "11"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "F", "userID" -> "12"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "F", "userID" -> "12")
    )

    //100% Success. Corner case for division
    val inputVector6 = Vector(
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "10"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "11"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "12"),
      Map("IPAddress" -> "127.0.0.1", "activityType" -> "S", "userID" -> "12")
    )

    val result = runWhereExpression(List(inputVector1, inputVector2, inputVector3, inputVector4, inputVector5, inputVector6)
      , expression)
    assert(result == List(true, false, false, false, true, false))
  }

  /**
    * #####################
    * ## PROD REGRESSION ##
    * #####################
    */

  //1
  "WhereCompiler" should "compile UserVelocity " in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(isValidTransaction:=:true)""")
    val listOfMaps = List(Map("ipAddress" -> 10.00.toString, "isValidTransaction" -> "true"),
      Map("ipAddress" -> 25.00.toString, "isValidTransaction" -> "false"),
      Map("ipAddress" -> 22.10.toString, "isValidTransaction" -> "true"),
      Map("ipAddress" -> 22.63.toString, "isValidTransaction" -> "false"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(true, false, true, false))
  }

  "HavingCompiler" should "compile UserVelocity " in {
    val expression = """distinct(IPAddress)~>1"""
    val inputVector1 = Vector(Map("IPAddress" -> "A", "userName" -> "10"),
      Map("IPAddress" -> "A", "userName" -> "3"),
      Map("IPAddress" -> "A", "userName" -> "3"),
      Map("IPAddress" -> "A", "userName" -> "10"))
    val inputVector2 = Vector(Map("reason" -> "A", "userName" -> "10"),
      Map("IPAddress" -> "A", "userName" -> "3"),
      Map("IPAddress" -> "A", "userName" -> "3"),
      Map("IPAddress" -> "B", "userName" -> "13"))
    val result = runWhereExpression(List(inputVector1, inputVector2)
      , expression)
    assert(result == List(false, true))
  }

  //2
  "WhereCompiler" should "compile OrderStatusIPVelocity  " in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(input_request_url:=:/od/cust/orderStatus/validateCaptcha)~&&~(type_alias:=:generic_hit_log)""")
    val listOfMaps = List(Map("input_request_url" -> 10.00.toString, "type_alias" -> "true"),
      Map("input_request_url" -> "/od/cust/orderStatus/validateCaptcha", "type_alias" -> "generic_hit_log"),
      Map("input_request_url" -> "/od/cust/orderStatus/validateCaptcha", "type_alias" -> "true"),
      Map("input_request_url" -> 22.63.toString, "type_alias" -> "false"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(false, true, false, false))
  }

  "HavingCompiler" should "compile OrderStatusIPVelocity  " in {
    val expression = """(~count(type_alias:=:generic_hit_log)~)~>1"""
    val inputVector1 = Vector(Map("type_alias" -> "generic_hit_log", "userName" -> "10"),
      Map("type_alias" -> "A", "userName" -> "3"),
      Map("type_alias" -> "A", "userName" -> "3"),
      Map("type_alias" -> "A", "userName" -> "10"))
    val inputVector2 = Vector(Map("type_alias" -> "generic_hit_log", "userName" -> "10"),
      Map("type_alias" -> "generic_hit_log", "userName" -> "3"),
      Map("type_alias" -> "generic_hit_log", "userName" -> "3"),
      Map("type_alias" -> "generic_hit_log", "userName" -> "13"))
    val result = runWhereExpression(List(inputVector1, inputVector2)
      , expression)
    assert(result == List(false, true))
  }

  //3
  "WhereCompiler" should "compile CreditCheckVelocity" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(input_request_url:contains:intiateCreditCheck)~&&~(type_alias:=:static_hit_ig_log)""")
    val listOfMaps = List(Map("input_request_url" -> 10.00.toString, "type_alias" -> "true"),
      Map("input_request_url" -> "/od/cust/orderStatus/intiateCreditCheck", "type_alias" -> "static_hit_ig_log"),
      Map("input_request_url" -> "intiateCreditCheck", "type_alias" -> "static_hit_ig_log"),
      Map("input_request_url" -> 22.63.toString, "type_alias" -> "false"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(false, true, true, false))
  }

  "HavingCompiler" should "compile CreditCheckVelocity" in {
    val expression = """(~count(type_alias:=:static_hit_ig_log)~)~>1"""
    val inputVector1 = Vector(Map("type_alias" -> "static_hit_ig_log", "userName" -> "10"),
      Map("type_alias" -> "A", "userName" -> "3"),
      Map("type_alias" -> "static_hit_ig_log", "userName" -> "3"),
      Map("type_alias" -> "A", "userName" -> "10"))
    val inputVector2 = Vector(Map("type_alias" -> "A", "userName" -> "10"),
      Map("type_alias" -> "static_hit_ig_log", "userName" -> "3"),
      Map("type_alias" -> "A", "userName" -> "3"),
      Map("type_alias" -> "B", "userName" -> "13"))
    val result = runWhereExpression(List(inputVector1, inputVector2)
      , expression)
    assert(result == List(true, false))
  }

  //4
  "WhereCompiler" should "compile ANIVelocity" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(module:=:wlnivr)""")
    val listOfMaps = List(Map("module" -> 10.00.toString, "type_alias" -> "true"),
      Map("module" -> "wlnivr", "type_alias" -> "static_hit_ig_log"),
      Map("module" -> "wlnivr", "type_alias" -> "static_hit_ig_log"),
      Map("module" -> 22.63.toString, "type_alias" -> "false"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(false, true, true, false))
  }

  "HavingCompiler" should "compile ANIVelocity" in {
    val expression = """count(module:=:wlnivr)~>1"""
    val inputVector1 = Vector(Map("module" -> "wlnivr", "userName" -> "10"),
      Map("module" -> "wlnivr", "userName" -> "3"),
      Map("module" -> "wlnivr", "userName" -> "3"),
      Map("module" -> "A", "userName" -> "10"))
    val inputVector2 = Vector(Map("module" -> "A", "userName" -> "10"),
      Map("module" -> "static_hit_ig_log", "userName" -> "3"),
      Map("module" -> "A", "userName" -> "3"),
      Map("module" -> "B", "userName" -> "13"))
    val result = runWhereExpression(List(inputVector1, inputVector2)
      , expression)
    assert(result == List(true, false))
  }

  //5
  "WhereCompiler" should "compile SPSVelocity" in {
    val whereEX = ExpressionParsingUtil.compileWhereExpression("""(transactionType:=:MP)~&&~(passwordChangeType:=:Temp Reset)""")
    val listOfMaps = List(Map("transactionType" -> 10.00.toString, "passwordChangeType" -> "Temp Reset"),
      Map("transactionType" -> "MP", "passwordChangeType" -> "Temp Reset"),
      Map("transactionType" -> "M", "passwordChangeType" -> "Temp Reset"),
      Map("transactionType" -> 22.63.toString, "passwordChangeType" -> "Temp Reset"))
    val result = listOfMaps.map(m => whereEX(m))
    assert(result == List(false, true, false, false))
  }

  "HavingCompiler" should "compile SPSVelocity" in {
    val expression = """distinct(userName)~>2"""
    val inputVector1 = Vector(Map("module" -> "wlnivr", "userName" -> "10"),
      Map("module" -> "wlnivr", "userName" -> "3"),
      Map("module" -> "wlnivr", "userName" -> "32"),
      Map("module" -> "A", "userName" -> "10"))
    val inputVector2 = Vector(Map("module" -> "A", "userName" -> "1"),
      Map("module" -> "static_hit_ig_log", "userName" -> "3"),
      Map("module" -> "A", "userName" -> "3"),
      Map("module" -> "B", "userName" -> "3"))
    val result = runWhereExpression(List(inputVector1, inputVector2)
      , expression)
    assert(result == List(true, false))
  }

  //6 - WHERE is empty fot IpVelocity
  "HavingCompiler" should "compile IpVelocity" in {
    val expression = """((~count(activityType:=:F)~*1d)/(~count(activityType:=:S)~+~count(activityType:=:F)~))*100>70&&~distinct(userID)~>1"""
    val inputVector1 = Vector(Map("activityType" -> "F", "userID" -> "10"),
      Map("activityType" -> "F", "userID" -> "3"),
      Map("activityType" -> "S", "userID" -> "32"),
      Map("activityType" -> "F", "userID" -> "10"))
    val inputVector2 = Vector(Map("activityType" -> "F", "userID" -> "10"),
      Map("activityType" -> "F", "userID" -> "3"),
      Map("activityType" -> "S", "userID" -> "32"),
      Map("activityType" -> "S", "userID" -> "10"))
    val inputVector3 = Vector(Map("activityType" -> "F", "userID" -> "10"),
      Map("activityType" -> "F", "userID" -> "10"),
      Map("activityType" -> "S", "userID" -> "10"),
      Map("activityType" -> "F", "userID" -> "10"))
    val result = runWhereExpression(List(inputVector1, inputVector2, inputVector3)
      , expression)
    assert(result == List(true, false, false))
  }

}

